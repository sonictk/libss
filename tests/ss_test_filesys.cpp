int test_tempfiles()
{
	SS_OSFileHandle hFile = createTempFile();

	SS_ASSERT(hFile != NULL);

	return 0;
}


int run_all_filesys_tests()
{
	SS_VERIFYM(test_tempfiles, "Tempfile tests");

	return 0;
}
