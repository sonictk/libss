#include "../src/ss_statistics_math.h"


int test_summate()
{
	float sum1 = summateX(2.0f, 4.5f, 2.5f);
	SS_ASSERTM(areFloatsEqual(sum1, 9.0f) == true, "Summation failed!");

	double sum2 = summateX(5.3, 15, 20.5, 40.0f);
	SS_ASSERTM(isEqual(sum2, 80.8) == true, "Summation failed!");

	float test[3] = {14.3f, 20.2f, 50.5f};
	float sum3 = summate(test, 0, 2);
	SS_ASSERTM(areFloatsEqual(sum3, 85.0f) == true, "Summation failed!");

	float valuesX[] = {9.0f, 15.0f, 25.0f, 14.0f, 10.0f, 18.0f, 0.0f, 16.0f, 5.0f, 19.0f, 16.0f, 20.0f};
	float resultX = summate(valuesX, 12);
	SS_ASSERT(isEqual(resultX, 167.0f) == true);

	float valuesY[] = {39.0f, 56.0f, 93.0f, 61.0f, 50.0f, 75.0f, 32.0f, 85.0f, 42.0f, 70.0f, 66.0f, 80.0f};
	float resultY = summate(valuesY, 12);
	SS_ASSERT(isEqual(resultY, 749.0f)  == true);

	return 0;
}


int test_mean()
{
	float mean1 = static_cast<float>(meanX(10, 20));
	SS_ASSERT(isEqual(mean1, 15) == true);

	float tmp[3] = {30.0f, 15.0f, 0.0f};
	float mean2 = mean(tmp, 0, 2);
	SS_ASSERT(isEqual(mean2, 15.0f) == true);

	double test2[] = {8.0, 9.0, 11.0, 12.0};
	SS_ASSERT(isEqual(mean(test2, 4), 10.0) == true);
	SS_ASSERT(isEqual(mean(test2, 0, 3), 10.0) == true);

	float valuesX[] = {9.0f, 15.0f, 25.0f, 14.0f, 10.0f, 18.0f, 0.0f, 16.0f, 5.0f, 19.0f, 16.0f, 20.0f};
	float resultX = mean(valuesX, 12);
	SS_ASSERT(isEqual(resultX, 13.92f, 0.01) == true);

	float valuesY[] = {39.0f, 56.0f, 93.0f, 61.0f, 50.0f, 75.0f, 32.0f, 85.0f, 42.0f, 70.0f, 66.0f, 80.0f};
	float resultY = mean(valuesY, 12);
	SS_ASSERT(isEqual(resultY, 62.42f, 0.01)  == true);

	float resultX1 = meanX(9.0f, 15.0f, 25.0f, 14.0f, 10.0f, 18.0f, 0.0f, 16.0f, 5.0f, 19.0f, 16.0f, 20.0f);
	SS_ASSERT(isEqual(resultX1, 13.92f, 0.01) == true);

	float test3[] = {2.1f, 2.5f, 4.0f, 3.6f};
	float result3 = mean(test3, 4);
	SS_ASSERT(isEqual(result3, 3.1f, 0.1) == true);

	float test4[] = {8.0f, 12.0f, 14.0f, 10.0f};
	float result4 = mean(test4, 4);
	SS_ASSERT(isEqual(result4, 11.0f, 0.1) == true);

	return 0;
}


int test_standard_deviation()
{
	float test[] = {0.0f, 8.0f, 12.0f, 20.0f};
	float result = standardDeviation(test, 0, 3);
	SS_ASSERT(isEqual(result, 8.326664f, 0.001f) == true);

	double test2[] = {8.0, 9.0, 11.0, 12.0};
	double result2 = standardDeviation(test2, 4);
	SS_ASSERT(isEqual(result2, 1.8257418583, 0.001) == true);

	float result3 = standardDeviationX(0.0f, 8.0f, 12.0f, 20.0f);
	SS_ASSERT(isEqual(result3, 8.326664f, 0.001) == true);

	return 0;
}


int test_covariance()
{
	float valuesX[] = {2.1f, 2.5f, 4.0f, 3.6f};
	float valuesY[] = {8.0f, 12.0f, 14.0f, 10.0f};

	float result = covariance(valuesX, valuesY, 4);
	SS_ASSERT(isEqual(result, 1.53f, 0.01f) == true);

	return 0;
}


// int test_covariance_matrix()
// {
// 	typedef MatX<float, 3, 4> TestMatX;

// 	TestMatX m = {5.0f, 0.0f, 3.0f, 7.0f,
// 				  1.0f, -5.0f, 7.0f, 3.0f,
// 				  4.0f, 9.0f, 8.0f, 10.0f};

// 	MatX<float, 4, 4> cov = covariance(m);

// 	for (int r=0; r < 4; ++r) {
// 		for (int c=0; c < 4; ++c) {
// 			printf("%.3f ", cov[r][c]);
// 		}
// 	}

// 	MatX<float, 4, 4> expectedResult = {4.3333f, 8.8333f, -3.0f, 5.6667f,
// 										8.8333f, 50.3333f, 6.5f, 24.1667f,
// 										-3.0f, 6.5f, 7.0f, 1.0f,
// 										5.6667f, 24.16667f, 1.0f, 12.3333f};

// 	SS_ASSERTM(cov == expectedResult, "The covariance matrix was not correct!");

// 	return 0;
// }
