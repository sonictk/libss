#include "../src/ss_platform.h"
#include "../src/ss_test.h"
#include "../src/ss_benchmark.h"
#include "../src/ss_timer.h"
#include "../src/ss_uuid.h"
#include "../src/ss_logging.h"
#include "../src/ss_errno.cpp"
#include "../src/ss_filesys.h"

#include "ss_test_math.cpp"
#include "ss_test_filesys.cpp"


int SS_TESTS_RUN = 0;
int SS_TESTS_FAILED = 0;


int test_uuids()
{
	SSUuid uuid1;
	int result = createUuid(uuid1);
	SS_ASSERTM(result == 0, "Creating UUID failed!");

	SSUuid uuid2;
	result = createUuid(uuid2);

	SS_ASSERTM(areUuidsEqual(uuid1, uuid1) == true, "UUID comparison failed!");
	SS_ASSERTM(areUuidsEqual(uuid1, uuid2) == false, "UUID comparison failed!");

	char uuidName[SS_UUID_STRING_LENGTH + 1];
	result = getUuidAsString(uuid1, uuidName);
	SS_ASSERTM(result == 0, "UUID string retrieval failed!");

	SSUuid uuidRetrieve;
	result = getUuid(uuidName, uuidRetrieve);
	SS_ASSERTM(result == 0, "UUID retrieval from string failed!");
	SS_ASSERTM(areUuidsEqual(uuid1, uuidRetrieve) == true, "UUID string retrieval gave a wrong UUID!");

	return 0;
}


int run_all_uuid_tests()
{
	SS_VERIFYM(test_uuids, "UUID tests");

	return 0;
}


int test_memory_allocations()
{
	char *testAlloc = (char *)ss_malloc(sizeof(char) * 2048);

	SS_ASSERTM(testAlloc != NULL, "Memory allocation failed!");

	char *testRealloc = (char *)ss_realloc(testAlloc, sizeof(char) * 1024);

	SS_ASSERTM(testRealloc != NULL, "Memory re-allocation failed!");

	ss_free(testRealloc);

	return 0;
}


int run_all_memory_tests()
{
	SS_VERIFYM(test_memory_allocations, "Memory allocations");

	return 0;
}


// NOTE: (sonictk) The main entry point for the tests.
int main(int, char *[])
{
	int result = 0;
	SS_PINFO("Running math unit tests...\n");

	SSPerfTimer timer = perfTimer();

	startSSPerfTimer(timer);

	result = run_all_math_tests();

	endSSPerfTimer(timer);
	double timeTaken = getSSPerfTimerValue(timer) / 1000000.0;

	SS_PINFO("\nTests run: %d in %fs\n", SS_TESTS_RUN, timeTaken);

	if (result == 0) {
		SS_PINFO("\nAll math tests passed..\n");
	} else {
		SS_PERROR("\nTests failed: %d!\n", SS_TESTS_FAILED);

		return result;
	}

	SS_PINFO("Running filesys unit tests...\n");

	resetSSPerfTimerValue(timer);
	startSSPerfTimer(timer);

	result = run_all_filesys_tests();

	endSSPerfTimer(timer);
	timeTaken = getSSPerfTimerValue(timer) / 1000000.0;

	SS_PINFO("\nTests run: %d in %fs\n", SS_TESTS_RUN, timeTaken);

	if (result == 0) {
		SS_PINFO("\nAll filesys tests passed..\n");
	} else {
		SS_PERROR("\nTests failed: %d!\n", SS_TESTS_FAILED);

		return result;
	}

	SS_PINFO("Running UUID unit tests...\n");

	resetSSPerfTimerValue(timer);
	startSSPerfTimer(timer);

	result = run_all_uuid_tests();

	endSSPerfTimer(timer);
	timeTaken = getSSPerfTimerValue(timer) / 1000000.0;

	SS_PINFO("\nTests run: %d in %fs\n", SS_TESTS_RUN, timeTaken);

	if (result == 0) {
		SS_PINFO("\nAll UUID tests passed..\n");
	} else {
		SS_PERROR("\nTests failed: %d!\n", SS_TESTS_FAILED);
	}

	resetSSPerfTimerValue(timer);
	startSSPerfTimer(timer);

	result = run_all_memory_tests();

	endSSPerfTimer(timer);
	timeTaken = getSSPerfTimerValue(timer) / 1000000.0;

	SS_PINFO("\nTests run: %d in %fs\n", SS_TESTS_RUN, timeTaken);

	if (result == 0) {
		SS_PINFO("\nAll memory tests passed.\n");
	} else {
		SS_PERROR("\nTests failed: %d!\n", SS_TESTS_FAILED);
	}

	return result;
}
