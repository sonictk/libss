#include "../src/ss_platform.h"
#include "../src/ss_test.h"
#include "../src/ss_benchmark.h"
#include "../src/ss_timer.h"
#include "../src/ss_common_math.h"
#include "../src/ss_vector_math.h"
#include "../src/ss_statistics_math.cpp"

#include "ss_test_statistics_math.cpp"


// NOTE: (sonictk) Vec3 tests
int test_vec3_addition()
{
	Vec3 v1 = vec3(5, 10, 15);
	Vec3 v2 = vec3(40, 45, 70);

	SS_ASSERTM(v1 + v1 == vec3(10, 20, 30), "Vector addition unsuccessful!");
	SS_ASSERTM(v1 + v2 == vec3(45, 55, 85), "Vector addition was unsuccessful!");

	return 0;
}


int test_vec3_subtraction()
{
	Vec3 v1 = vec3(5, 10, 15);
	Vec3 v2 = vec3(40, 45, 70);

	SS_ASSERTM(v1 - v1 == vec3(), "Vector subtraction was unsuccessful!");
	SS_ASSERTM(v1 - v2 == vec3(-35, -35, -55), "Vector subtraction was unsuccessful!");

	return 0;
}


int test_vec3_inner_product()
{
	Vec3 v1 = vec3(5, 10, 1);
	Vec3 v2 = vec3(40, 5, 3);

	SS_ASSERTM(innerProduct(v1, v2) == 253, "Dot product was unsuccessful!");

	return 0;
}


int test_vec3_cross_product()
{
	Vec3 v1 = vec3(1, 0, 0);
	Vec3 v2 = vec3(0, 1, 0);

	SS_ASSERTM(crossProduct(v1, v2) == vec3(0, 0, 1),
			   "Cross product was unsuccessful!");

	SS_ASSERTM(crossProduct(vec3(1, 0, 0), vec3(0, 0, -1)) == vec3(0, 1, 0),
			   "Cross product incorrect!");

	// NOTE: (sonictk) Test for degenerate case where cross product of vector with itself
	SS_ASSERTM(crossProduct(v1, v1) == vec3(0, 0, 0),
			   "Cross product of vector with itself should be zero!");

	return 0;
}


// NOTE: (sonictk) Math tests
int test_lerp()
{
	float a = 10;
	float b = 20;

	SS_ASSERTM(lerp(a, b, 0.3f) == 13, "Lerp failed!");

	SS_ASSERTM(unlerp(13.0f, a, b) == 0.3f, "Unlerp failed!");

	return 0;
}


int test_rotate_by_axis_angle()
{
	Vec3 v1 = vec3(0, 0, -1);
	Vec3 v2 = vec3(0, 0, 1);

	// NOTE: (sonictk) Test rotation about self
	SS_ASSERTM(rotateBy(v1, v2, 90) == v1, "Rotate by axis-angle rotation failed!");

	Vec3 v3 = vec3(1, 0, 0);
	SS_ASSERTM(rotateBy(v1, v3, 90) == vec3(0, 1, 0), "Rotate by axis-angle rotation failed!");

	SS_ASSERTM(rotateBy(vec3(1.0f, 0.0f, 0.0f),
						vec3(-0.0f, 1.0f, 0.0f),
						90.0f) == vec3(0.0f, 0.0f, -1.0f), "Rotation by axis-angle failed!");

	Vec3 v4 = rotateBy(vec3(1, 0, 0), vec3(0, 1, 0), 45);
	SS_ASSERTM(v4 == vec3(0.707106769f, 0.0f, -0.707106769f), "Rotation by axis-angle failed!");


	return 0;
}


int test_angle_between_vectors()
{
	Vec3 v1 = vec3(0, 1, 0);
	Vec3 v2 = vec3(0, 0, 1);
	SS_ASSERTM(angleBetween(v1, v2) == 90.0f, "Angle between vectors determined incorrectly!");

	return 0;
}


int test_normalize_vector()
{
	Vec3 v1 = vec3(1, 0, 0);
	SS_ASSERTM(normalize(v1) == v1, "Normalization of unit vector was incorrect!");

	SS_ASSERTM(normalize(vec3(5, 0, 0)) == vec3(1, 0 ,0), "Normalization of vector failed!");

	return 0;
}


int test_matrix_math()
{
	MatX<int, 4, 4> m1 = matX<int, 4, 4>();
	MatX<int, 4, 4> m2 = matX<int, 4, 4>(2);

	MatX<int, 4, 4> result = matX<int, 4, 4>(2);
	SS_ASSERT(m1 + m2 == result);

	MatX<int, 4, 4> result2 = matX<int, 4, 4>(-2);
	SS_ASSERT(m1 - m2 == result2);

	result2 -= -2;

	MatX<int, 4, 4> result3 = matX<int, 4, 4>(-4);
	SS_ASSERT(result * -2 == result3);

	result3 *= -4;

	MatX<int, 4, 4> result4 = matX<int, 4, 4>(16);
	SS_ASSERT(result3 == result4);

	Mat33 m33a = mat33(1, 0, 0, 0, 1, 0, 0, 0, 1);
	Mat33 m33b = mat33(4, 4, 4, 4, 4, 4, 4, 4, 4);

	Mat33 result5 = m33a + m33b;
	Mat33 expectedResult5 = mat33(5, 4, 4, 4, 5, 4, 4, 4, 5);

	SS_ASSERT(result5 == expectedResult5);

	return 0;
}


int test_matrix_transform_math()
{
	Mat44 m1 = mat44();
	Mat44 identityMatrix = mat44(1, 0, 0, 0,
								 0, 1, 0, 0,
								 0, 0, 1, 0,
								 0, 0, 0, 1);
	SS_ASSERT(m1 == identityMatrix);

	translateBy(m1, vec3(5, 15, 10));

	Mat44 expectedResult1 = mat44(1, 0, 0, 5,
								  0, 1, 0, 15,
								  0, 0, 1, 10,
								  0, 0, 0, 1);
	SS_ASSERTM(m1 == expectedResult1, "Matrix translation failed!");

	scaleBy(m1, vec3(3, 4, 5));

	Mat44 expectedResult2 = mat44(3, 0, 0, 5,
								  0, 4, 0, 15,
								  0, 0, 5, 10,
								  0, 0, 0, 1);
	SS_ASSERTM(m1 == expectedResult2, "Matrix scaling failed!");

	// TODO: (sonictk) Need to test matrix rotation
	// TODO: (sonictk) Need to test quaternion multiplication/division

	return 0;
}



// NOTE: (sonictk) All testing should be placed into this function.
int run_all_math_tests()
{
	SS_VERIFYM(test_vec3_addition, "Vector addition tests");
	SS_VERIFYM(test_vec3_subtraction, "Vector substraction tests");
	SS_VERIFY(test_vec3_inner_product);
	SS_VERIFY(test_vec3_cross_product);

	SS_VERIFY(test_lerp);

	SS_VERIFY(test_rotate_by_axis_angle);
	SS_VERIFY(test_angle_between_vectors);

	SS_VERIFY(test_normalize_vector);

	SS_VERIFYM(test_summate, "Summation tests");

	SS_VERIFYM(test_mean, "Mean tests");

	SS_VERIFYM(test_matrix_math, "Matrix basic math tests");
	SS_VERIFYM(test_matrix_transform_math, "Transformation matrix tests");

	SS_VERIFYM(test_standard_deviation, "Test for calculating standard deviation");

	SS_VERIFYM(test_covariance, "Finding covariance of values");

	return 0;
}
