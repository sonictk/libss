#!/usr/bin/env bash

# This is the GCC build script for the Super Simple Libraries.
# usage: build.sh [debug|release|clean]

# todo: look into using "`dirname \"$0\"`" to act as replacement for $PWD.

StartTime=`date +%T`;
echo "Build script started executing at ${StartTime}...";

# Process command line arguments
BuildType=$1;

if [ "${BuildType}" == "" ]; then
    BuildType="release";
fi;

ProjectName=$2;
if [ "${ProjectName}" == "" ]; then
    ProjectName="libss";
fi;

echo "Building ${ProjectName} in ${BuildType} configuration...";

# Define colours to be used for terminal output messages
RED='\033[0;31m';
GREEN='\033[0;32m';
NC='\033[0m'; # No Color

# Create a build directory to store artifacts
BuildDir="${PWD}/linuxbuild";

# If cleaning builds, just delete build artifacts and exit immediately
if [ "${BuildType}" == "clean" ]; then
    echo "Cleaning build from directory: ${BuildDir}. Files will be deleted!";
    read -p "Continue? (Y/N)" ConfirmCleanBuild;
    if [ $ConfirmCleanBuild == [Yy] ]; then
       echo "Removing files in: ${BuildDir}...";
       rm -rf $BuildDir;
    fi;

    exit 0;
fi;

echo "Building in directory: ${BuildDir}";
if [ ! -d "${BuildDir}" ]; then
   mkdir -p "${BuildDir}";
fi;


# Setup test(s) compilation
TestsIncludePath="${PWD}/src";

TestPlatformName="ss_test_platform";
TestPlatformEntryPoint="${PWD}/tests/${TestPlatformName}.cpp";
OutTestPlatformExe="${BuildDir}/${TestPlatformName}.exe";

TestPlatformLinkerFlags="-lm,-lstdc++";
TestPlatformDebugLinkerFlags="${TestPlatformLinkerFlags}";
TestPlatformReleaseLinkerFlags="${TestPlatformLinkerFlags},-s";

TestPlatformCompilerFlags="-m64 -Werror -Wall -fPIC -fvisibility=hidden -fpermissive -I${TestsIncludePath}";
TestPlatformCompilerFlagsDebug="-ggdb -O0 ${TestPlatformCompilerFlags} -D_DEBUG";
TestPlatformCompilerFlagsRelease="-O3 ${TestPlatformCompilerFlags} -DNDEBUG";

GCCPath="g++";
# GCCPath="/opt/rh/devtoolset-7/root/usr/bin/g++";


if [ "${BuildType}" == "debug" ]
then
    BuildCommand="${GCCPath} ${TestPlatformEntryPoint} ${TestPlatformCompilerFlagsDebug} -o ${OutTestPlatformExe} -Wl,${TestPlatformDebugLinkerFlags}";
else
    BuildCommand="${GCCPath} ${TestPlatformEntryPoint} ${TestPlatformCompilerFlagsRelease} -o ${OutTestPlatformExe} -Wl,${TestPlatformReleaseLinkerFlags}";
fi;


echo "Compiling/linking source files (command follows below)...";
echo "${BuildCommand}";
echo "";

${BuildCommand};

if [ $? -ne 0 ]; then
    echo -e "${RED}***************************************${NC}";
    echo -e "${RED}*      !!! An error occurred!!!       *${NC}";
    echo -e "${RED}***************************************${NC}";

    EndTime=`date +%T`;
    echo "Build script finished execution at ${EndTime}.";

    exit 1;
fi;


# On success, display confirmation message.
echo -e "${GREEN}***************************************${NC}";
echo -e "${GREEN}*    Build completed successfully!    *${NC}";
echo -e "${GREEN}***************************************${NC}";


EndTime=`date +%T`;
echo "Build script finished execution at ${EndTime}.";

exit 0;
