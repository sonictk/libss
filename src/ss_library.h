/**
 * @file   ss_library.h
 * @brief  DLL-related functions.
 */
#ifndef SS_LIBRARY_H
#define SS_LIBRARY_H


/**
 * This loads a shared library from the given ``filename``.
 *
 * @param filename 	The full path to the library to load. Must use OS native
 * 					path separators.
 *
 * @return 			The handle to the library. If an error occurred, returns ``NULL``.
 */
inline SS_DLLHandle loadSharedLibrary(const char *filename);


/**
 * Unloads a shared library and frees the memory associated with it.
 *
 * @param handle 	The handle that represents the library.
 *
 * @return 		``0`` on success, a negative value if an error occurred.
 */
inline int unloadSharedLibrary(SS_DLLHandle handle);


/**
 * This performs run-time dynamic linking of a ``symbol`` with the given name.
 *
 * @param handle	The library to load the symbol from.
 * @param symbol	The name of the symbol to load.
 *
 * @return			The address to the symbol requested on success, ``NULL`` if
 * 				an error occurred or the symbol does not exist.
 */
inline SS_FuncPtr loadSymbolFromLibrary(SS_DLLHandle handle, const char *symbol);


// NOTE: (sonictk) All DLL platform-related machinery goes here
#ifdef _WIN32

#include "ss_library_win32.cpp"

#elif __linux__ || __APPLE__

#include "ss_library_linux.cpp"

#else

#error "Unsupported OS platform for compilation."

#endif // Exports platform layer


#endif /* SS_LIBRARY_H */
