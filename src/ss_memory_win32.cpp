#include "ss_memory.h"
#include <Windows.h>
#include <assert.h>


void *ss_malloc(size_t sz)
{
	static HANDLE processHeap = GetProcessHeap();
	if (processHeap == NULL) {
		return NULL;
	}

#ifdef _DEBUG
	LPVOID buffer = HeapAlloc(processHeap, HEAP_GENERATE_EXCEPTIONS|HEAP_ZERO_MEMORY, (SIZE_T)sz);
#else
	LPVOID buffer = HeapAlloc(processHeap, 0, (SIZE_T)sz);
#endif // _DEBUG

	void *result = (void *)buffer;

	return result;
}


void *ss_realloc(void *p, size_t sz)
{
	// NOTE: (sonictk) Follow the CRT conventions; if ``p`` is NULL, it's as if malloc() was called instead.
	if (p == NULL) {
		return ss_malloc(sz);
	}

	static HANDLE processHeap = GetProcessHeap();
	if (processHeap == NULL) {
		return NULL;
	}

#ifdef _DEBUG
	LPVOID buffer = HeapReAlloc(processHeap, HEAP_GENERATE_EXCEPTIONS|HEAP_ZERO_MEMORY, (LPVOID)p, (SIZE_T)sz);
#else
	LPVOID buffer = HeapReAlloc(processHeap, 0, (LPVOID)p, (SIZE_T)sz);
#endif // _DEBUG
	void *result = (void *)buffer;

	return result;
}


void ss_free(void *p)
{
	assert(p != NULL);

	static HANDLE processHeap = GetProcessHeap();
	if (processHeap == NULL) {
		return;
	}

#ifdef _DEBUG
	BOOL result = HeapFree(processHeap, 0, (LPVOID)p);
	assert(result != 0);
#else
	HeapFree(processHeap, 0, (LPVOID)p);
#endif // _DEBUG

	return;
}


void *ss_memcpy(void *dest, const void *src, size_t count)
{
	// NOTE: (sonictk) Even though it's the same thing as memcpy(), we'll just use the win32 API in
	// case Microsoft changes it in the future...
	CopyMemory((PVOID)dest, (VOID *)src, (SIZE_T)count);

	return dest;
}
