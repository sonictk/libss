/**
 * @file 		ss_platform.h
 * @brief		This header contains common program functionality
 * 			that is intended to work across different operating systems.
 *
 *				Coding standards:
 * 			- All constants must be in the form of ``THIS_IS_A_CONSTANT``.
 *
 * 			- Tabs for indentation and spaces for alignment.
 *
 * 			- ``CamelCase`` for function/variable names and types.
 *
 * 			- No using-directives are allowed.
 *
 * 			- All code **must** be C++11 compatible. No C++14/17/20/xx-specific features are allowed.
 *
 * 			- All C++-specific code should be wrapped. This library is intended to work in both C/C++ environments.
 * 			  For cases where this is not possible (templates, references, C++ features etc.), providing the C++
 * 			  implementation first is acceptable, but the header should still compile.
 *
 * 			- The ``SS`` prefix should be used for library-specific implementations.
 *
 * 			- Headers may have dependencies within this library, except for this main platform file.
 */
#ifndef SS_PLATFORM_H
#define SS_PLATFORM_H


/// For unity builds, this definition is used for easy grepping.
#define globalVar static
#define SS_GLOBAL globalVar

/// For non-unity builds, see above.
#define localVar static
#define SS_LOCAL localVar

#if !defined(SS_INTERNAL)
#define SS_INTERNAL static
#endif


#define SS_PLATFORM_NUM_OF_BITS_IN_ONE_BYTE 8


#ifdef __cplusplus
/// Use this for de-mangling symbol names and allowing functions to have external
/// linkage to be used in C.
#define SS_SHARED extern "C"
#define SS_IMPORT extern "C"
#endif // __cplusplus


// NOTE: (sonictk) C++11 support is required for ``cstdint``
#if __cplusplus >= 201103L

#include <cstdint>
#include <cstddef>

using std::uint8_t;
using std::int8_t;

using std::uint16_t;
using std::int16_t;

using std::uint32_t;
using std::int32_t;

using std::uint64_t;
using std::int64_t;

using std::size_t;

#include <ctime>
using std::time_t;

#include <cstdlib>

#include <cstdio>
using std::rand;
using std::srand;
using std::perror;

using std::malloc;
using std::free;

#else

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#endif // C++ compiler support check


/// Aliases for basic size types
typedef short int sint;
typedef unsigned short int usint;

typedef unsigned int uint;

typedef uint8_t uint8;
typedef uint8 u8;
typedef int8_t int8;
typedef int8 i8;

typedef uint16_t uint16;
typedef uint16 u16;
typedef int16_t int16;
typedef int16 i16;

typedef uint32_t uint32;
typedef uint32 u32;
typedef int32_t int32;
typedef int32 i32;

typedef uint64_t uint64;
typedef uint64 u64;
typedef int64_t int64;
typedef int64 i64;

typedef size_t sizet;

typedef time_t timet;

typedef long long dlong;
typedef unsigned long long udlong;


/// Standard OS file separators for filesystems
static const char WINDOWS_PATH_SEPARATOR = '\\';
static const char UNIX_PATH_SEPARATOR = '/';


/// This is the difference between the Windows and Unix epoch start times.
/// Windows: 1601-01-01T00:00:00Z
/// Linux: 1970-01-01T00:00:00Z
#define WINDOWS_TICK 10000000 // NOTE: (sonictk) Windows uses 100ns intervals
#define SEC_TO_UNIX_EPOCH 11644473600LL


#ifdef _WIN32
#ifdef LIBSS_STATIC_BUILD
#include "ss_platform_win32.h"
#else
#include "ss_platform_win32.cpp"
#endif // LIBSS_STATIC_BUILD

#elif __linux__ || __APPLE__
#ifdef LIBSS_STATIC_BUILD
#include "ss_platform_linux.h"
#else
#include "ss_platform_linux.cpp"
#endif // LIBSS_STATIC_BUILD

#else
#error "Unsupported OS platform for compilation."
#endif /* OS platform implementation layer */


#endif /* SS_PLATFORM_H */
