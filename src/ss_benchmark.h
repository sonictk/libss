/**
 * @file   ss_benchmark.h
 * @brief  Utilities for running benchmarks on code.
 */
#ifndef SS_BENCHMARK_H
#define SS_BENCHMARK_H


#ifdef __cplusplus
#include <cstdio>
#else
#include <stdio.h>
#endif // __cplusplus


#include "ss_ansi.h"
#include "ss_timer.h"
#include "ss_string.h"


/// Descriptions for a benchmark cannot exceed this length, including the null terminator.
static const int SS_MAX_BENCHMARK_DESC_LENGTH = 512;


/// The data storage for a benchmark.
struct SSBenchmarkResult
{
	/// Number of times the function was tested.
	int iterations;

	/// Cumulative time for all iterations to run.
	double time;

	/// Information about what the benchmark is for.
	char description[SS_MAX_BENCHMARK_DESC_LENGTH];
};


/**
 * Runs a micro-benchmark on a given function.
 *
 * @param func 				The invocation of the function to benchmark.
 * @param iterations			The number of times to run the function for.
 * @param description			A text description to assign to the benchmark. Should not
 * 							exceed the length of ``MAX_BENCHMARK_DESC_LENGTH``.
 * @param BenchmarkResult		A ``BenchmarkResult`` storage that will be used for
 * 							containing the results of the benchmark.
 */
#define SS_BENCHMARK(func, iter, desc, BenchmarkResult)		\
do {															\
	SSPerfTimer timer = perfTimer();								\
	startSSPerfTimer(timer);										\
																\
	for (int i=0; i < iter; ++i) {								\
		func;													\
	}															\
																\
	endSSPerfTimer(timer);										\
	BenchmarkResult.time = getSSPerfTimerValue(timer);			\
	BenchmarkResult.iterations = iter;							\
	copyString(BenchmarkResult.description, desc);				\
} while (0)


/**
 * Formats and outputs the ``result`` to ``stdout``.
 *
 * @param result		The result to print.
 */
inline void SS_printBenchmarkResults(SSBenchmarkResult &result)
{
	printf(SS_ANSI_COLOR_GREEN "\n########################\nBenchmark results for %s:\n" SS_ANSI_COLOR_RESET, result.description);
	printf(SS_ANSI_COLOR_CYAN "\nIterations run: %d\n" SS_ANSI_COLOR_RESET, result.iterations);
	printf(SS_ANSI_COLOR_CYAN "Time taken: %f\n" SS_ANSI_COLOR_RESET, result.time);
	printf(SS_ANSI_COLOR_GREEN "\n########################\n" SS_ANSI_COLOR_RESET);
}


#endif /* SS_BENCHMARK_H */
