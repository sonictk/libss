/**
 * @file   ss_statistics_math.h
 * @brief  Math functions related to statistics.
 */
#ifndef SS_STATISTICS_MATH_H
#define SS_STATISTICS_MATH_H


#ifdef __cplusplus

#include <cmath>

#else

#include <math.h>

#endif // __cplusplus


#include "ss_matrix_math.h"
#include "ss_intrinsics_math.h"


/**
 * Computes the sum of the given ``values`` in the range given.
 * Warning: no bounds-checking is done!
 *
 * @param values	The values to sum up.
 * @param first	The start of the range of values to calculate the sum for.
 * @param last		The end of the range of values to calculate the sum for.
 *
 * @return			The sum.
 */
inline float summate(const float *values, int first, int last)
{
	float result = 0.0f;

	for (int i=first; i <= last; ++i) {
		result += values[i];
	}

	return result;
}

/// @overload
inline double summate(const double *values, int first, int last)
{
	double result = 0.0;

	for (int i=first; i <= last; ++i) {
		result += values[i];
	}

	return result;
}

/// @overload
inline int summate(const int *values, int first, int last)
{
	int result = 0;

	for (int i=first; i <= last; ++i) {
		result += values[i];
	}

	return result;
}

/**
 * Computes the sum of the given ``values`` in the range given.
 * Warning: no bounds-checking is done!
 *
 * @param values	The values to sum up.
 * @param size		The number of values. If ``values`` is not large enough, the
 * 				behaviour is undefined.
 *
 * @return			The sum.
 */
inline float summate(const float *values, int size)
{
	return summate(values, 0, size - 1);
}

/// @overload
inline double summate(const double *values, int size)
{
	return summate(values, 0, size - 1);
}

/// @overload
inline int summate(const int *values, int size)
{
	return summate(values, 0, size - 1);
}

#ifdef __cplusplus
template<typename T>
T summate(const T *values, int first, int last)
{
	T result = T();

	for (int i=first; i <= last; ++i) {
		result += values[i];
	}

	return result;
}

template<typename T>
T summate(const T *values, int size)
{
	return summate(values, 0, size -1);
}

template<typename T, typename... Targs>
T summateX(const T &t, const Targs &... Fargs)
{
	size_t numOfArgs = sizeof...(Fargs);
	T runningSum = t;
	T args[] = {static_cast<T>(Fargs)...};
	for (size_t i=0; i < numOfArgs; ++i) {
		runningSum += static_cast<T>(args[i]);
	}

	return runningSum;
}
#endif // __cplusplus


/**
 * Calculates the statistical mean of a given set of ``values``. Type ``T`` must
 * support basic math operands and the ``[]`` operator for index access.
 * Warning: no bounds-checking is done!
 *
 * @param values	A pointer to the start of the array of values.
 * @param first	The start index of the range of values to calculate the mean for.
 * @param last		The end index of the range of values to calculate the mean for.
 *
 * @return			The statistical mean.
 */
inline float mean(const float *values, int first, int last)
{
	return summate(values, first, last) / (last - first + 1);
}

/// @overload
inline int mean(const int *values, int first, int last)
{
	return summate(values, first, last) / (last - first + 1);
}

/**
 * Calculates the statistical mean of a given set of ``values``. Type ``T`` must
 * support basic math operands and the ``[]`` operator for index access.
 * Warning: no bounds-checking is done!
 *
 * @param values	A pointer to the start of the array of values.
 * @param size		The number of values. If ``values`` is not large enough, the
 * 				behaviour is undefined.
 *
 * @return			The statistical mean.
 */
inline float mean(const float *values, int size)
{
	return summate(values, size) / size;
}

/// @overload
inline int mean(const int *values, int size)
{
	return summate(values, size) / size;
}

#ifdef __cplusplus
template<typename T>
T mean(const T *values, int first, int last)
{
	return summate(values, first, last) / (last - first + 1);
}

template<typename T>
T mean(const T *values, int size)
{
	return mean(values, 0, size - 1);
}

template<typename T, typename... Targs>
T meanX(const T &t, const Targs &... Fargs)
{
	unsigned int numOfArgs = static_cast<unsigned int>(sizeof...(Fargs));

	return summateX(t, Fargs...) / (numOfArgs + 1);
}
#endif // __cplusplus


/**
 * Calculates the **standard deviation** for the given array of values. The standard
 * deviation is defined as the measure of how spread out the data is; the average
 * distance from the mean of a the data set to a point.
 *
 * @param values		The values to calculate the SD for.
 * @param first		The index of the first value to use.
 * @param last			The index of the last value to use.
 *
 * @return				The standard deviation of the given array.
 */
inline float standardDeviation(const float *values, int first, int last)
{
	// NOTE: (sonictk) Referenced formula from: http://www.cs.otago.ac.nz/cosc453/student_tutorials/principal_components.pdf
	float result = 0.0f;
	float meanVal = mean(values, first, last);

	for (int i=first; i <= last; ++i) {
		result += powf((values[i] - meanVal), 2);
	}

	result = squareRoot(result / (last - first));

	return result;
}

/// @overload
inline double standardDeviation(const double *values, int first, int last)
{
	double result = 0.0;
	double meanVal = mean(values, first, last);

	for (int i=first; i <= last; ++i) {
		result += pow((values[i] - meanVal), 2);
	}

	result = sqrt(result / (last - first));

	return result;
}

/**
 * Calculates the **standard deviation** for the given array of values. The standard
 * deviation is defined as the measure of how spread out the data is; the average
 * distance from the mean of a the data set to a point.
 *
 * @param values		The values to calculate the SD for.
 * @param size			The number of ``values``. If ``values`` is not large enough,
 * 					the behaviour is undefined.
 *
 * @return				The standard deviation of the given array.
 */
inline float standardDeviation(const float *values, int size)
{
	return standardDeviation(values, 0, size - 1);
}

/// @overload
inline double standardDeviation(const double *values, int size)
{
	return standardDeviation(values, 0, size - 1);
}

#ifdef __cplusplus
template<typename T, typename... Targs>
T standardDeviationX(const T &t, const Targs &... Fargs)
{
	unsigned int numOfArgs = static_cast<unsigned int>(sizeof...(Fargs)) + 1;

	T args[] = {t, static_cast<T>(Fargs)...};

	return standardDeviation(args, numOfArgs);
}
#endif // __cplusplus


/**
 * Calculates the **variance** of the data. This is defined as the spread of the
 * data, and is just the standard deviation squared.
 *
 * @param values		The array of values to calculate the variance for.
 * @param first		The index of the first value to consider in the range.
 * @param last			The index of the last value to consider in the range.
 *
 * @return				The variance.
 */
inline float variance(const float *values, int first, int last)
{
	return powf(standardDeviation(values, first, last), 2);
}

/// @overload
inline double variance(const double *values, int first, int last)
{
	return pow(standardDeviation(values, first, last), 2);
}

/**
 * Calculates the **variance** of the data. This is defined as the spread of the
 * data, and is just the standard deviation squared.
 *
 * @param values		The array of values to calculate the variance for.
 *
 * @param size 		The number of values. If ``values`` is not large enough,
 * 					the behaviour is undefined.
 *
 * @return				The variance.
 */
inline float variance(const float *values, int size)
{
	return variance(values, 0, size -1);
}

/// @overload
inline double variance(const double *values, int size)
{
	return variance(values, 0, size -1);
}

#ifdef __cplusplus
template<typename T, typename... Targs>
T varianceX(const T &t, const Targs &... Fargs)
{
	unsigned int numOfArgs = static_cast<unsigned int>(sizeof...(Fargs)) + 1;

	T args[] = {t, static_cast<T>(Fargs)...};

	return variance(args, numOfArgs);
}
#endif // __cplusplus


/**
 * Calculates the **covariance** relationship between 2 sets of values.
 *
 * @param x 		The first dimension of values to consider.
 *
 * @param y		The second dimension of values to consider.
 *
 * @param size		The number of values to consider from each array. Both ``x``
 * 				and ``y`` arrays must have at least be of ``size`` size,
 * 				or else behaviour is undefined.
 *
 * @return			The covariance value. A positive value indicates that both
 * 				dimensions increase together, a negative value indicates the
 * 				opposite, and a small value (~0) value indicates the two
 * 				dimensions are independent of each other.
 */
float covariance(const float *x, const float *y, int size);

/// @overload
double covariance(const double *x, const double *y, int size);

/// @overload
float covariance(const int *x, const int *y, int size);


#ifdef __cplusplus
/**
 * Calculates the covariance of a given variable-sized matrix.
 *
 * @tparam T			Specify the type of data each matrix member has. Typically ``float``/``double``.
 * @tparam Trows		Specify the number of rows that the matrix should have.
 * @tparam Tcolumns	Specify the number of columns that the matrix should have.
 * @param mat			The matrix to calculate the covariance of.
 *
 * @return				The covariance matrix.
 */
// template<typename T, int Trows, int Tcolumns>
// MatX<T, Tcolumns, Tcolumns> covariance(const MatX<T, Trows, Tcolumns> &mat)
// {
// 	// TODO: (sonictk) Use tutorial from http://www.cs.otago.ac.nz/cosc453/student_tutorials/principal_components.pdf instead.


// 	// NOTE: (sonictk) Implementation from: https://stackoverflow.com/questions/23301451/c-pca-calculating-covariance-matrix
// 	// TODO: (sonictk) This implementation seems to only work for square matrices
// 	// TODO: (sonictk) How does this math work?!
// 	MatX<T, Tcolumns, Tcolumns> result = matX<T, Tcolumns, Tcolumns>();

// 	for (int r=0; r < Trows; ++r) {
// 		T rowMean = mean(mat[r], 0, Trows - 1);

// 		T tmp[Tcolumns];
// 		for (int c=0; c < Tcolumns; ++c) {
// 			tmp[c] = mat[r][c] - rowMean;
// 		}

// 		// NOTE: (sonictk) Calculate outer product of tmp with itself
// 		MatX<T, Tcolumns, Tcolumns> t;
// 		for (int i=0; i < Tcolumns; ++i) {
// 			for (int j=0; j < Tcolumns; ++j) {
// 				t[i][j] = tmp[i] * tmp[j];
// 			}
// 		}

// 		result += t;
// 	}
// 	T factor = static_cast<T>(1) / (static_cast<T>(Trows) - static_cast<T>(1));
// 	result *= factor;

// 	return result;
// }
#endif // __cplusplus


#endif /* SS_STATISTICS_MATH_H */
