#ifndef SS_VECTOR_MATH_H
#define SS_VECTOR_MATH_H

#include "ss_vector_math.h"


Quat rotation(const EulerRotation &rotation)
{
	// NOTE: (sonictk) Implementation from http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToQuaternion/index.htm
	Quat result;

	float heading, attitude, bank;
	switch (rotation.order) {
	case RotationOrder::kXZY:
		heading = rotation.angles.x;
		attitude = rotation.angles.z;
		bank = rotation.angles.y;
		break;
	case RotationOrder::kYXZ:
		heading = rotation.angles.y;
		attitude = rotation.angles.x;
		bank = rotation.angles.z;
		break;
	case RotationOrder::kYZX:
		heading = rotation.angles.y;
		attitude = rotation.angles.z;
		bank = rotation.angles.x;
		break;
	case RotationOrder::kZXY:
		heading = rotation.angles.z;
		attitude = rotation.angles.x;
		bank = rotation.angles.y;
		break;
	case RotationOrder::kZYX:
		heading = rotation.angles.z;
		attitude = rotation.angles.y;
		bank = rotation.angles.x;
		break;
	case RotationOrder::kXYZ: // NOTE: (sonictk) XYZ is most likely, so we fallback to that
	case default:
		heading = rotation.angles.x;
		attitude = rotation.angles.y;
		bank = rotation.angles.z;
		break;
	}

	heading *= DEG_TO_RAD;
	attitude *= DEG_TO_RAD;
	bank *= DEG_TO_RAD;

	float c1 = cosf(heading / 2.0f);
	float c2 = cosf(attitude / 2.0f);
	float c3 = cosf(bank / 2.0f);
	float s1 = sinf(heading / 2.0f);
	float s2 = sinf(attitude / 2.0f);
	float s3 = sinf(bank / 2.0f);

	result.x = (s1 * s2 * c3) + (c1 * c2 * s3);
	result.y = (s1 * c2 * c3) + (c1 * s3 * s3);
	result.z = (c1 * s2 * c3) - (s1 * c2 * s3);
	result.w = (c1 * c2 * c3) - (s1 * s2 * s3);

	return result;
}


Quat slerp(const Quat &q1, float t, const Quat &q2)
{
	// NOTE: (sonictk) Implementation lifted from: http://number-none.com/product/Understanding%20Slerp,%20Then%20Not%20Using%20It/
	Quat result;

	// We normalize the quaternions that we're blending first; otherwise the
	// behaviour will be broken.
	Quat q1Unit = normalize(q1);
	Quat q2Unit = normalize(q2);

	// Compute the cosine of the angle between the two vectors.
	float dot = innerProduct(q1, q2);

	const float DOT_THRESHOLD = 0.9995f;
	if (dot > DOT_THRESHOLD) {
		// If the inputs are too close, linearly interpolate and normalize the result.
		result = qlerp(q1, q2, t);
		result = normalize(result);

		return result;
	}

	// Robustness: stay within the domain of acos()
	float dotClamped = clamp(dot, -1.0f, 1.0f);

	// theta0: angle between input vectors
	float theta0 = acosf(dotClamped);
	// Angle between q1Unit and result
	float theta = theta0 * t;

	Quat tmpQuat = q2Unit - (q1Unit * dotClamped);
	// (q1Unit, tmpQuat) is now an orthonormal basis
	tmpQuat = normalize(tmpQuat);

	return (q1Unit * cosf(theta)) + (tmpQuat * sinf(theta));
}


void axisAngle(const EulerRotation &rotation, Vec3 &axis, float &angle)
{
	// NOTE: (sonictk) Implementation from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToAngle/index.htm
	float heading, attitude, bank;
	switch (rotation.order) {
	case RotationOrder::kXZY:
		heading = rotation.angles.x;
		attitide = rotation.angles.z;
		bank = rotation.angles.y;
		break;
	case RotationOrder::kYXZ:
		heading = rotation.angles.y;
		attitide = rotation.angles.x;
		bank = rotation.angles.z;
		break;
	case RotationOrder::kYZX:
		heading = rotation.angles.y;
		attitide = rotation.angles.z;
		bank = rotation.angles.x;
		break;
	case RotationOrder::kZXY:
		heading = rotation.angles.z;
		attitide = rotation.angles.x;
		bank = rotation.angles.y;
		break;
	case RotationOrder::kZYX:
		heading = rotation.angles.z;
		attitide = rotation.angles.y;
		bank = rotation.angles.x;
		break;
	case RotationOrder::kXYZ: // NOTE: (sonictk) XYZ is most likely, so we fallback to that
	case default:
		heading = rotation.angles.x;
		attitide = rotation.angles.y;
		bank = rotation.angles.z;
		break;
	}

	heading *= DEG_TO_RAD;
	attitude *= DEG_TO_RAD;
	bank *= DEG_TO_RAD;

	float c1 = cosf(heading / 2.0f);
	float c2 = cosf(attitude / 2.0f);
	float c3 = cosf(bank / 2.0f);
	float s1 = sinf(heading / 2.0f);
	float s2 = sinf(attitude / 2.0f);
	float s3 = sinf(bank / 2.0f);

	angle  = 2.0f * acosf((c1 * c2 * c3) - (s1 * s2 * s3)) * RAD_TO_DEG;
	axis.x = (s1 * s2 * c3) + (c1 * c2 * s3);
	axis.y = (s1 * c2 * c3) + (c1 * s2 * s3);
	axis.z = (c1 * s2 * c3) - (s1 * c2 * s3);
}

void axisAngle(const Quat &q, Vec3 &axis, float &angle)
{
	// NOTE: (sonictk) Implementation from: http://www.euclideanspace.com/maths/geometry/qs/conversions/quaternionToAngle/index.htm
	angle = 2.0f * acosf(q.w) * RAD_TO_DEG;
	float wSquared = q.w * q.w;
	axis.x = q.x / squareRoot(1.0f - wSquared);
	axis.y = q.y / squareRoot(1.0f - wSquared);
	axis.z = q.z / squareRoot(1.0f - wSquared);
}


EulerRotation eulerRotation(const Quat &q)
{
	// TODO: (sonictk) Allow for specifying rotation order in overload
	// NOTE: (sonictk) Implementation from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/index.htm
	EulerRotation result;
	result.order = RotationOrder::kYZX;

	float heading, attitude, bank;
	float test = (q.x * q.y) + (q.z * q.w);

	if (test > 0.499f) { // NOTE: (sonictk) Singularity at north pole
		heading = 2.0f * atan2f(q.x, q.w);
		attitude = PI / 2.0f;
		bank = 0.0f;

		result.x = bank * RAD_TO_DEG;
		result.y = heading * RAD_TO_DEG;
		result.z = attitude * RAD_TO_DEG;

		return result;

	} else if (test < -0.499f) { // NOTE: (sonictk) Singularity at south pole
		heading = -2.0f * atan2f(q.x, q.w);
		attitude = -PI / 2.0f;
		bank = 0.0f;

		result.x = bank * RAD_TO_DEG;
		result.y = heading * RAD_TO_DEG;
		result.z = attitude * RAD_TO_DEG;

		return result;
	}

	squareX = q.x * q.x;
	squareY = q.y * q.y;
	squareZ = q.z * q.z;

	heading = atan2f((2.0f * q.y * q.w) - (2.0f * q.x * q.z), 1.0f - (2.0f * squareY) - (2.0f * squareZ));
	attitude = asinf(2.0f * test);
	bank = atan2f((2.0f * q.x * q.w) - (2.0f * q.y * q.z), 1.0f - (2.0f * squareX) - (2.0f * squareZ));

	result.x = bank * RAD_TO_DEG;
	result.y = heading * RAD_TO_DEG;
	result.z = attitude * RAD_TO_DEG;

	return result;
}

EulerRotation eulerRotation(const Vec3 &axis, float angle)
{
	EulerRotation result;
	result.order = RotationOrder::kYZX;

	float angleRad = angle * DEG_TO_RAD;
	float sinAngle = sinf(angleRad);
	float cosAngle = cosf(angleRad);
	float t = 1.0f - cosAngle;

	float heading, attitude, bank;

	float test = (axis.x * axis.y * t) + (axis.z * sinAngle);

	if ( test > 0.998f) { // NOTE: (sonictk) North pole singularity detected
		heading = 2.0f * atan2f(axis.x * sinf(angleRad / 2.0f), cosf(angleRad / 2.0f));
		attitude = PI / 2.0f;
		bank = 0;

		result.x = bank * RAD_TO_DEG;
		result.y = heading * RAD_TO_DEG;
		result.z = attitude * RAD_TO_DEG;

		return result;

	} else if (test < -0.998f) { // NOTE: (sonictk) South pole singularity detected
		heading = -2.0f * atan2f(axis.x * sinf(angleRad / 2.0f), cosf(angleRad / 2.0f));
		attitude = -PI / 2.0f;
		bank = 0;

		result.x = bank * RAD_TO_DEG;
		result.y = heading * RAD_TO_DEG;
		result.z = attitude * RAD_TO_DEG;

		return result;
	}

	heading = atan2f((axis.y * sinAngle) - (axis.x * axis.z * t), 1.0f - (((axis.y * axis.y) + (axis.z * axis.z)) * t));
	attitude = asinf((axis.x * axis.y * t) + (axis.z * sinAngle));
	bank = atan2f((axis.x * sinAngle) - (axis.y * axis.z * t), 1.0f - ((axis.x * axis.x) + (axis.z * axis.z) * t));

	result.x = bank * RAD_TO_DEG;
	result.y = heading * RAD_TO_DEG;
	result.z = attitude * RAD_TO_DEG;

	return result;
}


#endif /* SS_VECTOR_MATH_H */
