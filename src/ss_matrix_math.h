/**
 * @file   ss_matrix_math.h
 * @brief  	Simple library for basic matrix types. Intended to be used with
 * 			the simple vector types defined in ``ss_vector_math.h``.
 *
 * 			NOTE: Unless otherwise stated, this API is intended to be used with
 * 			OpenGL-style matrix notation; which have translation values in
 * 			the 13th, 14th and 15th indices, and coordinates in right-handed
 * 			space. (Normalized Device Coordinates are in left-handed space.)
 *				Additionally, all rotation inputs should be in *degrees*;
 * 			conversion to radians should be done internally within functions.
 */
#ifndef SS_MATRIX_MATH_H
#define SS_MATRIX_MATH_H

#include "ss_vector_math.h"


#ifdef __cplusplus

#include <cmath>

#else

#include <math.h>

#endif // __cplusplus


/// A simple 3x3 matrix data structure.
/// Example layout:
/// 		1  0  0
/// 		0  1  0
/// 		0  0  1
///
/// Values are stored in **floats**.
struct Mat33
{
	float e[3][3];

	inline float *operator[](int row) {
		return e[row];
	}

	inline const float *operator[](int row) const {
		return e[row];
	}

	inline const float &operator()(int row, int col) const {
		return e[row][col];
	}

	inline float &operator()(int row, int col) {
		return e[row][col];
	}
};

typedef Mat33 RotationMatrix;

/**
 * Creates a new 3x3 matrix with the provided values.
 *
 * @return	The new matrix.
 */
inline Mat33 mat33(const float vals[3][3])
{
	Mat33 mat = {
		{{vals[0][0], vals[0][1], vals[0][2]},
		 {vals[1][0], vals[0][1], vals[0][2]},
		 {vals[2][0], vals[0][1], vals[0][2]}}
	};

	return mat;
}

/**
 * Creates a new 3x3 matrix with the provided values in row-major order.
 *
 * @return	The new matrix.
 */
inline Mat33 mat33(const int vals[3][3])
{
	Mat33 mat = {
		{{(float)vals[0][0], (float)vals[0][1], (float)vals[0][2]},
		 {(float)vals[1][0], (float)vals[0][1], (float)vals[0][2]},
		 {(float)vals[2][0], (float)vals[0][1], (float)vals[0][2]}}
	};

	return mat;
}

/**
 * Creates a new 3x3 matrix with the provided values. The matrix will be created
 * using the values in the following indexes:
 * 	0   1   2
 * 	3   4   5
 * 	6   7   8
 *
 * @param vals		The values to use for constructing the matrix with.
 *
 * @return			The new matrix.
 */
inline Mat33 mat33(const float vals[9])
{
	Mat33 mat = {
		{{vals[0], vals[1], vals[2]},
		 {vals[3], vals[4], vals[5]},
		 {vals[6], vals[7], vals[8]}}
	};

	return mat;
}

/// @overload
inline Mat33 mat33(float a, float b, float c,
				   float d, float e, float f,
				   float g, float h, float i)
{
	Mat33 mat = {
		{{a, b, c},
		 {d, e, f},
		 {g, h, i}}
	};

	return mat;
}

/// @overload
inline Mat33 mat33(double a, double b, double c,
				   double d, double e, double f,
				   double g, double h, double i)
{
	return mat33((float)a, (float)b, (float)c,
				 (float)d, (float)e, (float)f,
				 (float)g, (float)h, (float)i);
}

/// @overload
inline Mat33 mat33(int a, int b, int c,
				   int d, int e, int f,
				   int g, int h, int i)
{
	return mat33((float)a, (float)b, (float)c,
				 (float)d, (float)e, (float)f,
				 (float)g, (float)h, (float)i);
}

/// @overload
inline Mat33 mat33(unsigned int a, unsigned int b, unsigned int c,
				   unsigned int d, unsigned int e, unsigned int f,
				   unsigned int g, unsigned int h, unsigned int i)
{
	return mat33((float)a, (float)b, (float)c,
				 (float)d, (float)e, (float)f,
				 (float)g, (float)h, (float)i);
}

inline Mat33 operator+(const Mat33 &a, const Mat33 &b)
{
	Mat33 result = {
		{{a(0, 0) + b(0, 0), a(0, 1) + b(0, 1), a(0, 2) + b(0, 2)},
		 {a(1, 0) + b(1, 0), a(1, 1) + b(1, 1), a(1, 2) + b(1, 2)},
		 {a(2, 0) + b(2, 0), a(2, 1) + b(2, 1), a(2, 2) + b(2, 2)}}
	};

	return result;
}

/**
 * Copies a new 3x3 matrix from an existing one.
 *
 * @param mat		The existing matrix to copy..
 *
 * @return			A copy of an existing matrix.
 */
inline Mat33 mat33(const Mat33 &mat)
{
	Mat33 result = {
		{{mat[0][0], mat[0][1], mat[0][2]},
		 {mat[1][0], mat[1][1], mat[1][2]},
		 {mat[2][0], mat[2][1], mat[2][2]}}
	};

	return result;
}

inline Mat33 &operator+=(Mat33 &a, const Mat33 &b)
{
	a = a + b;
	return a;
}

inline Mat33 operator-(const Mat33 &a, const Mat33 &b)
{
	Mat33 result = {
		{{a(0, 0) - b(0, 0), a(0, 1) - b(0, 1), a(0, 2) - b(0, 2)},
		 {a(1, 0) - b(1, 0), a(1, 1) - b(1, 1), a(1, 2) - b(1, 2)},
		 {a(2, 0) - b(2, 0), a(2, 1) - b(2, 1), a(2, 2) - b(2, 2)}}
	};

	return result;
}

inline Mat33 &operator-=(Mat33 &a, const Mat33 &b)
{
	a = a - b;
	return a;
}

inline Mat33 operator*(const Mat33 &mat, float factor)
{
	Mat33 result = {
		{{mat(0, 0) * factor, mat(0, 1) * factor, mat(0, 2) * factor},
		 {mat(1, 0) * factor, mat(1, 1) * factor, mat(1, 2) * factor},
		 {mat(2, 0) * factor, mat(2, 1) * factor, mat(2, 2) * factor}}
	};

	return result;
}

inline Mat33 operator*(const Mat33 &a, const Mat33 &b)
{
	Mat33 result = {};
	for (int r=0; r < 3; ++r) {
		for (int c=0; c < 3; ++c) {
			for (int i=0; i <= 3; ++i) {
				result[r][c] += a(r, i) * b(i, c);
			}
		}
	}

	return result;
}

inline Mat33 &operator*=(Mat33 &mat, float factor)
{
	mat = mat * factor;
	return mat;
}

inline Mat33 &operator*=(Mat33 &a, const Mat33 &b)
{
	a = a * b;
	return a;
}

inline Vec3 operator*(const Mat33 &mat, Vec3 vec)
{
	Vec3 result;

	result.x = (vec.x * mat[0][0]) + (vec.y * mat[0][1]) + (vec.z * mat[0][2]);
	result.y = (vec.x * mat[1][0]) + (vec.y * mat[1][1]) + (vec.z * mat[1][2]);
	result.z = (vec.x * mat[2][0]) + (vec.y * mat[2][1]) + (vec.z * mat[2][2]);

	return result;
}

inline Mat33 operator/(const Mat33 &mat, float factor)
{
	Mat33 result = {
		{{mat(0, 0) / factor, mat(0, 1) / factor, mat(0, 2) / factor},
		 {mat(1, 0) / factor, mat(1, 1) / factor, mat(1, 2) / factor},
		 {mat(2, 0) / factor, mat(2, 1) / factor, mat(2, 2) / factor}}
	};

	return result;
}

inline Mat33 &operator/=(Mat33 &mat, float factor)
{
	mat = mat / factor;
	return mat;
}

inline bool operator==(const Mat33 &mat1, const Mat33 &mat2)
{
	for (int r=0; r < 3; ++r) {
		for (int c=0; c < 3; ++c) {
			if (!areFloatsEqual(mat1[r][c], mat2[r][c])) {
				return false;
			}
		}
	}

	return true;
}

inline bool operator!=(const Mat33 &mat1, const Mat33 &mat2)
{
	return !(mat1 == mat2);
}

/**
 * Returns the transposed version of the matrix.
 *
 * @param mat	The matrix to transpose.
 *
 * @return		A transposed version of the matrix.
 */
inline Mat33 transpose(const Mat33 &mat)
{
	Mat33 result;
	for (int j=0; j < 3; ++j) {
		for (int i=0; i < 3; ++i) {
			result[j][i] = mat(i, j);
		}
	}

	return result;
}

/**
 * Retrieves the values from the given 3x3 matrix in row-column order to an array of floats.
 *
 * @param mat 		The matrix to retrieve the values for.
 * @param f 		Storage for the matrix's values.
 * @param size 	The size of the storage. Must be at least 9 wide to retrieve all values.
 */
inline void getValues(const Mat33 &mat, float *f, int size)
{
	int i = 0;
	while (i < size && i < 9) {
		for (int r=0; r < 3; ++r) {
			for (int c=0; c < 3; ++c) {
				f[i] = mat[r][c];
				++i;
			}
		}
	}
}

/**
 * Generates a rotation matrix in the X-axis. Rotation order is assumed as ``ZYX``.
 *
 * @param angle	The angle of rotation.
 *
 * @return			The rotation matrix.
 */
inline Mat33 xRotation33(float angle)
{
	float angleRad = angle * PI / 180.0f;
	float c = cosf(angleRad);
	float s = sinf(angleRad);

	Mat33 result = {
		{{1, 0, 0},
		 {0, c, -s},
		 {0, s, c}}
	};

	return result;
}

/**
 * Generates a rotation matrix in the Y-axis. Rotation order is assumed as ``ZYX``.
 *
 * @param angle	The angle of rotation.
 *
 * @return			The rotation matrix.
 */
inline Mat33 yRotation33(float angle)
{
	float angleRad = angle * PI / 180.0f;
	float c = cosf(angleRad);
	float s = sinf(angleRad);

	Mat33 result = {
		{{c, 0, s},
		 {0, 1, 0},
		 {-s, 0, c}}
	};

	return result;
}

/**
 * Generates a rotation matrix in the Z-axis. Rotation order is assumed as ``ZYX``.
 *
 * @param angle	The angle of rotation.
 *
 * @return			The rotation matrix.
 */
inline Mat33 zRotation33(float angle)
{
	float angleRad = angle * PI / 180.0f;
	float c = cosf(angleRad);
	float s = sinf(angleRad);

	Mat33 result = {
		{{c, -s, 0},
		 {s, c, 0},
		 {0, 0, 1}}
	};

	return result;
}


/// A simple 4x4 matrix data structure. These are assumed to be **row-major** by
/// convention and memory layout follows **OpenGL** standards.
/// Example layout:
/// 		1  0  0  tX
/// 		0  1  0  tY
/// 		0  0  1  tZ
/// 		0  0  0  1
///
/// DirectX has the layout transposed:
/// 		1  0  0  0
/// 		0  1  0  0
/// 		0  0  1  0
/// 		tX tY tZ 1
///
/// Values are stored in **floats**.
struct Mat44
{
	float e[4][4];

	inline float *operator[](int row) {
		return e[row];
	}

	inline const float *operator[](int row) const {
		return e[row];
	}

	inline const float &operator()(int row, int col) const {
		return e[row][col];
	}

	inline float &operator()(int row, int col) {
		return e[row][col];
	}
};

/// This should be used for matrices with OpenGL memory layout.
typedef Mat44 OpenGLMatrix;
typedef OpenGLMatrix OGLMat;

/// This should be used for matrices with DirectX memory layout (i.e. the transposed
/// version of the OpenGL matrix memory layout).
typedef Mat44 DirectXMatrix;
typedef DirectXMatrix DXMat;

typedef Mat44 TransformationMatrix;

/**
 * Creates a new 4x4 matrix with the provided values in row-major order.
 *
 * @return	The new matrix.
 */
inline Mat44 mat44(const float vals[4][4])
{
	Mat44 mat = {
		{{vals[0][0], vals[0][1], vals[0][2], vals[0][3]},
		 {vals[1][0], vals[0][1], vals[0][2], vals[0][3]},
		 {vals[2][0], vals[0][1], vals[0][2], vals[0][3]},
		 {vals[3][0], vals[0][1], vals[0][2], vals[0][3]}}
	};

	return mat;
}

inline Mat44 mat44(const double vals[4][4])
{
	Mat44 mat = {
		{{(float)vals[0][0], (float)vals[0][1], (float)vals[0][2], (float)vals[0][3]},
		 {(float)vals[1][0], (float)vals[0][1], (float)vals[0][2], (float)vals[0][3]},
		 {(float)vals[2][0], (float)vals[0][1], (float)vals[0][2], (float)vals[0][3]},
		 {(float)vals[3][0], (float)vals[0][1], (float)vals[0][2], (float)vals[0][3]}}
	};

	return mat;
}

/// @overload
inline Mat44 mat44(const int vals[4][4])
{
	Mat44 mat = {
		{{(float)vals[0][0], (float)vals[0][1], (float)vals[0][2], (float)vals[0][3]},
		 {(float)vals[1][0], (float)vals[0][1], (float)vals[0][2], (float)vals[0][3]},
		 {(float)vals[2][0], (float)vals[0][1], (float)vals[0][2], (float)vals[0][3]},
		 {(float)vals[3][0], (float)vals[0][1], (float)vals[0][2], (float)vals[0][3]}}
	};

	return mat;
}

inline Mat44 mat44(const unsigned int vals[4][4])
{
	Mat44 mat = {
		{{(float)vals[0][0], (float)vals[0][1], (float)vals[0][2], (float)vals[0][3]},
		 {(float)vals[1][0], (float)vals[0][1], (float)vals[0][2], (float)vals[0][3]},
		 {(float)vals[2][0], (float)vals[0][1], (float)vals[0][2], (float)vals[0][3]},
		 {(float)vals[3][0], (float)vals[0][1], (float)vals[0][2], (float)vals[0][3]}}
	};

	return mat;
}

/**
 * Creates a new 4x4 matrix with the provided values. The matrix will be created
 * using the values in the following indexes:
 * 	0   1   2   3
 * 	4   5   6   7
 * 	8   9   10  11
 * 	12  13  14  15
 *
 * @param vals		The values to use for constructing the matrix with.
 *
 * @return			The new matrix.
 */
inline Mat44 mat44(const float vals[16])
{
	Mat44 mat = {
		{{vals[0], vals[1], vals[2], vals[3]},
		 {vals[4], vals[5], vals[6], vals[7]},
		 {vals[8], vals[9], vals[10], vals[11]},
		 {vals[12], vals[13], vals[14], vals[15]}}
	};

	return mat;
}

/// @overload
inline Mat44 mat44(const double vals[16])
{
	Mat44 mat = {
		{{(float)vals[0], 		(float)vals[1], 	(float)vals[2], 	(float)vals[3]},
		 {(float)vals[4], 		(float)vals[5], 	(float)vals[6], 	(float)vals[7]},
		 {(float)vals[8], 		(float)vals[9], 	(float)vals[10], 	(float)vals[11]},
		 {(float)vals[12], 	(float)vals[13], 	(float)vals[14], 	(float)vals[15]}}
	};

	return mat;
}

/// @overload
inline Mat44 mat44(const int vals[16])
{
	Mat44 mat = {
		{{(float)vals[0], 		(float)vals[1], 	(float)vals[2], 	(float)vals[3]},
		 {(float)vals[4], 		(float)vals[5], 	(float)vals[6], 	(float)vals[7]},
		 {(float)vals[8], 		(float)vals[9], 	(float)vals[10], 	(float)vals[11]},
		 {(float)vals[12], 	(float)vals[13], 	(float)vals[14], 	(float)vals[15]}}
	};

	return mat;
}

/// @overload
inline Mat44 mat44(const unsigned int vals[16])
{
	Mat44 mat = {
		{{(float)vals[0], 		(float)vals[1], 	(float)vals[2], 	(float)vals[3]},
		 {(float)vals[4], 		(float)vals[5], 	(float)vals[6], 	(float)vals[7]},
		 {(float)vals[8], 		(float)vals[9], 	(float)vals[10], 	(float)vals[11]},
		 {(float)vals[12], 	(float)vals[13], 	(float)vals[14], 	(float)vals[15]}}
	};

	return mat;
}

/// @overload
inline Mat44 mat44(const float a, const float b, const float c, const float d,
				   const float e, const float f, const float g, const float h,
				   const float i, const float j, const float k, const float l,
				   const float m, const float n, const float o, const float p)
{
	Mat44 mat = {
		{{a, b, c, d},
		 {e, f, g, h},
		 {i, j, k, l},
		 {m, n, o, p}}
	};

	return mat;
}

/// @overload
inline Mat44 mat44(const double a, const double b, const double c, const double d,
				   const double e, const double f, const double g, const double h,
				   const double i, const double j, const double k, const double l,
				   const double m, const double n, const double o, const double p)
{
	Mat44 mat = {
		{{(float)a, (float)b, (float)c, (float)d},
		 {(float)e, (float)f, (float)g, (float)h},
		 {(float)i, (float)j, (float)k, (float)l},
		 {(float)m, (float)n, (float)o, (float)p}}
	};

	return mat;
}

/// @overload
inline Mat44 mat44(const int a, const int b, const int c, const int d,
				   const int e, const int f, const int g, const int h,
				   const int i, const int j, const int k, const int l,
				   const int m, const int n, const int o, const int p)
{
	Mat44 mat = {
		{{(float)a, (float)b, (float)c, (float)d},
		 {(float)e, (float)f, (float)g, (float)h},
		 {(float)i, (float)j, (float)k, (float)l},
		 {(float)m, (float)n, (float)o, (float)p}}
	};

	return mat;
}

/// @overload
inline Mat44 mat44(const unsigned int a, const unsigned int b, const unsigned int c, const unsigned int d,
				   const unsigned int e, const unsigned int f, const unsigned int g, const unsigned int h,
				   const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l,
				   const unsigned int m, const unsigned int n, const unsigned int o, const unsigned int p)
{
	Mat44 mat = {
		{{(float)a, (float)b, (float)c, (float)d},
		 {(float)e, (float)f, (float)g, (float)h},
		 {(float)i, (float)j, (float)k, (float)l},
		 {(float)m, (float)n, (float)o, (float)p}}
	};

	return mat;
}

/**
 * Copies a new 4x4 matrix from an existing one.
 *
 * @param mat		The existing matrix to copy..
 *
 * @return			A copy of an existig matrix.
 */
inline Mat44 mat44(const Mat44 &mat)
{
	Mat44 result = {
		{{mat[0][0], mat[0][1], mat[0][2], mat[0][3]},
		 {mat[1][0], mat[1][1], mat[1][2], mat[1][3]},
		 {mat[2][0], mat[2][1], mat[2][2], mat[2][3]},
		 {mat[3][0], mat[3][1], mat[3][2], mat[3][3]}}
	};

	return result;
}

/**
 * Creates a new 4x4 identity matrix.
 *
 * @return	The identity matrix.
 */
inline Mat44 mat44()
{
	Mat44 mat = {
		{{1, 0, 0, 0},
		 {0, 1, 0, 0},
		 {0, 0, 1, 0},
		 {0, 0, 0, 1}}
	};

	return mat;
}

/**
 * Gives an identity matrix.
 *
 * @return 	The identity matrix.
 */
inline Mat44 identityMat44()
{
	return mat44();
}


/**
 * Retrieves the values from the given 4x4 matrix in row-column order to an array of floats.
 *
 * @param mat 		The matrix to retrieve the values for.
 * @param f 		Storage for the matrix's values.
 * @param size 	The size of the storage. Must be at least 16 wide to retrieve all values.
 */
inline void getValues(const Mat44 &mat, float *f, int size)
{
	int i = 0;
	while (i < size && i < 16) {
		for (int r=0; r <= 3; ++r) {
			for (int c=0; c <= 3; ++c) {
				f[i] = mat[r][c];
				++i;
			}
		}
	}
}

inline Mat44 operator+(const Mat44 &a, const Mat44 &b)
{
	Mat44 result = {
		{{a(0, 0) + b(0, 0), a(0, 1) + b(0, 1), a(0, 2) + b(0, 2), a(0, 3) + b(0, 3)},
		 {a(1, 0) + b(1, 0), a(1, 1) + b(1, 1), a(1, 2) + b(1, 2), a(1, 3) + b(1, 3)},
		 {a(2, 0) + b(2, 0), a(2, 1) + b(2, 1), a(2, 2) + b(2, 2), a(2, 3) + b(2, 3)},
		 {a(3, 0) + b(3, 0), a(3, 1) + b(3, 1), a(3, 2) + b(3, 2), a(3, 3) + b(3, 3)}}
	};

	return result;
}

inline Mat44 &operator+=(Mat44 &a, const Mat44 &b)
{
	a = a + b;
	return a;
}

inline Mat44 operator-(const Mat44 &a, const Mat44 &b)
{
	Mat44 result = {
		{{a(0, 0) - b(0, 0), a(0, 1) - b(0, 1), a(0, 2) - b(0, 2), a(0, 3) - b(0, 3)},
		 {a(1, 0) - b(1, 0), a(1, 1) - b(1, 1), a(1, 2) - b(1, 2), a(1, 3) - b(1, 3)},
		 {a(2, 0) - b(2, 0), a(2, 1) - b(2, 1), a(2, 2) - b(2, 2), a(2, 3) - b(2, 3)},
		 {a(3, 0) - b(3, 0), a(3, 1) - b(3, 1), a(3, 2) - b(3, 2), a(3, 3) - b(3, 3)}}
	};

	return result;
}

inline Mat44 &operator-=(Mat44 &a, const Mat44 &b)
{
	a = a - b;
	return a;
}

inline Mat44 operator*(const Mat44 &mat, float factor)
{
	Mat44 result = {
		{{mat(0, 0) * factor, mat(0, 1) * factor, mat(0, 2) * factor, mat(0, 3) * factor},
		 {mat(1, 0) * factor, mat(1, 1) * factor, mat(1, 2) * factor, mat(1, 3) * factor},
		 {mat(2, 0) * factor, mat(2, 1) * factor, mat(2, 2) * factor, mat(2, 3) * factor},
		 {mat(3, 0) * factor, mat(3, 1) * factor, mat(3, 2) * factor, mat(3, 3) * factor}}
	};

	return result;
}

inline Mat44 operator*(const Mat44 &a, const Mat44 &b)
{
	Mat44 result = {};
	for (int r=0; r <= 3; ++r) {
		for (int c=0; c <=3; ++c) {
			for (int i=0; i <= 3; ++i) {
				result[r][c] += a(r, i) * b(i, c);
			}
		}
	}

	return result;
}

inline Mat44 &operator*=(Mat44 &mat, float factor)
{
	mat = mat * factor;
	return mat;
}

inline Mat44 &operator*=(Mat44 &a, const Mat44 &b)
{
	a = a * b;
	return a;
}

inline Vec4 operator*(const Mat44 &mat, Vec4 vec)
{
	Vec4 result;

	result.x = (vec.x * mat[0][0]) + (vec.y * mat[0][1]) + (vec.z * mat[0][2]) + (vec.w * mat[0][3]);
	result.y = (vec.x * mat[1][0]) + (vec.y * mat[1][1]) + (vec.z * mat[1][2]) + (vec.w * mat[1][3]);
	result.z = (vec.x * mat[2][0]) + (vec.y * mat[2][1]) + (vec.z * mat[2][2]) + (vec.w * mat[2][3]);
	result.w = (vec.x * mat[3][0]) + (vec.y * mat[3][1]) + (vec.z * mat[3][2]) + (vec.w * mat[3][3]);

	return result;
}

inline Vec3 operator*(const Mat44 &mat, Vec3 vec)
{
	Vec3 result = (mat * vec4(vec, 1.0f)).xyz;
	return result;
}

inline Mat44 operator/(const Mat44 &mat, float factor)
{
	Mat44 result = {
		{{mat(0, 0) / factor, mat(0, 1) / factor, mat(0, 2) / factor, mat(0, 3) / factor},
		 {mat(1, 0) / factor, mat(1, 1) / factor, mat(1, 2) / factor, mat(1, 3) / factor},
		 {mat(2, 0) / factor, mat(2, 1) / factor, mat(2, 2) / factor, mat(2, 3) / factor},
		 {mat(3, 0) / factor, mat(3, 1) / factor, mat(3, 2) / factor, mat(3, 3) / factor}}
	};

	return result;
}

inline Mat44 &operator/=(Mat44 &mat, float factor)
{
	mat = mat / factor;
	return mat;
}

inline bool operator==(const Mat44 &mat1, const Mat44 &mat2)
{
	for (int r=0; r <= 3; ++r) {
		for (int c=0; c <= 3; ++c) {
			if (!areFloatsEqual(mat1[r][c], mat2[r][c])) {
				return false;
			}
		}
	}
	return true;
}

inline bool operator!=(const Mat44 &mat1, const Mat44 &mat2)
{
	return !(mat1 == mat2);
}


/**
 * Returns the transposed version of the matrix.
 *
 * @param mat	The matrix to transpose.
 *
 * @return		A transposed version of the matrix.
 */
inline Mat44 transpose(const Mat44 &mat)
{
	Mat44 result;
	for (int j=0; j <= 3; ++j) {
		for (int i=0; i <= 3; ++i) {
			result[j][i] = mat(i, j);
		}
	}

	return result;
}


/**
 * Translates the given matrix by the given ``translation``.
 * This is for **row-major** matrices (i.e. XYZ stored in the 4th/8th/12th indices).
 *
 * @param mat			The matrix to apply the translation to.
 * @param translation	The translation to apply.
 *
 * @return				The translated matrix.
 */
inline void translateBy(Mat44 &mat, const Vec3 &translation)
{
	mat[0][3] += translation.x;
	mat[1][3] += translation.y;
	mat[2][3] += translation.z;
}


/**
 * Retrieves the translation from the given transformation matrix.
 *
 * @param mat	The matrix to get the translation for.
 *
 * @return		The translation as x, y and z components of a vector.
 */
inline Vec3 getTranslation(const Mat44 &mat)
{
	Vec3 result = {mat[0][3], mat[1][3], mat[2][3]};

	return result;
}

/**
 * Retrieves the translation from the given transformation matrix.
 *
 * @param mat			The matrix to get the translation for.
 * @param translation	The storage for the retrieved translation stored as XYZ values.
 */
inline void getTranslation(const Mat44 &mat, float translation[3])
{
	for (int i=0; i < 3; ++i) {
		translation[i] = mat[i][3];
	}
}

/**
 * Sets the translation on the given transformation matrix.
 *
 * @param mat	The matrix to set the translation for.
 *
 * @return		The translation as x, y and z components of a vector.
 */
inline void setTranslation(Mat44 &mat, const Vec3 &translation)
{
	mat[0][3] = translation.x;
	mat[1][3] = translation.y;
	mat[2][3] = translation.z;
}

/**
 * Sets the translation on the given transformation matrix.
 *
 * @param mat			The matrix to set the translation for.
 * @param translation	The translation to set as x, y and z components.
 */
inline void setTranslation(Mat44 &mat, const float translation[3])
{
	for (int i=0; i < 3; ++i) {
		mat[i][3] = translation[i];
	}
}

#ifdef __cplusplus

template<typename T>
inline void setTranslation(Mat44 &mat, const T translation[3])
{
	setTranslation(mat, static_cast<float *>(translation));
}

#endif // __cplusplus


/**
 * Neutralizes translation in the given transformation matrix.
 *
 * @param mat	The matrix to set translation values for.
 */
inline void clearTranslation(Mat44 &mat)
{
	float setter[3] = {0.0f};
	setTranslation(mat, setter);
}


/**
 * Translates the given matrix by the given ``x``, ``y`` and ``z`` values.
 * This is for **row-major** matrices (i.e. XYZ stored in the 4th/8th/12th indices
 * for a matrix with indices starting from 1 and incrementing left-to-right).
 *
 * @param mat			The matrix to apply the translation to.
 * @param x			The translation to apply in the X-axis.
 * @param y			The translation to apply in the Y-axis.
 * @param z			The translation to apply in the Z-axis.
 *
 * @return				The translated matrix.
 */
inline void translateBy(Mat44 &mat, const float x, const float y, const float z)
{
	mat[0][3] += x;
	mat[1][3] += y;
	mat[2][3] += z;
}

/// @overload
inline void translateBy(Mat44 &mat, const float vals[3])
{
	translateBy(mat, vals[0], vals[1], vals[2]);
}

/// @overload
inline void translateBy(Mat44 &mat, const double x, const double y, const double z)
{
	mat[0][3] += (float)x;
	mat[1][3] += (float)y;
	mat[2][3] += (float)z;
}

/// @overload
inline void translateBy(Mat44 &mat, const double vals[3])
{
	translateBy(mat, vals[0], vals[1], vals[2]);
}

/// @overload
inline void translateBy(Mat44 &mat, const int x, const int y, const int z)
{
	mat[0][3] += (float)x;
	mat[1][3] += (float)y;
	mat[2][3] += (float)z;
}

/// @overload
inline void translateBy(Mat44 &mat, const int vals[3])
{
	translateBy(mat, vals[0], vals[1], vals[2]);
}

/// @overload
inline void translateBy(Mat44 &mat, const unsigned int x, const unsigned int y, const unsigned int z)
{
	mat[0][3] += (float)x;
	mat[1][3] += (float)y;
	mat[2][3] += (float)z;
}

/// @overload
inline void translateBy(Mat44 &mat, const unsigned int vals[3])
{
	translateBy(mat, vals[0], vals[1], vals[2]);
}


/**
 * Generates a rotation matrix in the X-axis.
 *
 * @param angle	The angle of rotation.
 *
 * @return			The rotation matrix.
 */
inline Mat44 xRotation(float angle)
{
	float angleRad = angle * PI / 180.0f;
	float c = cosf(angleRad);
	float s = sinf(angleRad);

	Mat44 result = {
		{{1, 0, 0, 0},
		 {0, c, -s, 0},
		 {0, s, c, 0},
		 {0, 0, 0, 1}}
	};

	return result;
}

/**
 * Generates a rotation matrix in the Y-axis.
 *
 * @param angle	The angle of rotation.
 *
 * @return			The rotation matrix.
 */
inline Mat44 yRotation(float angle)
{
	float angleRad = angle * PI / 180.0f;
	float c = cosf(angleRad);
	float s = sinf(angleRad);

	Mat44 result = {
		{{c, 0, s, 0},
		 {0, 1, 0, 0},
		 {-s, 0, c, 0},
		 {0, 0, 0, 1}}
	};

	return result;
}

/**
 * Generates a rotation matrix in the Z-axis.
 *
 * @param angle	The angle of rotation.
 *
 * @return			The rotation matrix.
 */
inline Mat44 zRotation(float angle)
{
	float angleRad = angle * PI / 180.0f;
	float c = cosf(angleRad);
	float s = sinf(angleRad);

	Mat44 result = {
		{{c, -s, 0, 0},
		 {s, c, 0, 0},
		 {0, 0, 1, 0},
		 {0, 0, 0, 1}}
	};

	return result;
}

#ifdef __cplusplus
/// @overload
template<typename T>
inline Mat44 xRotation(T angle)
{
	return xRotation(static_cast<float>(angle));
}

/// @overload
template<typename T>
inline Mat44 yRotation(T angle)
{
	return yRotation(static_cast<float>(angle));
}

/// @overload
template<typename T>
inline Mat44 zRotation(T angle)
{
	return zRotation(static_cast<float>(angle));
}
#endif // __cplusplus


/**
 * Creates a 3x3 rotation matrix.
 *
 * @param axis 		The **normalized** axis for rotation.
 * @param angle 		The amount to rotate, in **degrees**.
 *
 * @return				The rotation matrix.
 */
Mat33 rotationMatrix(const Vec3 &axis, float angle);

/**
 * Creates a 3x3 rotation matrix.
 *
 * @param q 	The **normalized** quaternion that describes the rotation from origin.
 *
 * @return 	The rotation matrix.
 */
Mat33 rotationMatrix(const Quat &q);

/// @overload
Mat33 rotationMatrix(const EulerRotation &rotation);


/**
 * Creates a 4x4 rotation matrix.
 *
 * @param axis 		The **normalized** axis for rotation.
 * @param angle 		The amount to rotate, in **degrees**.
 *
 * @return				The rotation matrix.
 */
Mat44 rotationMatrixMat44(const Vec3 &axis, float angle);

/**
 * Creates a 4x4 rotation matrix.
 *
 * @param q 	The **normalized** quaternion that describes the rotation from origin.
 *
 * @return 	The rotation matrix.
 */
Mat44 rotationMatrixMat44(const Quat &q);

/// @overload
Mat44 rotationMatrixMat44(const EulerRotation &rotation);

/// @overload
Mat44 rotationMatrixMat44(const float rotation[3], const RotationOrder order);


/**
 * Rotates the given transformation matrix by the specified axis-angle rotation.
 *
 * @param mat		The 4x4 transformation matrix to apply the rotation to, **in-place**.
 * @param axis		The axis in which the rotation should be applied around.
 * @param angle	The amount of rotation in **degrees** to apply.
 */
void rotateBy(Mat44 &mat, const Vec3 &axis, const float angle);

/// @overload
void rotateBy(Mat44 &mat, const Quat &rotation);

/// @overload
void rotateBy(Mat44 &mat, const EulerRotation &rotation);

/// @overload
void rotateBy(Mat44 &mat, const float rotation[3], const RotationOrder order);


/**
 * Scales the given transformation matrix by the axis factors.
 *
 * @param mat 	The matrix to scale **in-place**.
 * @param x 	The amount to scale in X.
 * @param y 	The amount to scale in Y.
 * @param z 	The amount to scale in Z.
 */
inline void scaleBy(Mat44 &mat, float x, float y, float z)
{
	Mat44 scaleMatrix = mat44();
	scaleMatrix[0][0] = x;
	scaleMatrix[1][1] = y;
	scaleMatrix[2][2] = z;
	mat *= scaleMatrix;
}

/// @overload
inline void scaleBy(Mat44 &mat, const float vals[3])
{
	scaleBy(mat, vals[0], vals[1], vals[2]);
}

/// @overload
inline void scaleBy(Mat44 &mat, const Vec3 &scale)
{
	scaleBy(mat, scale.x, scale.y, scale.z);
}

/// @overload
inline void scaleBy(Mat44 &mat, const float factor)
{
	scaleBy(mat, factor, factor, factor);
}

/// @overload
inline void scaleBy(Mat44 &mat, const double factor)
{
	scaleBy(mat, (float)factor);
}

/// @overload
inline void scaleBy(Mat44 &mat, const int factor)
{
	scaleBy(mat, (float)factor);
}

/// @overload
inline void scaleBy(Mat44 &mat, const unsigned int factor)
{
	scaleBy(mat, (float)factor);
}


/**
 * Retrieves the scale for the given transformation matrix.
 *
 * @param mat		The transformation matrix to get the scale for. This assumes
 * 				that ``mat``is an affine transformation matrix with no shear.
 *
 * @param scale	The storage for the scale retrieved.
 */
inline void getScale(const Mat44 &mat, float scale[3])
{
	Vec3 basisVecX = vec3(mat[0][0], mat[0][1], mat[0][2]);
	Vec3 basisVecY = vec3(mat[1][0], mat[1][1], mat[1][2]);
	Vec3 basisVecZ = vec3(mat[2][0], mat[2][1], mat[2][2]);

	scale[0] = length(basisVecX);
	scale[1] = length(basisVecY);
	scale[2] = length(basisVecZ);
}

/**
 * Retrieves the scale for the given transformation matrix.
 *
 * @param mat		The transformation matrix to get the scale for. This assumes
 * 				that ``mat``is an affine transformation matrix with no shear.
 *
 * @return			The scale as x, y and z components of a vector.
 */
inline Vec3 getScale(const Mat44 &mat)
{
	float scale[3];
	getScale(mat, scale);
	Vec3 result = vec3(scale);

	return result;
}


/**
 * Neutralizes/removes scale from the given affine transformation matrix. This assumes
 * the given matrix has no shear present.
 *
 * @param mat 		The matrix to neutralize scale for. Matrix is modified *in-place*.
 */
inline void neutralizeScale(Mat44 &mat)
{
	Vec3 basisVecX = vec3(mat[0][0], mat[0][1], mat[0][2]);
	Vec3 basisVecY = vec3(mat[1][0], mat[1][1], mat[1][2]);
	Vec3 basisVecZ = vec3(mat[2][0], mat[2][1], mat[2][2]);

	Vec3 normalizedBasisVecX = normalize(basisVecX);
	Vec3 normalizedBasisVecY = normalize(basisVecY);
	Vec3 normalizedBasisVecZ = normalize(basisVecZ);

	mat[0][0] = normalizedBasisVecX.x;
	mat[0][1] = normalizedBasisVecX.y;
	mat[0][2] = normalizedBasisVecX.z;

	mat[1][0] = normalizedBasisVecY.x;
	mat[1][1] = normalizedBasisVecY.y;
	mat[1][2] = normalizedBasisVecY.z;

	mat[2][0] = normalizedBasisVecZ.x;
	mat[2][1] = normalizedBasisVecZ.y;
	mat[2][2] = normalizedBasisVecZ.z;
}


/**
 * Sets the scale of the given transformation matrix to the values provided. This
 * works only for affine transformation matrices that do not have any shear.
 *
 * @param mat		The affine transformation matrix to set the scale for.
 * @param scale	The scale to set.
 */
inline void setScale(Mat44 &mat, float scale[3])
{
	// NOTE: (sonictk) Normalize the basis vectors and then scale by desired factor.
	neutralizeScale(mat);
	scaleBy(mat, scale);
}

/**
 * Sets the scale of the given transformation matrix to the values provided. This
 * works only for affine transformation matrices that do not have any shear.
 *
 * @param mat		The affine transformation matrix to set the scale for.
 * @param scale	The scale to set.
 */
inline void setScale(Mat44 &mat, const Vec3 &scale)
{
	float setter[3] = {scale.x, scale.y, scale.z};
	setScale(mat, setter);
}

#ifdef __cplusplus
/// @overload
template<typename T>
inline void setScale(Mat44 &mat, T scale[3])
{
	setScale(mat, static_cast<float *>(scale));
}
#endif // __cplusplus


/**
 * Retrives the rotation from the given transformation matrix. This assumes that
 * the matrix does not have any shear.
 *
 * @param mat	The matrix to retrieve rotation from.
 *
 * @return		The rotation matrix.
 */
Mat44 getRotation(const Mat44 &mat);

/**
 * Retrives the Euler rotation from the given transformation matrix. This
 * assumes that the matrix does not have any shear.
 *
 * @param mat			The matrix to retrieve rotation from.
 * @param rotation		The storage for the rotation.
 * @param rotOrder		The rotation order to assume for the extracted rotation.
 * 					Currently, only ``XYZ`` and ``ZYX`` rotation orders are supported.
 */
void getRotation(const Mat44 &mat, float rotation[3], const RotationOrder order);

/**
 * Retrives the rotation as a quaternion from the given rotation matrix. The
 * resulting quaternion is *normalized* (unit quaternion).
 *
 * @param mat			The rotation matrix. This matrix must be *orthogonal* and
 * 					*special orthogonal* (i.e. pure rotation without any
 * 					reflection component)
 *
 * @param q			The storage for the rotation.
 */
void getRotation(const Mat44 &mat, Quaternion &q);

/**
 * Retrives the rotation as a quaternion from the given transformation matrix. The
 * resulting quaternion is *normalized* (unit quaternion).
 *
 * @param mat	The affine transformation matrix to retrieve the rotation for.
 *
 * @return		The rotation expressed as a quaternion.
 */
Quat getRotationFromTransformation(const Mat44 &mat);


bool setRotation(Mat44 &mat, const float rotation[3], const RotationOrder order);


/**
 * Extracts the scale from a given ``row`` of a upper 3x3 quadrant of a
 * transformation matrix.
 *
 * @param scale	The storage for the scaling component.
 * @param row		The row to extract the scaling component from.
 *
 * @return			``true`` if the operation was successful, ``false`` if an error
 * 				occurred while trying to extract the scale.
 */
bool getScaleFromRow(const float &scale, const Vec3 &row);


/**
 * Extracts the scale and shear for a given 4x4 affine transformation matrix. This
 * modifies the given matrix in-place.
 *
 * @param mat		The matrix to extract the scaling and shear components for.
 * @param scale	Storage for the retrieved scaling component.
 * @param shear	Storage for the retrieved shear component.
 *
 * @return			``true`` if the operation was successful, ``false`` otherwise.
 */
bool extractAndRemoveScaleAndShear(Mat44 &mat, Vec3 &scale, Vec3 &shear);


/**
 * Creates a 3x3 shear matrix.
 *
 * @param xy 	The amount of shear this matrix should apply in the XY plane.
 * @param xz 	The amount of shear this matrix should apply in the XZ plane.
 * @param yx 	The amount of shear this matrix should apply in the YX plane.
 * @param yz 	The amount of shear this matrix should apply in the YZ plane.
 * @param zx 	The amount of shear this matrix should apply in the ZX plane.
 * @param zy 	The amount of shear this matrix should apply in the ZY plane.
 *
 * @return		The shear matrix.
 */
inline Mat33 shearMatrix(float xy, float xz, float yx, float yz, float zx, float zy)
{
	Mat33 result = {
		{{1.0f, 	yx, 	zx},
		 {xy, 		1.0f, 	zy},
		 {xz, 		yz,	1.0f}}
	};

	return result;
}


/**
 * Creates a 4x4 shear matrix.
 *
 * @param xy 	The amount of shear this matrix should apply in the XY plane.
 * @param xz 	The amount of shear this matrix should apply in the XZ plane.
 * @param yx 	The amount of shear this matrix should apply in the YX plane.
 * @param yz 	The amount of shear this matrix should apply in the YZ plane.
 * @param zx 	The amount of shear this matrix should apply in the ZX plane.
 * @param zy 	The amount of shear this matrix should apply in the ZY plane.
 *
 * @return		The shear matrix.
 */
inline Mat44 shearMatrixMat44(float xy, float xz, float yx, float yz, float zx, float zy)
{
	Mat44 result = {
		{{1.0f, 	yx, 	zx, 	0.0f},
		 {xy, 		1.0f, 	zy, 	0.0f},
		 {xz, 		yz,	1.0f,	0.0f},
		 {0.0f, 	0.0f,	0.0f,	1.0f}}
	};

	return result;
}


/**
 * Calculate the determinant of a 4x4 matrix. This should be used instead of the
 * other version when the size of the matrix is 4x4, as it is faster.
 *
 * @param mat	A 4x4 matrix.
 *
 * @return		The determinant.
 */
float determinant(const Mat44 &mat);


/**
 * Calculate the determinant of a variable-sized square matrix.
 *
 * @param mat			Pointer to a pointer of an arbitrary square matrix.
 * @param dimension	The dimension of the square matrix.
 *
 * @return				The determinant.
 */
float determinant(float **mat, int dimension);


/**
 * This finds the matrix of cofactors for the given 4x4 matrix.
 *
 * @param mat	A 4x4 matrix.
 *
 * @return		The matrix of cofactors.
 */
Mat44 cofactor(const Mat44 &mat);


/**
 * This finds the adjugate/adjoint for the given 4x4 matrix.
 *
 * @param mat	A 4x4 matrix.
 *
 * @return		The adjugate/adjoint matrix.
 */
Mat44 adjugate(const Mat44 &mat);


/**
 * This finds the inverse of the given 4x4 matrix.
 *
 * @param inMat 	The 4x4 matrix to find the inverse of.
 * @param outMat	The 4x4 matrix that will have the result written to.
 *
 * @return 		``0`` on success, a negative value if the inverse does not
 * 				exist or an error occurred.
 */
int inverse(const Mat44 &inMat, Mat44 &outMat);


/**
 * This creates a projection matrix for use with 3D transformations. This is meant
 * to be used with a **left-handed** normalized device coordinate (NDC) system
 * (OpenGL style). (i.e. the ``-1`` is located in the 12th matrix element)
 *
 * @param fov			The width of the frustum, in **degrees**.
 * @param aspectRatio	The screen width / height ratio.
 * @param nearDist		The near plane distance.
 * @param farDist		The far plane distance.
 *
 * @return				The projection matrix.
 */
Mat44 projection(float fov, float aspectRatio, float nearDist, float farDist);


/**
 * Creates a view matrix (a.k.a camera matrix) for use with 3D transformations.
 * This uses right-handed coordinate space (OpenGL-style) where the camera is
 * looking in the ``-Z`` axis.
 *
 * @param pos			The position of the eye/camera.
 * @param target		The position of the target point (to look at).
 * @param up			The up vector.
 *
 * @return				The view matrix.
 */
Mat44 view(Vec3 pos, Vec3 target, Vec3 up);


/**
 * Creates a view matrix (a.k.a camera matrix) for use with 3D transformations.
 * This view matrix is in *right-handed* space.
 * This version is more suited for use with cameras that do not roll (e.g. FPS cameras).
 *
 * @param pos			The position of the eye/camera.
 * @param pitch		The angle to rotate around. Should be from ``-90`` to ``90`` degrees.
 * @param yaw			The angle to rotate around. Should be from ``0`` to ``360`` degrees.
 *
 * @return				The view matrix.
 */
Mat44 view(Vec3 pos, float pitch, float yaw);


/// A simple variable-size matrix data structure that stores its elements in a
/// continguous array.
struct MatCX
{
	float *e;

	int rows;
	int columns;

#ifdef __cplusplus
	/**
	 * Gets a pointer to the start of the ``row`` given.
	 *
	 * @param mat 	The matrix to get the row for.
	 * @param row 	The row index. Indices start from ``0``.
	 *
	 * @return 	A pointer to the start of the row.
	 */
	inline float &operator[](int row) {
		return e[row * columns];
	}

	inline float &operator()(int row, int column) {
		return e[row * column];
	}
#endif // __cplusplus
};


#ifdef __cplusplus
/**
 *  A variable-size matrix data structure. These are assumed to be **row-major**.
 * NOTE: My recommendation is to typedef your template specialization before using
 * it so that it's easier to figure out what you're doing/refactor your code.
 *
 * @tparam T		Specify the type of data each matrix member has. Typically ``float``/``double``.
 * @tparam Trows	The number of rows that the matrix should have.
 * @tparam Tcolumns	The number of columns that the matrix should have.
 *
 * @return			The matrix.
 */
template<class T, int Trows, int Tcolumns>
struct MatX
{
	T e[Trows][Tcolumns];

	/// The number of rows.
	static const int rows = Trows;

	/// The number of columns.
	static const int columns = Tcolumns;

	/**
	 * Gives access to a specific row of the matrix. Warning: no bounds-checking is done!
	 *
	 * @param row 	The row to retrive.
	 *
	 * @return 	A pointer to the requested row.
	 */
	inline T *operator[](int row) {
		return e[row];
	}

	/**
	 * Gives access to a specific row of the matrix. Warning: no bounds-checking is done!
	 *
	 * @param row 	The row to retrive.
	 *
	 * @return 	A pointer to the requested row.
	 */
	inline const T *operator[](int row) const {
		return e[row];
	}

	/**
	 *  Gives access to a specific member of the matrix. Warning: no bounds-checking is done!
	 *
	 * @param row 	The row of the member.
	 * @param col 	The column of the member.
	 *
	 * @return		A reference to the member.
	 */
	inline T &operator()(int row, int col) {
		return e[row][col];
	}

	/**
	 *  Gives access to a specific member of the matrix. Warning: no bounds-checking is done!
	 *
	 * @param row 	The row of the member.
	 * @param col 	The column of the member.
	 *
	 * @return		A reference to the member.
	 */
	inline const T &operator()(int row, int col) const {
		return e[row][col];
	}
};

/**
 * Creates a variable-sized matrix and initializes all values in it to default values.
 *
 * @return			The new matrix.
 */
template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> matX()
{
	MatX<T, Trows, Tcolumns> result;
	for (int r=0; r < Trows; ++r) {
		for (int c=0; c < Tcolumns; ++c) {
			result[r][c] = T();
		}
	}

	return result;
}

/**
 * Creates a variable-sized matrix and initializes all values in it to ``value``.
 *
 * @tparam value	The value to initialize all members to.
 *
 * @return			The new matrix.
 */
template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> matX(const T value)
{
	MatX<T, Trows, Tcolumns> result;
	for (int r=0; r < Trows; ++r) {
		for (int c=0; c < Tcolumns; ++c) {
			result[r][c] = value;
		}
	}

	return result;
}

/**
 * Creates a variable-sized matrix from the given ``values``.
 *
 * @param values	A pointer to the array of values to initialize the matrix with.
 * 				These will be used in **row-major** order. If the array is not
 * 				large enough to fill the entire matrix's dimensions, it will
 * 				result in undefined behaviour!
 *
 * @return			The variable-sized matrix.
 */
template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> matX(const T *values)
{
	MatX<T, Trows, Tcolumns> result;
	int runningIndex = 0;
	for (int r=0; r < Trows; ++r) {
		for (int c=0; c < Tcolumns; ++c) {
			result[r][c] = values[runningIndex];
			runningIndex++;
		}
	}
}


/**
 * Creates an identity matrix for the given variable-sized matrix.
 *
 * @tparam T			The type of data each member in the matrix will have.
 * @tparam Trows		The number of rows in the matrix.
 * @tparam Tcolumns	The number of columns in the matrix.
 * @tparam mat			The matrix to find the identity matrix for. This is
 * 					assuming the identity matrix will be used in **pre-multiplication**.
 *
 * @return				The identity matrix.
 */
template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Trows> identityMatX(const MatX<T, Trows, Tcolumns> &mat)
{
	MatX<T, Trows, Trows> result;
	for (int r=0; r < Trows; ++r) {
		result[r][r] = static_cast<T>(1.0);
	}

	return result;
}

template<typename T, int Trows, int Tcolumns>
inline void getValues(const MatX<T, Trows, Tcolumns> &mat, T *storage, int size)
{
	int i = 0;
	while (i < size && i < Trows * Tcolumns) {
		for (int r = 0; r < Trows; ++r) {
			for (int c = 0; c < Tcolumns; ++c) {
				storage[i] = mat[r][c];
				++i;
			}
		}
	}
}

template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> operator+(const MatX<T, Trows, Tcolumns> &m1,
										  const MatX<T, Trows, Tcolumns> &m2)
{
	MatX<T, Trows, Tcolumns> result;
	for (int r = 0; r < Trows; ++r) {
		for (int c = 0; c < Tcolumns; ++c) {
			result[r][c] = m1[r][c] + m2[r][c];
		}
	}

	return result;
}

template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> &operator+=(MatX<T, Trows, Tcolumns> &m1,
											const MatX<T, Trows, Tcolumns> &m2)
{
	m1 = m1 + m2;

	return m1;
}

template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> operator+(const MatX<T, Trows, Tcolumns> &m1,
										  const T &factor)
{
	MatX<T, Trows, Tcolumns> result;
	for (int r = 0; r < Trows; ++r) {
		for (int c = 0; c < Tcolumns; ++c) {
			result[r][c] = m1[r][c] + factor;
		}
	}

	return result;
}

template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> &operator+=(MatX<T, Trows, Tcolumns> &m1,
										   const T &factor)
{
	m1 = m1 + factor;

	return m1;
}

template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> operator-(const MatX<T, Trows, Tcolumns> &m1,
										  const MatX<T, Trows, Tcolumns> &m2)
{
	MatX<T, Trows, Tcolumns> result;
	for (int r = 0; r < Trows; ++r) {
		for (int c = 0; c < Tcolumns; ++c) {
			result[r][c] = m1[r][c] - m2[r][c];
		}
	}

	return result;
}

template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> &operator-=(MatX<T, Trows, Tcolumns> &m1,
											const MatX<T, Trows, Tcolumns> &m2)
{
	m1 = m1 - m2;
	return m1;
}

template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> operator-(const MatX<T, Trows, Tcolumns> &m1,
										  const T &factor)
{
	MatX<T, Trows, Tcolumns> result;
	for (int r = 0; r < Trows; ++r) {
		for (int c = 0; c < Tcolumns; ++c) {
			result[r][c] = m1[r][c] - factor;
		}
	}

	return result;
}

template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> &operator-=(MatX<T, Trows, Tcolumns> &m1,
											const T &factor)
{
	m1 = m1 - factor;

	return m1;
}

template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> operator*(const MatX<T, Trows, Tcolumns> &mat, T factor)
{
	MatX<T, Trows, Tcolumns> result;
	for (int r = 0; r < Trows; ++r) {
		for (int c = 0; c < Tcolumns; ++c) {
			result[r][c] = mat[r][c] * factor;
		}
	}

	return result;
}

template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> &operator*=(MatX<T, Trows, Tcolumns> &mat, T factor)
{
	mat = mat * factor;

	return mat;
}

/**
 * Multiplies one variable-sized matrix with another.
 *
 * @tparam T			The type to use for the matrix members.
 * @tparam Trows		The number of rows in the first matrix.
 * @tparam Tcolumns	The number of columns in the first matrix.
 * @param m1			The first matrix to multiply.
 * @param m2			The second matrix to multiply with the first.
 *
 * @return				The result.
 */
template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> operator*(const MatX<T, Trows, Tcolumns> &m1,
										  const MatX<T, Tcolumns, Trows> &m2)
{
	MatX<T, Trows, Tcolumns> result;
	for (int r = 0; r < Trows; ++r) {
		for (int c = 0; c < Tcolumns; ++c) {
			for (int i = 0; i < Trows; ++i) {
				result[r][c] += m1(r, i) * m2(i, c);
			}
		}
	}

	return result;
}

template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> &operator*=(const MatX<T, Trows, Tcolumns> &m1,
											const MatX<T, Tcolumns, Trows> &m2)
{
	m1 = m1 * m2;
	return m1;
}

template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> operator/(const MatX<T, Trows, Tcolumns> &mat,
										  const T &factor)
{
	MatX<T, Trows, Tcolumns> result;
	for (int r = 0; r < Trows; ++r) {
		for (int c = 0; c < Tcolumns; ++c) {
			result[r][c] = mat[r][c] / factor;
		}
	}

	return result;
}

template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> &operator/=(MatX<T, Trows, Tcolumns> &mat,
											const T factor)
{
	mat = mat / factor;
	return mat;
}

template<typename T, int Trows, int Tcolumns>
inline bool operator==(const MatX<T, Trows, Tcolumns> &mat1,
					   const MatX<T, Trows, Tcolumns> &mat2)
{
	for (int r = 0; r < Trows; ++r) {
		for (int c = 0; c < Tcolumns; ++c) {
			if (mat1[r][c] != mat2[r][c]) {
				return false;
			}
		}
	}

	return true;
}

template<typename T, int Trows, int Tcolumns>
inline bool operator!=(const MatX<T, Trows, Tcolumns> &mat1,
					   const MatX<T, Trows, Tcolumns> &mat2)
{
	return !(mat1 == mat2);
}


/**
 * Returns the transposed version of the matrix.
 *
 * @param mat	The matrix to transpose.
 *
 * @return		A transposed version of the matrix.
 */
template<typename T, int Trows, int Tcolumns>
inline MatX<T, Trows, Tcolumns> transpose(const MatX<T, Trows, Tcolumns> &mat)
{
	MatX<T, Tcolumns, Trows> result;

	for (int r = 0; r < Trows; ++r) {
		for (int c = 0; c < Tcolumns; ++c) {
			result[r][c] = mat[c][r];
		}
	}

	return result;
}


/// A simple 4x4 matrix data structure. These are assumed to be **row-major** by
/// convention and memory layout follows **OpenGL** standards.
/// Example layout:
/// 		1  0  0  tX
/// 		0  1  0  tY
/// 		0  0  1  tZ
/// 		0  0  0  1
///
/// DirectX has the layout transposed:
/// 		1  0  0  0
/// 		0  1  0  0
/// 		0  0  1  0
/// 		tX tY tZ 1
///
/// This is essentially identical to the ``Mat44`` struct except that this class
/// can use any type that supports standard mathematical operator overloads.
template<typename T>
struct Mat44X
{
	T e[4][4];

	inline T *operator[](int row) {
		return e[row];
	}

	inline const T *operator[](int row) const {
		return e[row];
	}

	inline const T &operator()(int row, int col) const {
		return e[row][col];
	}

	inline T &operator()(int row, int col) {
		return e[row][col];
	}
};


/**
 * Creates a new 4x4 matrix with the provided values.
 *
 * @return	The new matrix.
 */
template<typename T>
inline Mat44X<T> mat44x(const T vals[4][4])
{
	Mat44X<T> mat = {
		{{vals[0][0], vals[0][1], vals[0][2], vals[0][3]},
		 {vals[1][0], vals[0][1], vals[0][2], vals[0][3]},
		 {vals[2][0], vals[0][1], vals[0][2], vals[0][3]},
		 {vals[3][0], vals[0][1], vals[0][2], vals[0][3]}}
	};

	return mat;
}


/**
 * Creates a new 4x4 matrix with the provided values. The matrix will be created
 * using the values in the following indexes:
 * 	0   1   2   3
 * 	4   5   6   7
 * 	8   9   10  11
 * 	12  13  14  15
 *
 * @param vals		The values to use for constructing the matrix with.
 *
 * @return			The new matrix.
 */
template<typename T>
inline Mat44X<T> mat44x(const T vals[16])
{
	Mat44X<T> mat = {
		{{vals[0], vals[1], vals[2], vals[3]},
		 {vals[4], vals[5], vals[6], vals[7]},
		 {vals[8], vals[9], vals[10], vals[11]},
		 {vals[12], vals[13], vals[14], vals[15]}}
	};

	return mat;
}


/**
 * Copies a new 4x4 matrix from an existing one.
 *
 * @param mat		The existing matrix to copy..
 *
 * @return			A copy of an existing matrix.
 */
template<typename T>
inline Mat44X<T> mat44x(const Mat44X<T> &mat)
{
	Mat44X<T> result = {
		{{mat[0][0], mat[0][1], mat[0][2], mat[0][3]},
		 {mat[1][0], mat[0][1], mat[0][2], mat[0][3]},
		 {mat[2][0], mat[0][1], mat[0][2], mat[0][3]},
		 {mat[3][0], mat[0][1], mat[0][2], mat[0][3]}}
	};

	return result;
}


/**
 * Creates a new 4x4 identity matrix.
 *
 * @return	The identity matrix.
 */
template<typename T>
inline Mat44X<T> mat44x()
{
	Mat44X<T> mat = {
		{{static_cast<T>(1), static_cast<T>(0), static_cast<T>(0), static_cast<T>(0)},
		 {static_cast<T>(0), static_cast<T>(1), static_cast<T>(0), static_cast<T>(0)},
		 {static_cast<T>(0), static_cast<T>(0), static_cast<T>(1), static_cast<T>(0)},
		 {static_cast<T>(0), static_cast<T>(0), static_cast<T>(0), static_cast<T>(1)}}
	};

	return mat;
}

/**
 * Retrieves the values from the given 4x4 matrix in row-column order to an array of floats.
 *
 * @param mat 		The matrix to retrieve the values for.
 * @param f 		Storage for the matrix's values.
 * @param size 	The size of the storage. Must be at least 16 wide to retrieve all values.
 */
template<typename T>
inline void getValues(const Mat44X<T> &mat, T *f, int size)
{
	int i = 0;
	while (i < size && i < 16) {
		for (int r=0; r <= 3; ++r) {
			for (int c=0; c <= 3; ++c) {
				f[i] = mat[r][c];
				++i;
			}
		}
	}
}

template<typename T>
inline Mat44X<T> operator+(const Mat44X<T> &a, const Mat44X<T> &b)
{
	Mat44X<T> result = {
		{{a(0, 0) + b(0, 0), a(0, 1) + b(0, 1), a(0, 2) + b(0, 2), a(0, 3) + b(0, 3)},
		 {a(1, 0) + b(1, 0), a(1, 1) + b(1, 1), a(1, 2) + b(1, 2), a(1, 3) + b(1, 3)},
		 {a(2, 0) + b(2, 0), a(2, 1) + b(2, 1), a(2, 2) + b(2, 2), a(2, 3) + b(2, 3)},
		 {a(3, 0) + b(3, 0), a(3, 1) + b(3, 1), a(3, 2) + b(3, 2), a(3, 3) + b(3, 3)}}
	};

	return result;
}

template<typename T>
inline Mat44X<T> &operator+=(Mat44X<T> &a, const Mat44X<T> &b)
{
	a = a + b;
	return a;
}

template<typename T>
inline Mat44X<T> operator-(const Mat44X<T> &a, const Mat44X<T> &b)
{
	Mat44X<T> result = {
		{{a(0, 0) - b(0, 0), a(0, 1) - b(0, 1), a(0, 2) - b(0, 2), a(0, 3) - b(0, 3)},
		 {a(1, 0) - b(1, 0), a(1, 1) - b(1, 1), a(1, 2) - b(1, 2), a(1, 3) - b(1, 3)},
		 {a(2, 0) - b(2, 0), a(2, 1) - b(2, 1), a(2, 2) - b(2, 2), a(2, 3) - b(2, 3)},
		 {a(3, 0) - b(3, 0), a(3, 1) - b(3, 1), a(3, 2) - b(3, 2), a(3, 3) - b(3, 3)}}
	};

	return result;
}

template<typename T>
inline Mat44X<T> &operator-=(Mat44X<T> &a, const Mat44X<T> &b)
{
	a = a - b;
	return a;
}

template<typename T>
inline Mat44X<T> operator*(const Mat44X<T> &mat, T factor)
{
	Mat44X<T> result = {
		{{mat(0, 0) * factor, mat(0, 1) * factor, mat(0, 2) * factor, mat(0, 3) * factor},
		 {mat(1, 0) * factor, mat(1, 1) * factor, mat(1, 2) * factor, mat(1, 3) * factor},
		 {mat(2, 0) * factor, mat(2, 1) * factor, mat(2, 2) * factor, mat(2, 3) * factor},
		 {mat(3, 0) * factor, mat(3, 1) * factor, mat(3, 2) * factor, mat(3, 3) * factor}}
	};

	return result;
}

template<typename T>
inline Mat44X<T> operator*(const Mat44X<T> &a, const Mat44X<T> &b)
{
	Mat44X<T> result = {};
	for (int r=0; r <= 3; ++r) {
		for (int c=0; c <=3; ++c) {
			for (int i=0; i <= 3; ++i) {
				result[r][c] += a(r, i) * b(i, c);
			}
		}
	}

	return result;
}

template<typename T>
inline Mat44X<T> &operator*=(Mat44X<T> &mat, T factor)
{
	mat = mat * factor;
	return mat;
}

template<typename T>
inline Mat44X<T> &operator*=(Mat44X<T> &a, const Mat44X<T> &b)
{
	a = a * b;
	return a;
}

template<typename T>
inline Vec4 operator*(const Mat44X<T> &mat, Vec4 vec)
{
	Vec4 result;

	result.x = (vec.x * mat[0][0]) + (vec.y * mat[0][1]) + (vec.z * mat[0][2]) + (vec.w * mat[0][3]);
	result.y = (vec.x * mat[1][0]) + (vec.y * mat[1][1]) + (vec.z * mat[1][2]) + (vec.w * mat[1][3]);
	result.z = (vec.x * mat[2][0]) + (vec.y * mat[2][1]) + (vec.z * mat[2][2]) + (vec.w * mat[2][3]);
	result.w = (vec.x * mat[3][0]) + (vec.y * mat[3][1]) + (vec.z * mat[3][2]) + (vec.w * mat[3][3]);

	return result;
}

template<typename T>
inline Vec3 operator*(const Mat44X<T> &mat, Vec3 vec)
{
	Vec3 result = (mat * vec4(vec, 1.0f)).xyz;
	return result;
}

template<typename T>
inline Mat44X<T> operator/(const Mat44X<T> &mat, T factor)
{
	Mat44X<T> result = {
		{{mat(0, 0) / factor, mat(0, 1) / factor, mat(0, 2) / factor, mat(0, 3) / factor},
		 {mat(1, 0) / factor, mat(1, 1) / factor, mat(1, 2) / factor, mat(1, 3) / factor},
		 {mat(2, 0) / factor, mat(2, 1) / factor, mat(2, 2) / factor, mat(2, 3) / factor},
		 {mat(3, 0) / factor, mat(3, 1) / factor, mat(3, 2) / factor, mat(3, 3) / factor}}
	};

	return result;
}

template<typename T>
inline Mat44X<T> &operator/=(Mat44X<T> &mat, T factor)
{
	mat = mat / factor;
	return mat;
}

template<typename T>
inline bool operator==(const Mat44X<T> &mat1, const Mat44X<T> &mat2)
{
	for (int r=0; r <= 3; ++r) {
		for (int c=0; c <= 3; ++c) {
			if (mat1[r][c] != mat2[r][c]) {
				return false;
			}
		}
	}
	return true;
}

template<typename T>
inline bool operator!=(const Mat44X<T> &mat1, const Mat44X<T> &mat2)
{
	return !(mat1 == mat2);
}


/**
 * Returns the transposed version of the matrix.
 *
 * @param mat	The matrix to transpose.
 *
 * @return		A transposed version of the matrix.
 */
template<typename T>
inline Mat44X<T> transpose(const Mat44X<T> &mat)
{
	Mat44X<T> result;
	for (int j=0; j <= 3; ++j) {
		for (int i=0; i <= 3; ++i) {
			result[j][i] = mat(i, j);
		}
	}

	return result;
}
#endif // __cplusplus


// TODO: (sonictk) Implement transformation operations for Mat44X


#endif /* SS_MATRIX_MATH_H */
