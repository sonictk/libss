/**
 * @file   ss_uuid.h
 * @brief  Utilities for working with universally unique identifiers (UUIDs).
 * 	   On Windows, requires linking to ``Ole32`` and ``Rpcrt4``.
 *
 */
#ifndef SS_UUID_H
#define SS_UUID_H

/// The number of characters in a UUID when converted to a string.
#define SS_UUID_STRING_LENGTH 36

#ifdef _WIN32

#include "ss_uuid_win32.cpp"

#elif __linux__ || __APPLE__

#include "ss_uuid_linux.cpp"

#endif // Platform layer


/**
 * Generates a new Universally Unique Identifier (UUID) for use.
 *
 * @param uuid		The storage for the new UUID.
 *
 * @return			``0`` on success, a negative value on failure.
 */
int createUuid(SSUuid &uuid);


/**
 * Gets a string representation of the given UUID.
 *
 * @param uuid 	The UUID to get a string representation of.
 * @param name		Storage for the UUID string representation.
 *
 * @return 		``0`` on success, a negative value if an error occurred.
 */
int getUuidAsString(const SSUuid &uuid, char name[SS_UUID_STRING_LENGTH]);


/**
 * This finds an existing Uuid with the given ``name``.
 *
 * @param name		The UUID to find with the given string representation.
 * @param uuid		Storage for the UUID.
 *
 * @return			``0`` on success, ``-1`` if the UUID with the given name does
 * 				not exist. Returns a negative value on all other failures.
 */
int getUuid(const char name[SS_UUID_STRING_LENGTH], SSUuid &uuid);


/**
 * Checks if the two UUIDs are equivalent.
 *
 * @param uuid1	The first UUID to compare.
 * @param uuid2	The second UUID to compare.
 *
 * @return			``true`` if both UUIDs are the same, ``false`` otherwise.
 */
bool areUuidsEqual(SSUuid uuid1, SSUuid uuid2);


// TODO: (sonictk) Add functionality to validate UUID.
// bool isUuidValid(const SSUuid &uuid);


#endif /* SS_UUID_H */
