/**
 * @file   ss_timer.h
 * @brief  Timing-related functions.
 */
#ifndef SS_TIMER_H
#define SS_TIMER_H

#include "ss_ansi.h"


struct SSPerfTimer;


#ifdef _WIN32

#include "ss_timer_win32.cpp"

#elif __linux__ || __APPLE__

#include "ss_timer_linux.cpp"

#else

#error "Unsupported OS platform for compilation."

#endif // Platform layer


/**
 * This resets the given ``timer``.
 *
 * @param timer	The timer to reset.
 *
 * @return			``0`` on success, a negative value if an error occurred.
 */
int resetSSPerfTimerValue(SSPerfTimer &timer);


/**
 * This creates a new **timer** for use in measuring code execution sections.
 *
 * @return		The timer.
 */
SSPerfTimer perfTimer();


/**
 * This begins the timer's measurement.
 *
 * @param timer 	The timer to start measurements.
 *
 * @return			``0`` on success, a negative value if an error occurred.
 */
int startSSPerfTimer(SSPerfTimer &timer);


/**
 * This ends the timer's measurement.
 *
 * @param timer 	The timer to end measurements.
 *
 * @return			``0`` on success, a negative value if an error occurred.
 */
int endSSPerfTimer(SSPerfTimer &timer);


/**
 * This retrieves the current measurement value of the timer.
 *
 * @param timer 	The timer to get measurements.
 *
 * @return 		The measured interval.
 *
 * 				On Windows, this is the number of counts from the high-resolution
 * 				performance counter elapsed per second.
 *
 * 				On Linux, this is in nanoseconds.
 */
double getOSSSPerfTimerValue(const SSPerfTimer &timer);


/**
 * This retrieves the current measurement value of the timer, in microseconds.
 * Unlike ``getOSSSPerfTimerValue``, the return value of this function is the same
 * regardless of OS.
 *
 * @param timer 	The timer to get measurements.
 *
 * @return 		The measured interval.
 *
 */
double getSSPerfTimerValue(const SSPerfTimer &timer);


/// This acts as a global timer that can used for the host application.
#ifdef SS_ENABLE_GLOBAL_TIMER

globalVar SSPerfTimer kGlobalSSPerfTimer;

/**
 * This begins the global timer's measurement.
 *
 * @return			``0`` on success, a negative value if an error occurred.
 */
int startSSPerfTimer();

/**
 * This ends the global timer's measurement.
 *
 * @return			``0`` on success, a negative value if an error occurred.
 */
int endSSPerfTimer();

/**
 * This retrieves the current measurement value of the global timer.
 *
 * @return 		The measured interval.
 *
 * 				On Windows, this is the number of counts from the high-resolution
 * 				performance counter elapsed per second.
 *
 * 				On Linux, this is in nanoseconds.
 */
double getOSSSPerfTimerValue();


/**
 * This retrieves the current measurement value of the global timer, in microseconds.
 * Unlike ``getOSSSPerfTimerValue``, the return value of this function is the same
 * regardless of OS.
 *
 * @return 		The measured interval.
 *
 */
double getSSPerfTimerValue();

#endif // SS_ENABLE_GLOBAL_TIMER


/**
 * Returns the number of milliseconds that have elapsed since the system was
 * started.
 * NOTE: On Windows, this value is accurate up to 49.7 days.
 *
 * @return		The number of milliseconds since the system was started.
 */
OSTickCount getOSTickCount();


/**
 * Suspends execution of the current thread for the given ``interval``.
 *
 * @param interval		The time to sleep in milliseconds.
 */
void sleepOS(int interval);



#define SS_PRINT_EXPR_TIME(expr) do { SSPerfTimer t = perfTimer(); expr; endSSPerfTimer(t); double tv = getOSSSPerfTimerValue(t); printf(SS_ANSI_COLOR_CYAN "Time taken: %f" SS_ANSI_COLOR_RESET, tv); } while (0)


#endif /* SS_TIMER_H */
