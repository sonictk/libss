/**
 * @file   ss_errno.h
 * @brief  Various status codes and functions for setting/retrieving status codes.
 */
#ifndef SS_ERRNO_H
#define SS_ERRNO_H

#include <limits.h>


/// Various status codes for the application globally.
enum SS_Status
{
	SS_Status_UnknownFailure = INT_MIN,

	SS_Status_InvalidNumOfArgs,
	SS_Status_InvalidFilePath,
	SS_Status_InvalidFileData,
	SS_Status_EnvironmentVariableNotFound,

	// Filesystem-related status codes.
	SS_Status_CannotSetFilePtr,
	SS_Status_EndOfFile,
	SS_Status_FileReadFailure,
	SS_Status_FileWriteFailure,
	SS_Status_InvalidParameter,
	SS_Status_UnableToCloseHandle,
	SS_Status_AccessDenied,
	SS_Status_FileSharingViolation,
	SS_Status_AlreadyExists,

	SS_Status_OutOfMemory,
	SS_Status_Segfault,

	SS_Status_Success = 0
};


globalVar const char GLOBAL_SS_ERR_MSG_SUCCESS[] = "";
globalVar const char GLOBAL_SS_ERR_MSG_UNKNOWN_FAILURE[] = "An unknown error has occurred.";

globalVar const char GLOBAL_SS_ERR_MSG_INVALID_FILE_PATH[] = "A file path specified was invalid.";
globalVar const char GLOBAL_SS_ERR_MSG_INVALID_FILE_DATA[] = "The file data was corrupted.";
globalVar const char GLOBAL_SS_ERR_MSG_CANNOT_SET_FILE_PTR[] = "Unable to set the file pointer to the requested location.";
globalVar const char GLOBAL_SS_ERR_MSG_ENVIRONMENT_VARIABLE_NOT_FOUND[] = "The requested environment variable was not available.";

globalVar const char GLOBAL_SS_ERR_MSG_EOF[] = "A read operation was requested past the end of the file.";
globalVar const char GLOBAL_SS_ERR_MSG_FILE_READ_FAILURE[] = "An unknown failure occurred when attempting to read the file.";
globalVar const char GLOBAL_SS_ERR_MSG_FILE_WRITE_FAILURE[] = "An unknown failure occurred when attempting to write to the file.";
globalVar const char GLOBAL_SS_ERR_MSG_INVALID_PARAMETER[] = "An invalid parameter was passed to the function.";
globalVar const char GLOBAL_SS_ERR_MSG_UNABLE_TO_CLOSE_HANDLE[] = "A file handle was unable to be closed.";
globalVar const char GLOBAL_SS_ERR_MSG_ACCESS_DENIED[] = "Access denied.";
globalVar const char GLOBAL_SS_ERR_MSG_FILE_SHARING_VIOLATION[] = "A file sharing violation occurred.";
globalVar const char GLOBAL_SS_ERR_MSG_ALREADY_EXISTS[] = "The resource already exists on disk.";

globalVar const char GLOBAL_SS_ERR_MSG_OUT_OF_MEMORY[] = "The system ran out of memory and could not accomodate the requested allocation.";
globalVar const char GLOBAL_SS_ERR_MSG_ACCESS_VIOLATION[] = "An access violation has occurred.";

globalVar const char GLOBAL_SS_ERR_MSG_INVALID_NUM_OF_ARGS[] = "An invalid number of arguments were specified.";


/**
 * Retrieves the global error code for this application.
 * The error code is kept in thread-local storage so that multiple threads do not
 * overwrite each other's values.
 *
 * @return 	The status code.
 */
SS_Status SSGetLastError();


/**
 * Sets the global error code for this application for the calling thread.
 * The error code is kept in thread-local storage so that multiple threads do not
 * overwrite each other's values.
 *
 * @param status		The status code to set globally.
 */
void SSSetLastError(SS_Status status);


/**
 * Converts an error code defined to a human-readable error message.
 *
 * @param status 		The error code to convert.
 *
 * @return 			An error message that is suitable for displaying to an end-user.
 */
const char *SSGetErrMsg(SS_Status status);


#define SS_PERROR_LAST()                        \
{                                               \
    SS_Status status = SSGetLastError();        \
    if (status != SS_Status_Success) {          \
        SS_PERROR(SSGetErrMsg(status));         \
    }                                           \
}                                               \



#endif /* SS_ERRNO_H */
