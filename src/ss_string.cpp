#include "ss_string.h"


void convertUInt32ToChars(const uint32_t val, char *buffer)
{
	buffer[3] = val >> 24;
	buffer[2] = val >> 16;
	buffer[1] = val >> 8;
	buffer[0] = val;

	return;
}


uint32_t convertUCharsToUInt32(const char *val)
{
	uint32_t result = (uint32_t)*val;

	return result;
}


int replaceSubstring(const char *input, char *out, const char *substring, const char *replace)
{
	assert(input != NULL);
	assert(substring != NULL);
	assert(replace != NULL);

	size_t lenInput = findStrLenC(input);
	size_t lenSubstring = findStrLenC(substring);
	size_t lenReplace = findStrLenC(replace);

	int i;
	int substringOccurrences = 0;
	for (i=0; input[i] != '\0'; ++i) {
		const char *found = findSubStr(&input[i], substring);
		if (found == &input[i]) {
			substringOccurrences++;
			i += lenSubstring - 1; // NOTE: (sonictk) Jump past the substring that was found.
		}
	}

	// NOTE: (sonictk) i is num. of chars without the tokens. Middle part is how much is required
	// for storing the replaced tokens. Final +1 is for null terminator.
	int minSize = i + (substringOccurrences * (replace - substring)) + 1;

	if (out == NULL) {

		return minSize;
	}

	if (substringOccurrences == 0) {

		return 0;
	}

	i = 0;
	while (*input) {
		const char *found = findSubStr(input, substring);
		// NOTE: (sonictk) If we found the substring, copy the replacement string to that location
		// and jump ahead by the replacement string's length. If we didn't find the substring, copy the
		// characters at that location instead.
		if (found == input) {
			copyStringN(&out[i], replace, findStrLenC(replace));
			i += lenReplace;
			input += lenSubstring;
		} else {
			out[i++] = *input++;
		}
	}

	out[i] = '\0';

	return substringOccurrences;
}


int replaceChar(const char *input,
				char *output,
				const char token,
				const char replace,
				unsigned int size)
{
	size_t len = findStrLenC(input);
	if (len <= 0) {
		return 0;
	}
	int replaced = 0;
	unsigned int i = 0;
	for (; i < size && i < (len + 1) && input[i] != '\0'; ++i) {
		if (input[i] == token) {
			output[i] = replace;
			replaced++;
		} else {
			output[i] = input[i];
		}
	}
	output[i] = '\0';

	return replaced;
}


char *IntToCStr(const int i)
{
	int charsNeeded = snprintf(NULL, 0, "%d", i) + 1;
	char *result = (char *)ss_malloc(charsNeeded * sizeof(char));

	int check = snprintf(result, charsNeeded, "%d", i);

	assert(check == charsNeeded);

	return result;
}
