/**
 * @file   ss_ansi.h
 * @brief  Colour codes for terminal emulators using ANSI.
 */
#ifndef SS_ANSI_H
#define SS_ANSI_H

#define SS_ANSI_COLOR_RED     "\x1b[31m"
#define SS_ANSI_COLOR_GREEN   "\x1b[32m"
#define SS_ANSI_COLOR_YELLOW  "\x1b[33m"
#define SS_ANSI_COLOR_BLUE    "\x1b[34m"
#define SS_ANSI_COLOR_MAGENTA "\x1b[35m"
#define SS_ANSI_COLOR_CYAN    "\x1b[36m"
#define SS_ANSI_COLOR_RESET   "\x1b[0m"

#endif /* SS_ANSI_H */
