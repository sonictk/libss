#include "ss_platform.h"
#include "ss_filesys.h"
#include "ss_string.h"
#include "ss_stream.h"
#include "ss_memory.h"
#include "ss_errno.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifdef __cplusplus
#include <cassert>
#include <cstdio>
#include <climits>
#include <cstdint>
#include <cstring>

#else
#include <assert.h>
#include <limits.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>

#endif // __cplusplus


bool doesFileExist(char *filePath)
{
	FILE *fileHandle = fopen(filePath, "r");
	if (fileHandle == NULL) {
		return false;
	} else {
		int status = fclose(fileHandle);
		if (status -1) {
			return false;
		}

		return true;
	}
}


int getAppPath(char *filename, unsigned int len)
{
	ssize_t pathLen = readlink("/proc/self/exe", filename, len);
	if (pathLen == -1) {
		perror("Failed to get executable path!\n");
		return -1;
	}
	return pathLen;
}


int getDirPath(const char *path, char *dirPath)
{
	size_t origPathLen = findStrLenC(path);
	const char *tokenIdx = strrchr(path, SS_OS_PATH_SEPARATOR);
	if (!tokenIdx) {
		perror("Path was unmodified!\n");
		return -1;
	}
	size_t dirPathLen = origPathLen - findStrLenC(tokenIdx);
	copyStringN(dirPath, path, dirPathLen);

	return (int)dirPathLen;
}


FileTime getLastWriteTime(const char *filename)
{
	struct stat attrib = {};
	int statResult = stat(filename, &attrib);
	if (statResult != 0) {
		SS_OSPrintLastError();
		return 0;
	}
	timet mtime = attrib.st_mtim.tv_sec;

	return (FileTime)(unixSecondsToWin32Ticks(mtime));
}


inline int convertPathSeparatorsToOSNative(char *filename)
{
	sizet len = findStrLenC(filename);
	char *tmp = (char *)ss_malloc((len + 1) * sizeof(char));
	int replaced = replaceChar(filename,
							   tmp,
							   WINDOWS_PATH_SEPARATOR,
							   SS_OS_PATH_SEPARATOR,
							   (unsigned int)len + 1);
	if (replaced <= 0) {
		return replaced;
	}

	copyStringN(filename, tmp, len + 1);
	ss_free(tmp);

	return replaced;
}


inline int renameFile(const char *oldPath, const char *newPath)
{
	int result = rename(oldPath, newPath);
	if (result != 0) {
		SS_OSPrintLastError();
		return -1;
	}

	return 0;
}


bool isFile(const char *path)
{
	struct stat st;
	int s = stat(path, &st);
	if (s == 0) {
		return true;
	} else {
		return false;
	}
}


bool isDirectory(const char *path)
{
	struct stat st;
	int s = stat(path, &st);
	if (s != 0) {
		return false;
	}
	bool result = S_ISDIR(st.st_mode);
	return result;
}


SS_OSFileHandle createFile(const char *path, const int access, const int shareMode, const SS_FileCreationDisposition creationDisposition)
{
	FILE *hf = fopen(path, "w");
	if (hf == NULL) {
		SSSetLastError(SS_Status_AccessDenied);
	}
	return hf;
}


SS_OSFileHandle openFile(const char *path, const int access, const int shareMode)
{
	FILE *hf = NULL;
	switch (access) {
	case FILE_ACCESS_GENERIC_READ:
		hf = fopen(path, "r");
		break;
	case FILE_ACCESS_GENERIC_WRITE:
		hf = fopen(path, "W");
		break;
	case FILE_ACCESS_GENERIC_READ|FILE_ACCESS_GENERIC_WRITE:
		hf = fopen(path, "rW");
		break;
	default:
		SSSetLastError(SS_Status_InvalidParameter);
		return NULL;
	}
	return hf;
}


int closeFile(SS_OSFileHandle &hFile)
{
	int st = fclose(hFile);
	return st;
}


bool isRelativePath(const char *path)
{
	if (path[0] == '/') {
		return false;
	} else {
		return true;
	}
}


int absolutePath(char *dest, const char *path, const int len)
{
	if (dest == NULL && len == 0) {
		char *tmpResolvedPath = realpath(path, NULL);
		if (tmpResolvedPath == NULL) {
			return 0;
		}
		size_t lenTmpResolvedPath = strlen(tmpResolvedPath);
		free(tmpResolvedPath);
		return lenTmpResolvedPath;
	}
	char *resolvedPath = realpath(path, dest);
	if (resolvedPath == NULL) {
		return 0;
	}

	size_t lenResolvedPath = strlen(resolvedPath);
	return lenResolvedPath;
}


int setFilePtr(SS_OSFileHandle &hFile, int distance, FilePos movement)
{
	int moveMethod;
	switch (movement) {
	case FilePos_Begin:
		moveMethod = SEEK_SET;
		break;
	case FilePos_Current:
		moveMethod = SEEK_CUR;
		break;
	case FilePos_End:
		moveMethod = SEEK_END;
		break;
	default:
		SSSetLastError(SS_Status_InvalidParameter);
		return -1;
	}

	int st = fseek(hFile, (long)distance, moveMethod);
	if (st != 0) {
		return -2;
	}

	long resultL = ftell(hFile);
	int result = (int)resultL;
	return result;
}

int64_t setFilePtr64(SS_OSFileHandle &hFile, int64_t distance, FilePos movement)
{
	int moveMethod;
	switch (movement) {
	case FilePos_Begin:
		moveMethod = SEEK_SET;
		break;
	case FilePos_Current:
		moveMethod = SEEK_CUR;
		break;
	case FilePos_End:
		moveMethod = SEEK_END;
		break;
	default:
		SSSetLastError(SS_Status_InvalidParameter);
		return -1;
	}

	int st = fseeko(hFile, (off_t)distance, moveMethod);
	if (st != 0) {
		return -2;
	}

	off_t resultL = ftello(hFile);
	int64_t result = (int64_t)resultL;
	return result;
}


int getFilePtrPos(SS_OSFileHandle &hFile)
{
	long resultL = ftell(hFile);
	int result = (int)resultL;
	return result;
}


int readFile(SS_OSFileHandle &hFile, void *buffer, int size)
{
	size_t bytesRead = fread(buffer, 1, (size_t)size, hFile);
	if (bytesRead == 0) {
		int eof = feof(hFile);
		if (eof != 0) {
			SSSetLastError(SS_Status_EndOfFile);
			return -2;
		}
		int err = ferror(hFile);
		if (err != 0) {
			SSSetLastError(SS_Status_FileReadFailure);
			return err;
		} else {
			clearerr(hFile);
			return err;
		}
	}
	if (bytesRead != (size_t)size) {
		SSSetLastError(SS_Status_FileReadFailure);
		int err = ferror(hFile);
		return err;
	}

	int result = (int)bytesRead;
	return result;
}


int64_t readFile64(SS_OSFileHandle &hFile, void *buffer, int64_t size)
{
	assert(size != 0);

	size_t chunkSize = sizeof(int64_t);
	int64_t numChunks = size / (int64_t)chunkSize;
	int64_t finalChunkSize = size % (int64_t)chunkSize;

	int chunkBytesRead = 0;
	int64_t totalBytesRead = 0;
	for (int64_t i=0; i < numChunks; ++i) {
		size_t bytesRead = fread(buffer, 1, (size_t)size, hFile);
		if (bytesRead == 0) {
			int eof = feof(hFile);
			if (eof != 0) {
				SSSetLastError(SS_Status_EndOfFile);
				return -2;
			}
			int err = ferror(hFile);
			if (err != 0) {
				SSSetLastError(SS_Status_FileReadFailure);
				return err;
			} else {
				clearerr(hFile);
				return err;
			}
		}
		if ((int64_t)bytesRead != size) {
			SSSetLastError(SS_Status_FileReadFailure);
			int err = ferror(hFile);
			return err;
		}
		totalBytesRead += chunkBytesRead;
	}

	size_t bytesRead = fread((uint8_t *)buffer + totalBytesRead, 1, (size_t)finalChunkSize, hFile);
	if (bytesRead == 0) {
		int eof = feof(hFile);
		if (eof != 0) {
			SSSetLastError(SS_Status_EndOfFile);
			return -2;
		}
		int err = ferror(hFile);
		if (err != 0) {
			SSSetLastError(SS_Status_FileReadFailure);
			return err;
		} else {
			clearerr(hFile);
			return err;
		}
	}
	if ((int64_t)bytesRead != size) {
		SSSetLastError(SS_Status_FileReadFailure);
		int err = ferror(hFile);
		return err;
	}
	totalBytesRead += finalChunkSize;
	return totalBytesRead;
}


bool writeFile(SS_OSFileHandle &hFile, const void *buffer, int bytesToWrite, int &bytesWritten)
{
	size_t bytesWrittenSz = fwrite(buffer, 1, bytesToWrite, hFile);
	if (bytesWrittenSz == 0) {
		int err = ferror(hFile);
		if (err != 0) {
			SSSetLastError(SS_Status_FileWriteFailure);
			return false;
		} else {
			clearerr(hFile);
			return false;
		}
	}

	bytesWritten = (int)bytesWrittenSz;

	return true;
}


void pathStripPath(char *path)
{
	assert(path != NULL);
	size_t lenPath = strlen(path);
	char *tmp = (char *)ss_malloc((lenPath + 1) * sizeof(char));
	memcpy(tmp, path, lenPath + 1);

	size_t i = lenPath - 1;
	for (; i > 0; --i) {
		if (tmp[i] == '/') {
			break;
		}
	}

	if (i == 0) { return; }
	memset(path, 0, lenPath + 1);
	memcpy(path, tmp + (lenPath - i), i);
	ss_free(tmp);
	return;
}


SS_OSFileHandle createTempFile()
{
	FILE *fh = tmpfile();
	assert(fh != NULL);
	return fh;
}


char *getTempDirPath()
{
	char *tmpTemplate = (char *)ss_malloc((SS_MAX_PATH + 1) * sizeof(char));
	static const char templatePath[] = "/tmp/tmpdir-XXXXXX";
	size_t lenTemplatePath = strlen(templatePath);
	memcpy(tmpTemplate, templatePath, lenTemplatePath + 1);
	char *tmpdirpath = mkdtemp(tmpTemplate);
	if (tmpdirpath == NULL) { return NULL; }
	return tmpTemplate;
}
