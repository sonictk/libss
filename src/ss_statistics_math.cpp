#include "ss_statistics_math.h"


float covariance(const float *x, const float *y, int size)
{
	// NOTE: (sonictk) Implementation based off paper: http://www.cs.otago.ac.nz/cosc453/student_tutorials/principal_components.pdf
	float meanX = mean(x, size);
	float meanY = mean(y, size);

	float runningSum = 0.0f;
	for (int i=0; i < size; ++i) {
		runningSum += ((x[i] - meanX) * (y[i] - meanY));
	}

	float result = runningSum / (float)(size - 1);

	return result;
}

double covariance(const double *x, const double *y, int size)
{
	double meanX = mean(x, size);
	double meanY = mean(y, size);

	double runningSum = 0.0;
	for (int i=0; i < size; ++i) {
		runningSum += ((x[i] - meanX) * (y[i] - meanY));
	}

	double result = runningSum / (double)(size - 1);

	return result;
}

float covariance(const int *x, const int *y, int size)
{
	return covariance((float *)x, (float *)y, size);
}
