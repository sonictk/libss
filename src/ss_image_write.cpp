#include "ss_image_write.h"
#include "ss_filesys.h"

#include <stdio.h>


// NOTE: (sonictk) PPM image format documentation is referenced from:
// http://netpbm.sourceforge.net/doc/ppm.html

static const char SS_PPM_MAGIC_NUMBER[] = "P3";
static const char SS_PPM_NEWLINE[] = "\n";


int ss_write_ppm(const char *filename,
				 const uint8_t const *buffer,
				 const int width,
				 const int height,
				 const int max,
				 const int channels)
{
	assert(max < 65536); // NOTE: (sonictk) This is a limit imposed by the PPM specification.

	// NOTE: (sonictk) Write the file header first, which is just the magic number.
	SS_OSFileHandle hFile = createFile(filename, FILE_ACCESS_GENERIC_WRITE, FILE_ACCESS_SHARE_BLOCK, SS_FileCreationDisposition_CreateNew);
	if (hFile == NULL) {
		return -1;
	}

	int totalBytesWritten = 0;
	int bytesWritten = 0;

	static const size_t SS_PPM_LEN_NEWLINE = findStrLenC(SS_PPM_NEWLINE);

	bool writeResult = writeFile(hFile, SS_PPM_MAGIC_NUMBER, 2, &bytesWritten);
	if (!writeResult) {
		return -2;
	}

	assert(bytesWritten == 2);

	totalBytesWritten += bytesWritten;

	// NOTE: (sonictk) Now write the width/height information.
	writeFile(hFile, SS_PPM_NEWLINE, SS_PPM_LEN_NEWLINE, &bytesWritten);

	assert(bytesWritten == SS_PPM_LEN_NEWLINE);

	totalBytesWritten += bytesWritten;

	int widthStrSize = snprintf(NULL, 0, "%d", width);
	char *widthStr = (char *)ss_malloc((widthStrSize + 1)  * sizeof(char));

	int heightStrSize = snprintf(NULL, 0, "%d", height);
	char *heightStr = (char *)ss_malloc((heightStrSize + 1)  * sizeof(char));

	snprintf(widthStr, widthStrSize, "%d", width);
	snprintf(heightStr, heightStrSize, "%d", height);

	writeFile(hFile, widthStr, widthStrSize + 1, &bytesWritten);

	assert(bytesWritten == widthStrSize + 1);

	totalBytesWritten += bytesWritten;

	writeFile(hFile, " ", 1, &bytesWritten);

	assert(bytesWritten == 1);

	totalBytesWritten += bytesWritten;

	writeFile(hFile, heightStr, heightStrSize + 1, &bytesWritten);

	assert(bytesWritten == 1);

	totalBytesWritten += bytesWritten;

	writeFile(hFile, SS_PPM_NEWLINE, SS_PPM_LEN_NEWLINE, &bytesWritten);

	assert(bytesWritten == SS_PPM_LEN_NEWLINE);

	// NOTE: (sonictk) Now write the max colour information.
	int maxStrSize = snprintf(NULL, 0, "%d", max);
	char *maxStr = (char *)ss_malloc((maxStrSize + 1) * sizeof(char));
	snprintf(maxStr, maxStrSize, "%d", max);

	writeFile(hFile, maxStr, maxStrSize + 1, &bytesWritten);

	assert(bytesWritten == maxStrSize + 1);

	totalBytesWritten += bytesWritten;

	writeFile(hFile, SS_PPM_NEWLINE, SS_PPM_LEN_NEWLINE, &bytesWritten);

	assert(bytesWritten == SS_PPM_LEN_NEWLINE);

	// NOTE: (sonictk) Now write the pixel information.

	ss_free(widthStr);
	ss_free(heightStr);
	ss_free(maxStr);

	return totalBytesWritten;
}
