#include "ss_uuid.h"


#define SS_UUID_NULL NULL

typedef struct
{
	char uuidStr[SS_UUID_STRING_LENGTH + 1];
} SSUuid;


#ifdef __cplusplus

#include <cstdio>
#include <cstring>

#else

#include <stdio.h>
#include <string.h>

#endif // __cplusplus


int createUuid(SSUuid &uuid)
{
	FILE *f = fopen("/proc/sys/kernel/random/uuid", "rb");
	if (f == NULL) {
		return -1;
	}

	size_t numRead = fread(uuid.uuidStr, 1, SS_UUID_STRING_LENGTH, f);
	if (numRead != SS_UUID_STRING_LENGTH) {
		return -2;
	}
	uuid.uuidStr[SS_UUID_STRING_LENGTH] = '\0';

	return 0;
}


int getUuidAsString(const SSUuid &uuid, char name[SS_UUID_STRING_LENGTH])
{
	copyStringN(name, uuid.uuidStr, SS_UUID_STRING_LENGTH);

	return 0;
}


int getUuid(const char name[SS_UUID_STRING_LENGTH], SSUuid &uuid)
{
	copyStringN(uuid.uuidStr, name, SS_UUID_STRING_LENGTH);

	return 0;
}


bool areUuidsEqual(SSUuid uuid1, SSUuid uuid2)
{
	int result = strNCmp(uuid1.uuidStr, uuid2.uuidStr, SS_UUID_STRING_LENGTH);
	if (result == 0) {
		return true;
	} else {
		return false;
	}
}
