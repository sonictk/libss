/**
 * @file   ss_platform_linux.h
 * @brief  Platform layer for Linux.
 */
#ifndef SS_PLATFORM_LINUX_H
#define SS_PLATFORM_LINUX_H

/// TLS storage specifier.
#define SS_ThreadLocalStorage __thread


/// Macros for defining symbols to be exported on Linux.
#define SS_DLLExport __attribute__ ((visibility ("default")))
#define SS_DLLImport __attribute__ ((visibility ("default")))
#define SS_GCCExport __attribute__ ((visibility ("default")))


#define SS_MAX_PATH PATH_MAX
#define SS_OS_PATH_SEPARATOR '/'
#define SS_OS_ALT_PATH_SEPARATOR '\\'

/// A handle to a file.
typedef FILE *SS_OSFileHandle;

/// A handle to a **Shared Object**.
typedef void * SS_DLLHandle;



/// A function pointer to a symbol in a **Shared Object**.
typedef void * SS_FuncPtr;


#endif /* SS_PLATFORM_LINUX_H */
