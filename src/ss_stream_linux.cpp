#include "ss_stream.h"

#ifdef __cplusplus
#include <cstring>
#include <cerrno>

#else
#include <errno.h>
#include <string.h>

#endif // __cplusplus


void SS_OSPrintLastError()
{
	perror(strerror(errno));
}
