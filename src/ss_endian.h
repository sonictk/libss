#ifndef SS_ENDIAN_H
#define SS_ENDIAN_H


#ifdef __cplusplus
#include <cstdint>
using std::int8_t;
using std::int16_t;
using std::int32_t;
using std::int64_t;

using std::uint8_t;
using std::uint16_t;
using std::uint32_t;
using std::uint64_t;

#else
#include <stdint.h>

#endif


/**
 * Swaps the endianness of a given unsigned 8-bit value in-place.
 *
 * @param val 	The value to swap the endianness for.
 */
void swapEndianU8(uint8_t &val);


/**
 * Swaps the endianness of a given signed 8-bit value in-place.
 *
 * @param val 	The value to swap the endianness for.
 */
void swapEndian8(int8_t &val);


/**
 * Swaps the endianness of a given unsigned 16-bit value in-place.
 *
 * @param val 	The value to swap the endianness for.
 */
void swapEndianU16(uint16_t &val);


/**
 * Swaps the endianness of a given signed 16-bit value in-place.
 *
 * @param val 	The value to swap the endianness for.
 */
void swapEndian16(int16_t &val);


/**
 * Swaps the endianness of a given unsigned 32-bit value in-place.
 *
 * @param val 	The value to swap the endianness for.
 */
void swapEndianU32(uint32_t &val);


/**
 * Swaps the endianness of a given signed 32-bit value in-place.
 *
 * @param val 	The value to swap the endianness for.
 */
void swapEndian32(int32_t &val);


/**
 * Swaps the endianness of a given unsigned 64-bit value in-place.
 *
 * @param val 	The value to swap the endianness for.
 */
void swapEndianU64(uint64_t &val);


/**
 * Swaps the endianness of a given signed 64-bit value in-place.
 *
 * @param val 	The value to swap the endianness for.
 */
void swapEndian64(int64_t &val);


/**
 * Swaps the endianness of a given variable-sized buffer.
 *
 * @param val	A pointer to a buffer containing the data to swap the endianness for.
 * @param len	The length of the buffer containing the data.
 */
void swapEndian(uint8_t &val, const int len);


#endif /* SS_ENDIAN_H */
