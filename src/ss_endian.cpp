#include "ss_endian.h"
#include "ss_memory.h"

#ifdef __cplusplus
#include <cassert>
#else
#include <assert.h>
#endif


void swapEndianU8(uint8_t &val)
{
	// NOTE: (sonictk) Thus for a &value of:
	// 10 10 01 00 it will become:
	// 0100 0000 | 0000 1010
	// which gives:
	// 0100 1010
	val = (val << 4) | (val >> 4);

	return;
}


void swapEndian8(int8_t &val)
{
	// NOTE: (sonictk) Thus for a &value of:
	// 11 11 00 10 it will become:
	// 0010 0000 | (0000 1111 & 1111 1111)
	// 0010 0000 | (0000 1111)
	// which gives:
	// 00 10 11 11
	// The AND mask is to ensure that the sign bit is removed. (if the platform inserts 1s after a bit-shift op).
	val = (val << 4) | ((val >> 4) & 0xF);

	return;
}


void swapEndianU16(uint16_t &val)
{
	// NOTE: (sonictk) Thus for a &value of:
	// 01 11 00 00 10 10 01 00 it will become:
	// 1010 0100 0000 0000 | 0000 0000 0111 0000
	// which gives:
	// 10 10 01 00 01 11 00 00
	val = (val << 8) | (val >> 8);

	return;
}


void swapEndian16(int16_t &val)
{
	// NOTE: (sonictk) Thus for a &value of:
	// 11 11 00 00 10 10 01 00 it will become:
	// 1010 0100 0000 0000 | (0000 0000 1111 0000 & 1111 1111 1111 1111)
	// 1010 0100 0000 0000 | (0000 0000 1111 0000)
	// which gives:
	// 10 10 01 00 11 11 00 00
	// The AND mask is to ensure that the sign bit is removed.
	val = (val << 8) | ((val >> 8) & 0xFF);

	return;
}


void swapEndianU32(uint32_t &val)
{
	// NOTE: (sonictk) Thus for a &value of:
	//  0A BB CC DD it will become:

	//  BB CC DD 00 &
	//  FF 00 FF 00            |    00 0A BB CC &
	//                              00 FF 00 FF
	//  BB 00 DD 00 | 00 0A 00 CC
	//  BB 0A DD CC
	val = ((val << 8) & 0xFF00FF00) | ((val >> 8) & 0x00FF00FF);

	//  DD CC 00 00 | 00 00 BB 0A
	//  DD CC BB 0A   Boom, the correct result!
	val = (val << 16) | (val >> 16);

	return;
}


void swapEndian32(int32_t &val)
{
	// NOTE: (sonictk) Thus for a &value of:
	//  AA BB CC DD it will become:

	//  BB CC DD 00 & FF 00 FF 00    |    00 AA BB CC & 00 FF 00 FF
	//  BB 00 DD 00 | 00 AA 00 CC
	//  BB AA DD CC
	val = ((val << 8) & 0xFF00FF00) | ((val >> 8) & 0x00FF00FF);

	//  DD CC 00 00 |        00 00 BB AA & 00 00 FF FF
	//  DD CC BB AA   Exact same thing, except that we AND mask to remove the sign bit (if the platform inserts 1s after a bit-shift op).
	val = (val << 16) | (val >> 16 & 0x0000FFFF);

	return;
}


void swapEndianU64(uint64_t &val)
{
	// NOTE: (sonictk) Thus for a &value of:
	//    AA BB CC DD EE FF GG HH it will become:
	//
	//    BB CC DD EE FF GG HH 00 &
	//    FF 00 FF 00 FF 00 FF 00     |    00 AA BB CC DD EE FF GG &
	//                                     00 FF 00 FF 00 FF 00 FF
	//    BB 00 DD 00 FF 00 HH 00     |
	//    00 AA 00 CC 00 EE 00 GG
	//    BB AA DD CC FF EE HH GG
	val = ((val << 8) & 0xFF00FF00FF00FF00) | ((val >> 8) & 0x00FF00FF00FF00FF);

	//    DD CC FF EE HH GG 00 00 &            00 00 BB AA DD CC FF EE &
	//    FF FF 00 00 FF FF 00 00     |        00 00 FF FF 00 00 FF FF
	//
	//    DD CC 00 00 HH GG 00 00     |
	//    00 00 BB AA 00 00 FF EE
	//    DD CC BB AA HH GG FF EE
	val  = ((val << 16) & 0xFFFF0000FFFF0000) | ((val >> 16) & 0x0000FFFF0000FFFF);

	//    HH GG FF EE 00 00 00 00 | 00 00 00 00 DD CC BB AA
	//    HH GG FF EE DD CC BB AA
	val = (val << 32) | (val >> 32);

	return;
}


void swapEndian64(int64_t &val)
{
	// NOTE: (sonictk) Thus for a &value of:
	//    AA BB CC DD EE FF GG HH it will become:
	//
	//    BB CC DD EE FF GG HH 00 &
	//    FF 00 FF 00 FF 00 FF 00     |    00 AA BB CC DD EE FF GG &
	//                                     00 FF 00 FF 00 FF 00 FF
	//    BB 00 DD 00 FF 00 HH 00     |
	//    00 AA 00 CC 00 EE 00 GG
	//    BB AA DD CC FF EE HH GG
	val = ((val << 8) & 0xFF00FF00FF00FF00) | ((val >> 8) & 0x00FF00FF00FF00FF);

	//    DD CC FF EE HH GG 00 00 &            00 00 BB AA DD CC FF EE &
	//    FF FF 00 00 FF FF 00 00     |        00 00 FF FF 00 00 FF FF
	//
	//    DD CC 00 00 HH GG 00 00     |
	//    00 00 BB AA 00 00 FF EE
	//    DD CC BB AA HH GG FF EE
	val  = ((val << 16) & 0xFFFF0000FFFF0000) | ((val >> 16) & 0x0000FFFF0000FFFF);

	//    HH GG FF EE 00 00 00 00 | 00 00 00 00 DD CC BB AA
	//    HH GG FF EE DD CC BB AA
	val = (val << 32) | (val >> 32 & 0xFFFFFFFF); // NOTE: (sonictk) Mask out the sign bit that may be introduced by platform implementation after a bit-shift operation.

	return;
}


void swapEndian(uint8_t *val, const int len)
{
	assert(val != NULL);

	if (len == 0) {
		return;
	}

	uint8_t *tmpVal = (uint8_t *)ss_malloc(len * sizeof(uint8_t));

	assert(tmpVal != NULL);

	// TODO: (sonictk) Figure out a faster way to swap endianness for a variable-sized buffer.
	// This works, but is slow.
	for (int i=0; i < len; ++i) {
		tmpVal[i] = val[len - i - 1];
	}

	for (int i=0; i < len; ++i) {
		val[i] = tmpVal[i];
	}

	ss_free(tmpVal);

	return;
}
