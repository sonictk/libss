#include "ss_timer.h"

#include <Windows.h>


/// Used for timer operations.
typedef LARGE_INTEGER HighResTimerVal;

/// Used for storing tick counts since the system was started.
typedef DWORD OSTickCount;


/// Performance timer API data structure.
struct SSPerfTimer
{
	/// On Windows, this will store the CPU frequency for the counters.
	/// On Linux/OSX, this is undefined.
	LARGE_INTEGER freq;

	/// When this timer was started.
	LARGE_INTEGER start;

	/// When this timer was stopped.
	LARGE_INTEGER end;
};


int resetSSPerfTimerValue(SSPerfTimer &timer)
{
	BOOL query = QueryPerformanceFrequency(&timer.freq);
	if (query == 0) {
		perror("Failed to initialize a timer!\n");
		return -1;
	}
	timer.start.QuadPart = 0;
	timer.end.QuadPart = 0;

	return 0;
}


SSPerfTimer perfTimer()
{
	SSPerfTimer result;
	resetSSPerfTimerValue(result);

	return result;
}


int startSSPerfTimer(SSPerfTimer &timer)
{
	BOOL result = QueryPerformanceCounter(&timer.start);
	if (result == 0) {
		perror("Starting the performance timer failed!\n");
		return -1;
	}

	return 0;
}


int endSSPerfTimer(SSPerfTimer &timer)
{
	BOOL result = QueryPerformanceCounter(&timer.end);
	if (result == 0) {
		perror("Stopping the performance timer failed!\n");
		return -1;
	}

	return 0;
}


double getOSSSPerfTimerValue(const SSPerfTimer &timer)
{
	return (double)(timer.end.QuadPart - timer.start.QuadPart) * 1.0 / (double)timer.freq.QuadPart;
}


double getSSPerfTimerValue(const SSPerfTimer &timer)
{
	return getOSSSPerfTimerValue(timer) * 1000000.0; // NOTE: (sonictk) Convert to microseconds
}


#ifdef SS_ENABLE_GLOBAL_TIMER
int startSSPerfTimer()
{
	kGlobalSSPerfTimer = perfTimer();
	return startSSPerfTimer(kGlobalSSPerfTimer);
}


int endSSPerfTimer()
{
	return endSSPerfTimer(kGlobalSSPerfTimer);
}


double getOSSSPerfTimerValue()
{
	return getOSSSPerfTimerValue(kGlobalSSPerfTimer);
}


double getSSPerfTimerValue()
{
	return getOSSSPerfTimerValue(kGlobalSSPerfTimer) * 1000000.0; // NOTE: (sonictk) Convert to microseconds
}
#endif // SS_ENABLE_GLOBAL_TIMER


OSTickCount getOSTickCount()
{
	return GetTickCount();
}


void sleepOS(int interval)
{
	Sleep(interval);
}
