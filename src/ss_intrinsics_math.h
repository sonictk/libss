/**
 * @file   ss_intrinsics_math.h
 * @brief  Intrinsics-related math functions.
 */
#ifndef SS_INTRINSICS_MATH_H
#define SS_INTRINSICS_MATH_H

#include "ss_instrset.h"

#if INSTRSET >= 2 // NOTE: (sonictk) Require SSE2 support for these intrinsics

inline float squareRoot(const float val)
{
	float result = _mm_cvtss_f32(_mm_sqrt_ss(_mm_set_ss(val)));
	return result;
}

#else // NOTE: (sonictk) Fallback implementation


#ifdef __cplusplus
#include <cmath>

using std::sqrtf;

#else
#include <math.h>

#endif // __cplusplus


inline float squareRoot(const float val)
{
	return sqrtf(val);
}

#endif /* INSTRSET */

#endif /* SS_INTRINSICS_MATH_H */
