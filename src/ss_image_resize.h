/**
 * @file   ss_image_resize.h
 * @brief  Library for handling image resize operations.
 */
#ifndef SS_IMAGE_RESIZE_H
#define SS_IMAGE_RESIZE_H


/**
 * Resizes image data using nearest-neighbour interpolation.
 *
 * @param dest				The buffer to store the resultant data. This should be at least
 * 						``newWidth * newHeight * numChannels`` large in size.
 * @param src				The source image data in pixel-order format.
 * @param origWidth		The width of the source image data.
 * @param origHeight		The height of the source image data.
 * @param newWidth			The desired new width of the result image.
 * @param newHeight		The desired new height of the result image.
 * @param numChannels		The number of channels in the original source image.
 * @param bytesPerChannel	The number of bytes per channel in the original source image.
 */
void resizeImgDataNearestNeighbour(uint8_t *dest,
								   const uint8_t *src,
								   const int origWidth,
								   const int origHeight,
								   const int newWidth,
								   const int newHeight,
								   const int numChannels,
								   const int bytesPerChannel);


/**
 * Resizes image data using bilinear interpolation with a 2x2 pixel filter.
 *
 * @param dest				The buffer to store the resultant data. This should be at least
 * 						``newWidth * newHeight * numChannels`` large in size.
 * @param src				The source image data in pixel-order format.
 * @param origWidth		The width of the source image data.
 * @param origHeight		The height of the source image data.
 * @param newWidth			The desired new width of the result image.
 * @param newHeight		The desired new height of the result image.
 * @param numChannels		The number of channels in the original source image.
 * @param bytesPerChannel	The number of bytes per channel in the original source image.
 */
void resizeImgDataBilinear(uint8_t *dest,
						   const uint8_t *src,
						   const int origWidth,
						   const int origHeight,
						   const int newWidth,
						   const int newHeight,
						   const int numChannels,
						   const int bytesPerChannel);


#endif /* SS_IMAGE_RESIZE_H */
