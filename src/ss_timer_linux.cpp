#include "ss_timer.h"
#include <time.h>
#include <sys/time.h>
#include <unistd.h>


/// Used for timer operations.
typedef double HighResTimerValue; // NOTE: (sonictk) This is a double since we divide by CPU frequency on Windows.
typedef HighResTimerValue HighResTimerVal;

/// Used for storing tick counts since the system was started.
typedef long OSTickCount;


/// Performance timer API data structure.
struct SSPerfTimer
{
	/// On Windows, this will store the CPU frequency for the counters.
	/// On Linux/OSX, this is undefined.
	double freq;

	/// When this timer was started.
	double start;

	/// When this timer was stopped.
	double end;
};


int resetSSPerfTimerValue(SSPerfTimer &timer)
{
	timer.freq = 0.0;
	timer.start = 0.0;
	timer.end = 0.0;

	return 0;
}


SSPerfTimer perfTimer()
{
	SSPerfTimer result;
	resetSSPerfTimerValue(result);

	return result;
}


int startSSPerfTimer(SSPerfTimer &timer)
{
	timespec res;
	int result = clock_gettime(CLOCK_MONOTONIC, &res);
	timer.start = double((res.tv_sec * 1000000000) + res.tv_nsec);

	return result;
}


int endSSPerfTimer(SSPerfTimer &timer)
{
	timespec res;
	int result = clock_gettime(CLOCK_MONOTONIC, &res);
	timer.end = double((res.tv_sec * 1000000000) + res.tv_nsec);

	return result;
}


double getOSSSPerfTimerValue(const SSPerfTimer &timer)
{
	return (timer.end - timer.start);
}


double getSSPerfTimerValue(const SSPerfTimer &timer)
{
	return getOSSSPerfTimerValue(timer) / 1000.0; // NOTE: (sonictk) Convert to microseconds
}


#ifdef SS_ENABLE_GLOBAL_TIMER
int startSSPerfTimer()
{
	kGlobalSSPerfTimer = perfTimer();
	return startSSPerfTimer(kGlobalSSPerfTimer);
}


int endSSPerfTimer()
{
	return endSSPerfTimer(kGlobalSSPerfTimer);
}


double getOSSSPerfTimerValue()
{
	return getOSSSPerfTimerValue(kGlobalSSPerfTimer);
}


double getSSPerfTimerValue()
{
	return getOSSSPerfTimerValue(kGlobalSSPerfTimer) / 1000.0; // NOTE: (sonictk) Convert to microseconds
}
#endif // SS_ENABLE_GLOBAL_TIMER


OSTickCount getOSTickCount()
{
	struct timeval tv;
	if(gettimeofday(&tv, NULL) != 0) {
		return 0;
	}

	return (tv.tv_sec * 1000) + (tv.tv_usec / 1000);
}


void sleepOS(int interval)
{
	// TODO: (sonictk) Consider using clock_nanosleep instead for higher accuracy
	usleep((useconds_t)(interval * 1000));
}
