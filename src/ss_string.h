/**
 * @file   ss_string.h
 * @brief  String-related functions.
 */
#ifndef SS_STRING_H
#define SS_STRING_H


#ifdef __cplusplus
#include <cstdint>
#include <cstring>

#else
#include <stdint.h>
#include <string.h>

#endif // __cplusplus


#ifdef _WIN32
#include "ss_string_win32.cpp"

#elif __linux__ || __APPLE__
#include "ss_string_linux.cpp"

#else
#error "Unsupported OS platform for compilation."

#endif // OS layer


/**
 * Converts a single unsigned 32-bit value to an array of ``char``.
 *
 * @param val		The value to convert.
 * @param buffer	The buffer to store the resulting array in. This must be a buffer of at least 4 bytes in size.
 */
void convertUInt32ToChars(const uint32_t val, char *buffer);


/**
 * Converts ``uint32_t`` values to a ``char`` array.
 *
 * @param val 	A pointer to an array of unsigned 32-bit values to convert.
 *
 * @return 	A pointer to an array of ``const char`` equivalents.
 */
uint32_t convertUCharsToUInt32(const char *val);


/**
 * Compares a specified number of characters from the beginning of two strings to
 * determine if they are the same. The comparison is case-sensitive.
 *
 * @param lhs 	A pointer to the first null-terminated string to be compared.
 * @param rhs 	A pointer to the second null-terminated string to be compared.
 * @param count The number of characters from the beginning of each string to be compared.
 *
 * @return 	Returns zero if the strings are identical. Returns a positive value if the
 * 			first nChar characters of the string pointed to by lhs are greater than those from the
 * 			string pointed to by rhs. It returns a negative value if the first count characters of
 * 			the string pointed to by lhs are less than those from the string pointed to by rhs.
 */
int strNCmp(const char *lhs, const char *rhs, const int count);


/**
 * Finds the last occurrence of ch (after conversion to char as if by (char)ch) in the
 * null-terminated byte string pointed to by str (each character interpreted as unsigned char).
 * The terminating null character is considered to be a part of the string and can be found if
 * searching for '\0'.
 *
 * @param str    pointer to the null-terminated byte string to be analyzed.
 * @param ch     character to search for.
 *
 * @return		Pointer to the found character in str, or null pointer if no such character is found.
 */
char *findRChar(char *str, const int ch);

/// @overload
const char *findRChar(const char *str, const int ch);


/**
 * Returns the length of the given byte string, that is, the number of characters in a character array whose first element is pointed to by str up to and not including the first null character.
 *
 * @param str 	The string to examine.
 *
 * @return 	The number of characters. If an error occurred, returns a negative value.
 */
size_t findStrLenC(const char *str);


/**
 * Finds the first occurrence of a substring within a string. The comparison is case-sensitive.
 *
 * @param str 	The string to search within.
 * @param tgt 	The substring to search for.
 *
 * @return 	Returns the address of the first occurrence of the matching substring if successful, or NULL otherwise.
 */
char *findSubStr(char *str, const char *tgt);

/// @overload
const char *findSubStr(const char *str, const char *tgt);


/**
 * This function replaces the given ``token`` in the ``input`` string.
 *
 * @param input		The input string to replace characters in.
 * @param output		A buffer that will have the new string written to. Should
 * 					be the ``size`` specified.
 * @param token		The search character to replace.
 * @param replace		The character to replace it with.
 * @param size			The number of characters to write to the new string.
 *
 * @return				The number of times the ``token`` was replaced. Returns a
 * 					negative value on error.
 */
int replaceChar(const char *input,
				char *output,
				const char token,
				const char replace,
				unsigned int size);


/**
 * Replaces all instances of ``substring`` with ``replace`` in the given ``input`` string.
 *
 * @param input			The string to replace the substring in.
 *
 * @param output			The buffer to write the output to. If specified as ``NULL``, this
 * 						function will return the size of the buffer required to store the result,
 * 						in bytes.
 *
 * @param substring		The substring to search for.
 *
 * @param replace 			The new string to replace all substring occurrences with.
 *
 * @return					Returns the number of times the substring was replaced. If ``output`` was
 * 						specified as ``NULL``, the size of the buffer required to store the result
 * 						is returned instead.
 */
int replaceSubstring(const char *input, char *output, const char *substring, const char *replace);


/**
 * Copies the contents of ``src`` to ``dest``.
 *
 * @param dest 		The destination buffer to copy the string to. The resulting
 * 					string is guaranteed to be null-terminated. If the array
 * 					is not wide enough, the behaviour is undefined.
 *
 * @param src			The source string to copy from.
 *
 * @param count 		The maximum number of characters to copy.
 *
 * @return				A copy of ``dest``.
 */
char *copyStringN(char *dest, const char *src, const size_t count);


/**
 * Copies the contents of ``src`` to ``dest``.
 *
 * @param dest 		The destination buffer to copy the string to. The resulting
 * 					string is guaranteed to be null-terminated. If the array
 * 					is not wide enough, the behaviour is undefined.
 * @param src			The source string to copy from.
 *
 * @return				A copy of ``dest``.
 */
template<size_t count>
char *copyString(char (&dest)[count], const char *src)
{
	char *result = copyStringN(dest, src, count);

	return result;
}


/**
 * Converts a string that represents a decimal value to an integer.
 *
 * @param str 		A pointer to the null-terminated string to be converted. A valid
 * 				string representing a decimal value contains only the characters 0-9 and must
 * 				have the following form to be parsed successfully.
 *
 * 				(optional white space)(optional sign)(one or more decimal digits)
 *
 * 				The optional sign can be the character '-' or '+'; if omitted, the sign is
 * 				assumed to be positive.
 *
 * @return			Returns the int value represented by ``str``. For instance, the string "123" returns the integer value 123.
 */
int CStrToInt(const char *str);


/**
 * Allocates and returns a null-terminated byte string that accurately represents the integer ``i``.
 * The caller is responsible for freeing the memory allocated by this function.
 *
 * @param i		The integer to get the string representation for.
 *
 * @return			A pointer to the null-terminated byte string that represents the integer.
 */
char *IntToCStr(const int i);


#endif /* SS_STRING_H */
