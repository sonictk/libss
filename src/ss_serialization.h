/**
 * @file   ss_serialization.h
 * @brief  Functions for dealing with reading/writing data to files.
 */
#ifndef SS_SERIALIZATION_H
#define SS_SERIALIZATION_H

#include "ss_platform.h"


#ifdef __cplusplus

#include <climits>

#else

#include <limits.h>

#endif // __cplusplus


/// Various status codes for the result of a read operation.
enum SS_ReadResult
{
	SS_ReadResult_Failure = INT_MIN,
	SS_ReadResult_FileOpenFailure,
	SS_ReadResult_OutOfMemory,
	SS_ReadResult_ReadFailure,
	SS_ReadResult_Success = 0
};

/**
 * This function reads the contents of the given ``filePath``.
 *
 * @param filePath 	The file to read from.
 * @param status		The storage for the operation result. If successful, will
 * 					be set to ``SS_ReadResult_Success``.
 *
 * @return 			A pointer to the content read. This should be deallocated
 * 					manually once it is no longer needed using ``free`` or
 * 					other memory management mechanism.
 */
uint8 *readText(const char *filePath, SS_ReadResult &status);

/// @overload
uint8 *readText(const char *filePath);


#endif /* SS_SERIALIZATION_H */
