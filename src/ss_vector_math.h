/**
 * @file   ss_vector_math.h
 * @brief  Simple library for basic math vector types.
 */
#ifndef SS_VECTOR_MATH_H
#define SS_VECTOR_MATH_H

#include "ss_intrinsics_math.h"
#include "ss_common_math.h"


#ifdef __cplusplus

#include <cmath>

#else

#include <math.h>

#endif // __cplusplus


enum RotationOrder {
	kXYZ,
	kYZX,
	kZXY,
	kXZY,
	kYXZ,
	kZYX
};


/// Simple 2D vector for math operations.
union Vec2
{
	struct {
		float x, y;
	};
	struct {
		float u, v;
	};
	float e[2];

	inline float &operator[](int index) {
		return e[index];
	}

	inline const float &operator[](int index) const {
		return e[index];
	}
};

inline Vec2 vec2(float x, float y)
{
	Vec2 result;
	result.x = x;
	result.y = y;
	return result;
}

inline Vec2 vec2(int x, int y)
{
	Vec2 result = {{(float)x, (float)y}};
	return result;
}

inline Vec2 vec2(const float vals[2])
{
	Vec2 result = {vals[0], vals[1]};
	return result;
}

inline Vec2 vec2()
{
	return vec2(0, 0);
}

inline Vec2 vec2(const Vec2 vec)
{
	Vec2 result = {vec.x, vec.y};
	return result;
}


#ifdef __cplusplus

template<typename Tx, typename Ty>
Vec2 vec2(Tx x, Ty y)
{
	return vec2(static_cast<float>(x), static_cast<float>(y));
}

template<typename T>
inline Vec2 vec2(const T vals[2])
{
	Vec2 result = vec2(vals[0], vals[1]);
	return result;
}

#endif // __cplusplus


inline Vec2 operator*(const float factor, const Vec2 &v)
{
	Vec2 result;
	result.x = factor * v.x;
	result.y = factor * v.y;
	return result;
}

inline Vec2 operator*(const Vec2 &v, const float factor)
{
	Vec2 result = factor * v;
	return result;
}

inline Vec2 &operator*=(Vec2 &v, const float factor)
{
	v = v * factor;
	return v;
}

inline Vec2 operator/(const Vec2 &v, const float factor)
{
	Vec2 result;
	result.x = v.x / factor;
	result.y = v.y / factor;
	return result;
}

inline Vec2 operator/(const float factor, const Vec2 &v)
{
	return v / factor;
}

inline Vec2 &operator/=(Vec2 &v, const float factor)
{
	v = v / factor;
	return v;
}

inline Vec2 operator-(const Vec2 &v)
{
	Vec2 result;
	result.x = -v.x;
	result.y = -v.y;
	return result;
}

inline Vec2 operator+(const Vec2 &v1, const Vec2 &v2)
{
	Vec2 result;
	result.x = v1.x + v2.x;
	result.y = v1.y + v2.y;
	return result;
}

inline Vec2 &operator+=(Vec2 &v1, const Vec2 &v2)
{
	v1 = v1 + v2;
	return v1;
}

inline Vec2 operator-(const Vec2 &v1, const Vec2 &v2)
{
	Vec2 result;
	result.x = v1.x - v2.x;
	result.y = v1.y - v2.y;
	return result;
}

inline Vec2 &operator-=(Vec2 &v1, const Vec2 &v2)
{
	v1 = v1 - v2;
	return v1;
}

inline bool operator==(const Vec2 &v1, const Vec2 &v2)
{
	if (areFloatsEqual(v1.x, v2.x) && areFloatsEqual(v1.y, v2.y)) {
		return true;
	} else {
		return false;
	}
}

inline bool operator!=(const Vec2 &v1, const Vec2 &v2)
{
	return !(v1 == v2);
}

/**
 * Calculates the inner product (dot product) of the given vectors.
 *
 * @param v1 	The first vector.
 * @param v2 	The second vector.
 *
 * @return 	The inner product.
 */
inline float innerProduct(const Vec2 &v1, const Vec2 &v2)
{
	float result = (v1.x * v2.x) + (v1.y * v2.y);
	return result;
}

/**
 * Calculates the length of the given vector.
 *
 * @param v	The vector.
 *
 * @return		Its length.
 */
inline float length(const Vec2 &v)
{
	float result = squareRoot(innerProduct(v, v));
	return result;
}

/**
 * Normalizes the given vector to have a length of ``1``. (Unit vector)
 * If the vector has no length, the result is *undefined*.
 *
 * @param v	The vector.
 *
 * @return		Its normalized version.
 */
inline Vec2 normalize(const Vec2 &v)
{
	Vec2 result = v * (1.0f / length(v));
	return result;
}


/**
 * Calculates the inner angle between the two given vectors.
 *
 * @param v1 	The first vector.
 * @param v2 	The second vector.
 *
 * @return 	The inner angle between the two vectors in *degrees*.
 */
inline float angleBetween(const Vec2 &v1, const Vec2 &v2)
{
	float dot = innerProduct(v1, v2);
	if (dot == 0.0f) {
		return 90.0f;
	}

	float result = acosf(dot / length(v1) * length(v2));

	return result;
}


/**
 * Linearly interpolate between the two vectors.
 *
 * @param a 	The first vector.
 * @param b 	The second vector.
 * @param t 	The interpolation factor.
 *
 * @return 	The interpolated vector.
 */
inline Vec2 lerp(Vec2 &v1, float t, Vec2 &v2)
{
	Vec2 result = ((1.0f - t) * v1) + (t * v2);
	return(result);
}


/// Simple 3D vector for math operations.
union Vec3
{
	struct {
		float x, y, z;
	};
	struct {
		float r, g, b;
	};
	struct {
		float u, v, w;
	};
	struct {
		Vec2 xy;
		float _ignored0;
	};
	struct {
		float _ignored1;
		Vec2 yz;
	};
	float e[3];

	inline float &operator[](int index) {
		return e[index];
	}

	inline const float &operator[](int index) const {
		return e[index];
	}
};

inline Vec3 vec3(float x, float y, float z)
{
	Vec3 result;
	result.x = x;
	result.y = y;
	result.z = z;
	return result;
}

inline Vec3 vec3(const float vals[3])
{
	Vec3 result;
	result.x = vals[0];
	result.y = vals[1];
	result.z = vals[2];

	return result;
}

inline Vec3 vec3(int x, int y, int z)
{
	Vec3 result = {{(float)x, (float)y, (float)z}};
	return result;
}

inline Vec3 vec3()
{
	return vec3(0, 0, 0);
}

inline Vec3 vec3(const Vec3 vec)
{
	Vec3 result = {vec.x, vec.y, vec.z};
	return result;
}


#ifdef __cplusplus

template<typename Tx, typename Ty, typename Tz>
inline Vec3 vec3(Tx x, Ty y, Tz z)
{
	return vec3(static_cast<float>(x), static_cast<float>(y), static_cast<float>(z));
}

template<typename T>
inline Vec3 vec3(T vals[3])
{
	float tmpVals[3];
	for (int i=0; i < 3; ++i) {
		tmpVals[i] = static_cast<float>(vals[i]);
	}

	return vec3(tmpVals);
}

#endif // __cplusplus


inline Vec3 operator*(const float factor, const Vec3 &v)
{
	Vec3 result;
	result.x = factor * v.x;
	result.y = factor * v.y;
	result.z = factor * v.z;
	return result;
}

inline Vec3 operator*(const Vec3 &v, const float factor)
{
	Vec3 result = factor * v;
	return result;
}

inline Vec3 &operator*=(Vec3 &v, const float factor)
{
	v = v * factor;
	return v;
}

inline Vec3 operator/(const Vec3 &v, const float factor)
{
	Vec3 result;
	result.x = v.x / factor;
	result.y = v.y / factor;
	result.z = v.z / factor;
	return result;
}

inline Vec3 operator/(const float factor, const Vec3 &v)
{
	return v / factor;
}

inline Vec3 &operator/=(Vec3 &v, const float factor)
{
	v = v / factor;
	return v;
}

inline Vec3 operator-(const Vec3 &v)
{
	Vec3 result;
	result.x = -v.x;
	result.y = -v.y;
	result.z = -v.z;
	return result;
}

inline Vec3 operator+(const Vec3 &v1, const Vec3 &v2)
{
	Vec3 result;
	result.x = v1.x + v2.x;
	result.y = v1.y + v2.y;
	result.z = v1.z + v2.z;
	return result;
}

inline Vec3 &operator+=(Vec3 &v1, const Vec3 &v2)
{
	v1 = v1 + v2;
	return v1;
}

inline Vec3 operator-(const Vec3 &v1, const Vec3 &v2)
{
	Vec3 result;
	result.x = v1.x - v2.x;
	result.y = v1.y - v2.y;
	result.z = v1.z - v2.z;
	return result;
}

inline Vec3 &operator-=(Vec3 &v1, const Vec3 &v2)
{
	v1 = v1 - v2;
	return v1;
}

inline bool operator==(const Vec3 &v1, const Vec3 &v2)
{
	if (areFloatsEqual(v1.x, v2.x) && areFloatsEqual(v1.y, v2.y) && areFloatsEqual(v1.z, v2.z)) {
		return true;
	} else {
		return false;
	}
}

inline bool operator!=(const Vec3 &v1, const Vec3 &v2)
{
	return !(v1 == v2);
}


#if INSTRSET >= 2 // NOTE: (sonictk) Require SSE2 support for these intrinsics
#include "ss_instrset.h"

inline __m128 loadVec3(const Vec3 &vec)
{
	// NOTE: (sonictk) Referenced from http://fastcpp.blogspot.com/2011/03/loading-3d-vector-into-sse-register.html
	// Load x, y with a 64 bit integer load (result: 00YX), upper 64 bits are zeroed
	__m128i xy = _mm_loadl_epi64((const __m128i*)&vec);
	// NOTE: (sonictk) Now load the z element using a 32 bit float load (000Z)
	__m128 z = _mm_load_ss(&vec.z);
	// NOTE: (sonictk) Now cast the __m128i register into a __m128 one (0ZYX)
	return _mm_movelh_ps(_mm_castsi128_ps(xy), z);

	// NOTE: (sonictk) This alternative is for non 8byte alignment (64bit movq is slow when address of data is not 8byte aligned)
	// __m128 x = _mm_load_ss(&vec.x);
	// __m128 y = _mm_load_ss(&vec.y);
	// __m128 z = _mm_load_ss(&vec.z);
	// __m128 xy = _mm_movelh_ps(x, y);
	// return _mm_shuffle_ps(xy, z, _MM_SHUFFLE(2, 0, 2, 0));
}


inline Vec3 crossProduct(const Vec3 &v1, const Vec3 &v2)
{
	// NOTE: (sonictk) Referenced from http://fastcpp.blogspot.com/2011/04/vector-cross-product-using-sse-code.html
	__m128 a = _mm_setr_ps(v1.x, v1.y, v1.z, 0);
	__m128 b = _mm_setr_ps(v2.x, v2.y, v2.z, 0);

	__m128 sseResult = _mm_sub_ps(
		_mm_mul_ps(_mm_shuffle_ps(a, a, _MM_SHUFFLE(3, 0, 2, 1)), _mm_shuffle_ps(b, b, _MM_SHUFFLE(3, 1, 0, 2))),
		_mm_mul_ps(_mm_shuffle_ps(a, a, _MM_SHUFFLE(3, 1, 0, 2)), _mm_shuffle_ps(b, b, _MM_SHUFFLE(3, 0, 2, 1)))
		);

	float fResult[4];
	_mm_store_ps(fResult, sseResult);

	Vec3 result = {{fResult[0], fResult[1], fResult[2]}};

	return result;
}

#else

/**
 * Calculates the cross product of the given vectors.
 *
 * @param v1 	The first vector.
 * @param v2 	The second vector.
 *
 * @return 	The cross product.
 */
inline Vec3 crossProduct(const Vec3 &v1, const Vec3 &v2)
{
	Vec3 result;
	result.x = (v1.y * v2.z) - (v1.z * v2.y);
	result.y = (v1.z * v2.x) - (v1.x * v2.z);
	result.z = (v1.x * v2.y) - (v1.y * v2.x);
	return result;
}

#endif // INSTRSET


#if INSTRSET >=3 // NOTE: (sonictk) Require SSE 4.1 support for these intrinsics
#include "smmintrin.h"

/**
 * Calculates the inner product (dot product) of the given vectors.
 *
 * @param v1 	The first vector.
 * @param v2 	The second vector.
 *
 * @return 	The inner product.
 */
inline float innerProduct(const Vec3 &v1, const Vec3 &v2)
{
	//__m128 sseV1 = _mm_set_ps(v1.x, v1.y, v1.z, 0);
	//__m128 sseV2 = _mm_set_ps(v2.x, v2.y, v2.z, 0);
	//return _mm_cvtss_f32(_mm_dp_ps(sseV1, sseV2, 0xE1)); // NOTE: (sonictk) This is 1110 0001 in binary

	__m128 sseV1 = loadVec3(v1); // NOTE: (sonictk) loadVec3 loads it in 0ZYX order
	__m128 sseV2 = loadVec3(v2);
	return _mm_cvtss_f32(_mm_dp_ps(sseV1, sseV2, 0x71)); // NOTE: (sonictk) This is 0111 0001 in binary
}

#else
/**
 * Calculates the inner product (dot product) of the given vectors.
 *
 * @param v1 	The first vector.
 * @param v2 	The second vector.
 *
 * @return 	The inner product.
 */
inline float innerProduct(const Vec3 &v1, const Vec3 &v2)
{
	float result = (v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z);
	return result;
}

#endif	// INSTRSET

/**
 * Calculates the length of the given vector.
 *
 * @param v	The vector.
 *
 * @return		Its length.
 */
inline float length(const Vec3 &v)
{
	float result = squareRoot(innerProduct(v, v));
	return result;
}

/**
 * Normalizes the given vector to have a length of ``1``. (Unit vector)
 * If the vector has no length, the result is *undefined*.
 *
 * @param v	The vector.
 *
 * @return		Its normalized version.
 */
inline Vec3 normalize(const Vec3 &v)
{
	Vec3 result = v * (1.0f / length(v));
	return result;
}


/**
 * Calculates the inner angle between the two given vectors.
 *
 * @param v1 	The first vector.
 * @param v2 	The second vector.
 *
 * @return 	The inner angle between the two vectors.
 */
inline float angleBetween(const Vec3 &v1, const Vec3 &v2)
{
	float dot = innerProduct(v1, v2);
	if (dot == 0.0f) {
		return 90.0f;
	}

	float result = acosf(dot / length(v1) * length(v2));

	return result;
}


/**
 * Linearly interpolate between the two vectors.
 * NOTE: This is precise (guarantees result is ``v1`` when t = 1).
 *
 * @param a 	The first vector.
 * @param b 	The second vector.
 * @param t 	The interpolation factor.
 *
 * @return 	The interpolated vector.
 */
inline Vec3 lerp(const Vec3 &v1, float t, const Vec3 &v2)
{
	Vec3 result = ((1.0f - t) * v1) + (t * v2);
	return result;
}


/**
 * Linearly interpolate between the two vectors.
 * NOTE: ``qlerp`` stands for quicklerp; this is imprecise and does not guarantee
 * result is ``v1`` when ``t`` is ``1``,
 * due to floating-point error.
 * May be used when the hardware has a FMA instruction.
 *
 * @param a 	The first vector.
 * @param b 	The second vector.
 * @param t 	The interpolation factor.
 *
 * @return 	The interpolated vector.
 */
inline Vec3 qlerp(const Vec3 &v1, float t, const Vec3 &v2)
{
	Vec3 result = v1 + (t * (v2 - v1));
	return result;
}


/// Describes an Euler rotation. Angles should be specified in **degrees**.
struct EulerRotation
{
	Vec3 angles;
	RotationOrder order;
};

inline EulerRotation eulerRotation(const Vec3 &angles, RotationOrder order)
{
	EulerRotation result;
	result.angles = angles;
	result.order = order;

	return result;
}


/// Simple 4D vector for math operations. Values are stored as floats.
union Vec4
{
	struct {
		union {
			Vec3 xyz;
			struct {
				float x, y, z;
			};
		};
		float w;
	};
	struct {
		union {
			Vec3 rgb;
			struct {
				float r, g, b;
			};
		};
		float a;
	};
	struct {
		Vec2 xy;
		float _ignored0, _ignored1;
	};
	struct {
		float _ignored2;
		Vec2 yz;
		float _ignored3;

	};
	struct {
		float _ignored4;
		float _ignored5;
		Vec2 zw;
	};
	float e[4];

	inline float &operator[](int index) {
		return e[index];
	}

	inline const float &operator[](int index) const {
		return e[index];
	}
};

/// A representation of a quaternion.
typedef Vec4 Quaternion;
typedef Quaternion Quat;

inline Vec4 vec4(float x, float y, float z, float w)
{
	Vec4 result;
	result.x = x;
	result.y = y;
	result.z = z;
	result.w = w;
	return result;
}

inline Vec4 vec4(int x, int y, int z, int w)
{
	Vec4 result = {{(float)x, (float)y, (float)z, (float)w}};
	return result;
}

inline Vec4 vec4(float vals[4])
{
	return vec4(vals[0], vals[1], vals[2], vals[3]);
}

inline Vec4 vec4()
{
	return vec4(0, 0, 0, 0);
}

inline Vec4 vec4(const Vec3 &vec, float w)
{
	return vec4(vec.x, vec.y, vec.z, w);
}

inline Vec4 vec4(const Vec3 &vec)
{
	return vec4(vec, 0);
}

inline Vec4 vec4(const Vec4 &vec)
{
	return vec4(vec.x, vec.y, vec.z, vec.w);
}

#ifdef __cplusplus

template<typename Tx, typename Ty, typename Tz, typename Tw>
Vec4 vec4(const Tx x, const Ty y, const Tz z, const Tw w)
{
	return vec4(static_cast<float>(x),
				static_cast<float>(y),
				static_cast<float>(z),
				static_cast<float>(w));
}

template<typename T>
inline Vec4 vec4(const T vals[4])
{
	return vec4(vals[0], vals[1], vals[2], vals[3]);
}

template<typename T>
inline Vec4 vec4(const Vec3 &vec, const T w)
{
	return vec4(vec.x, vec.y, vec.z, static_cast<float>(w));
}

#endif // __cplusplus

inline Vec4 operator*(const float factor, const Vec4 &v)
{
	Vec4 result;
	result.x = factor * v.x;
	result.y = factor * v.y;
	result.z = factor * v.z;
	result.w = factor * v.w;
	return result;
}

inline Vec4 operator*(const Vec4 &v, const float factor)
{
	Vec4 result = factor * v;
	return result;
}

inline Vec4 &operator*=(Vec4 &v, const float factor)
{
	v = v * factor;
	return v;
}

inline Quat operator*(const Quat &q1, const Quat &q2)
{
	// NOTE: (sonictk) Implementation from: https://www.3dgep.com/understanding-quaternions/#Quaternion_Products
	Quat result;
	result.x = (q1.w * q2.x) + (q2.w * q1.x) + (q1.y * q2.z) - (q2.y * q1.z);
	result.y = (q1.w * q2.y) + (q2.w * q1.y) + (q1.z * q2.x) - (q2.z * q1.x);
	result.z = (q1.w * q2.z) + (q2.w * q1.z) + (q1.x * q2.y) - (q2.x * q1.y);
	result.w = (q1.w * q2.w) - (q1.x * q2.x) - (q1.y * q2.y) - (q1.z * q2.z);

	return result;
}

inline Quat &operator*=(Quat &q1, const Quat &q2)
{
	q1 = q1 * q2;
	return q1;
}

inline Vec4 operator/(const Vec4 &v, const float factor)
{
	Vec4 result;
	result.x = v.x / factor;
	result.y = v.y / factor;
	result.z = v.z / factor;
	result.w = v.w / factor;
	return result;
}

inline Vec4 operator/(const float factor, const Vec4 &v)
{
	return v / factor;
}

inline Vec4 &operator/=(Vec4 &v, const float factor)
{
	v = v / factor;
	return v;
}

inline Quat operator/(const Quat &q1, const Quat &q2)
{
	// NOTE: (sonictk) Implementation from: https://www.mathworks.com/help/aerotbx/ug/quatdivide.html
	Quat result;

	float divisor = (q2.w * q2.w) + (q2.x * q2.x) + (q2.y * q2.y) + (q2.z * q2.z);

	result.x = ((q2.w * q1.x) - (q2.x * q1.w) - (q2.y * q1.z) + (q2.z * q1.y)) / divisor;
	result.y = ((q2.w * q1.y) - (q2.x * q1.z) - (q2.y * q1.w) + (q2.z * q1.x)) / divisor;
	result.z = ((q2.w * q1.z) - (q2.x * q1.y) - (q2.y * q1.x) + (q2.z * q1.w)) / divisor;
	result.w = ((q2.w * q1.w) - (q2.x * q1.x) - (q2.y * q1.y) + (q2.z * q1.z)) / divisor;

	return result;
}

inline Quat &operator/=(Quat &q1, const Quat &q2)
{
	q1 = q1 / q2;
	return q1;
}

inline Vec4 operator-(const Vec4 &v)
{
	Vec4 result;
	result.x = -v.x;
	result.y = -v.y;
	result.z = -v.z;
	result.w = -v.w;
	return result;
}

inline Vec4 operator+(const Vec4 &v1, const Vec4 &v2)
{
	Vec4 result;
	result.x = v1.x + v2.x;
	result.y = v1.y + v2.y;
	result.z = v1.z + v2.z;
	result.w = v1.w + v2.w;
	return result;
}

inline Vec4 &operator+=(Vec4 &v1, const Vec4 &v2)
{
	v1 = v1 + v2;
	return v1;
}

inline Vec4 operator-(const Vec4 &v1, const Vec4 &v2)
{
	Vec4 result;
	result.x = v1.x - v2.x;
	result.y = v1.y - v2.y;
	result.z = v1.z - v2.z;
	result.w = v1.w - v2.w;
	return result;
}

inline Vec4 &operator-=(Vec4 &v1, const Vec4 &v2)
{
	v1 = v1 - v2;
	return v1;
}

inline bool operator==(const Vec4 &v1, const Vec4 &v2)
{
	if (areFloatsEqual(v1.x, v2.x)
		&& areFloatsEqual(v1.y, v2.y)
		&& areFloatsEqual(v1.z, v2.z)
		&& areFloatsEqual(v1.w, v2.w)) {
		return true;
	} else {
		return false;
	}
}

inline bool operator!=(const Vec4 &v1, const Vec4 &v2)
{
	return !(v1 == v2);
}


inline float innerProduct(const Vec4 &v1, const Vec4 &v2)
{
	float result = (v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z) + (v1.w * v2.w);
	return result;
}


inline Vec4 lerp(const Vec4 &v1, float t, const Vec4 &v2)
{
	Vec4 result = ((1.0f - t) * v1) + (t * v2);
	return(result);
}


/**
 * Rotates the given vector by a specified amount.
 *
 * @param v			The vector to rotate.
 * @param rotation		The rotation to apply from a **normalized** quaternion.
 * 					The angle should be specified in **degrees**.
 *
 * @return				The rotated vector.
 */
inline Vec3 rotateBy(const Vec3 &v, const Quat &rotation)
{
	Vec3 axis = vec3(rotation.x, rotation.y, rotation.z);
	float scalar = rotation.w;

	// NOTE: (sonictk) Derived from Rodrigues' rotation formula:
	// (https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula)
	// Implementation from: https://gamedev.stackexchange.com/questions/28395/rotating-vector3-by-a-quaternion
	Vec3 result =
		(2.0f * innerProduct(axis, v) * axis) +
		(((scalar * scalar) - innerProduct(axis, axis)) * v) +
		(2.0f * scalar * crossProduct(axis, v));

	return result;
}

/**
 * Rotates the given vector by a specified amount.
 *
 * @param v			The vector to rotate.
 * @param axis			The axis to rotate about.
 * @param angle		The angle to rotate by, in **degrees**.
 *
 * @return				The rotated vector.
 */
inline Vec3 rotateBy(const Vec3 &v, const Vec3 &axis, float angle)
{
	// NOTE: (sonictk) Referenced from Jorge Rodriguez's video
	// regarding the subject: https://www.youtube.com/watch?v=dttFiVn0rvc
	// Implementation available at: https://github.com/BSVino/MathForGameDevelopers/blob/axis-angle/math/axisangle.h

	float a = angle * DEG_TO_RAD;

	Vec3 result =  (v * cosf(a)) +
		(innerProduct(v, axis) * axis * (1.0f - cosf(a))) +
		(crossProduct(axis, v) * sinf(a));

	return result;
}

#ifdef __cplusplus
/// @overload
template<typename T>
inline Vec3 rotateBy(const Vec3 &v, const Vec3 &axis, T angle)
{
	return rotateBy(v, axis, static_cast<float>(angle));
}
#endif // __cplusplus


/**
 * Returns the length/magnitude of the given quaternion.
 *
 * @param q 	The quaternion to find the length of.
 *
 * @return 	The length. Also known as the *norm*.
 */
inline float length(const Quat &q)
{
	return squareRoot((q.x * q.x) + (q.y * q.y) + (q.z * q.z) + (q.w * q.w));
}


/**
 * Returns a normalized version of the quaternion specified.
 *
 * @param q	The quaternion to normalize.
 *
 * @return		A normalized version of the quaternion (i.e. has unit length).
 */
inline Quat normalize(const Quat &q)
{
	Quat result;
	result /= length(q);

	return result;
}


/**
 * Returns the conjugate of the given quaterion.
 *
 * @param q 	The quaternion to get the conjugate of.
 *
 * @return 	The quaternion conjugate.
 */
inline Quat conjugate(const Quat &q)
{
	Quat result;
	result.x = q.x;
	result.y = q.y;
	result.z = q.z;
	result.w = -q.w;

	return result;
}


/**
 * Returns the inverse of the given quaternion.
 *
 * @param q	The quaternion to get the inverse of.
 *
 * @return		The inverse of the quaternion.
 */
inline Quat inverse(const Quat &q)
{
	float qLen = length(q);
	Quat result = conjugate(q) / (qLen * qLen);

	return result;
}


/**
 * Returns the quaternion that represents the rotation of the specified vector
 * into the other vector about their mutually perpendicular axis.
 *
 * @param v1		The first vector.
 * @param v2		The vector to rotate to.
 *
 * @return			The rotation that will transform the first vector to the second,
 * 				in axis-angle representation format. Rotation is expressed in **degrees**.
 */
inline Quat rotation(const Vec3 &v1, const Vec3 &v2)
{
	Quat result;

	float angle = angleBetween(v1, v2);
	Vec3 axis = crossProduct(v1, v2);

	result.x = axis.x;
	result.y = axis.y;
	result.z = axis.z;
	result.w = angle;

	return result;
}

/**
 * Creates a quaternion that represents the given axis-angle rotation.
 *
 * @param axis 	The axis of rotation. This must be a **normalized ** vector.
 * @param angle	The amount of rotation to be applied around the axis, in **degrees**.
 *
 * @return			The quaternion that represents the given axis-angle rotation.
 * 				This will be **normalized**.
 */
inline Quat rotation(const Vec3 &axis, float angle)
{
	Quat result;

	float angleRad = angle * DEG_TO_RAD;
	result.x = axis.x * sinf(angleRad / 2.0f);
	result.y = axis.y * sinf(angleRad / 2.0f);
	result.z = axis.z * sinf(angleRad / 2.0f);
	result.w = cosf(angleRad / 2.0f);

	return result;
}

/**
 * Creates a quaternion that represents the given Euler rotation.
 *
 * @param rotation 	The rotation.
 *
 * @return 			The Euler rotation as a **normalized** quaternion.
 */
Quat rotation(const EulerRotation &rotation);


/**
 * Finds the quaternion that describes the rotation which will convert ``q1`` to ``q2``.
 *
 * @param q1	The reference rotation.
 * @param q2	The rotation to end at.
 *
 * @return		The rotation that can be applied to ``q1`` to rotate it to ``q2``.
 */
inline Quat rotation(const Quat &q1, const Quat &q2)
{
	return q2 * inverse(q1);
}


/**
 * Linearly interpolate between the two quaternions.
 * NOTE: ``qlerp`` stands for quicklerp; this is imprecise and does not guarantee
 * result is ``v1`` when ``t`` is ``1``,
 * due to floating-point error.
 * May be used when the hardware has a FMA instruction.
 *
 * @param a 	The first quaternion.
 * @param b 	The second quaternion.
 * @param t 	The interpolation factor.
 *
 * @return 	The interpolated quaternion.
 */
inline Quat qlerp(const Quat &q1, float t, const Quat &q2)
{
	Quat result = q1 + (t * (q2 - q1));
	return result;
}

#ifdef __cplusplus

template<typename T>
inline Quat qlerp(const Quat &q1, T t, const Quat &q2)
{
	return qlerp(q1, static_cast<float>(t), q2);
}

#endif // __cplusplus


/**
 * Normalized linear interpolation between two quaternions.
 * NOTE: As a reminder, nlerp is commutative, does *not* have constant velocity,
 * and is torque-minimal.
 *
 * @param q1		The first quaternion to blend.
 * @param t		The amount to blend.
 * @param q2		The second quaternion to blend.
 *
 * @return			The blended quaternion.
 */
inline Quat nlerp(const Quat &q1, float t, const Quat &q2)
{
	Quat result;
	result = qlerp(q1, t, q2);
	result = normalize(result);

	return result;
}

#ifdef __cplusplus

template<typename T>
inline Quat nlerp(const Quat &q1, T t, const Quat &q2)
{
	return nlerp(q1, static_cast<float>(t), q2);
}

#endif // __cplusplus


/**
 * Spherical linear interpolation between two quaternions.
 * NOTE: As a reminder, slerp is *not* commutative, has constant velocity,
 * and is torque-minimal.
 *
 * @param q1		The first quaternion to blend.
 * @param t 		The amount to blend.
 * @param q2		The second quaternion to blend.
 *
 * @return			The blended quaternion.
 */
Quat slerp(const Quat &q1, float t, const Quat &q2);


/**
 * Converts the given Euler rotation to an axis-angle representation.
 *
 * @param rotation	The Euler rotation. Angles should be in **degrees**.
 * @param axis		The storage for the **unnormalized** computed axis of rotation.
 * @param angle	The storage for the compute angle, in **degrees**.
 */
void axisAngle(const EulerRotation &rotation, Vec3 &axis, float &angle);

/**
 * Converts the given quaternion rotation to an axis-angle representation.
 *
 * @param rotation	The **normalized** quaternion rotation.
 * @param axis		The storage for the **unnormalized** computed axis of rotation.
 * @param angle	The storage for the compute angle, in **degrees**.
 */
void axisAngle(const Quat &q, Vec3 &axis, float &angle);


/**
 * Converts the given quaternion to an Euler rotation.
 *
 * @param q		The **normalized** quaternion.
 *
 * @return			The Euler rotation that represents this quaternion rotation.
 * 				Angles are specified in **degrees**. Rotation order is determined
 * 				as ``YZX``.
 */
EulerRotation eulerRotation(const Quat &q);

/**
 * Converts the given axis-angle rotation to an Euler rotation.
 *
 * @param axis		The **normalized** axis of rotation.
 * @param angle	The amount of rotation to apply in **degrees**.
 *
 * @return			The Euler rotation that represents this axis-angle rotation.
 * 				Angles are specified in **degrees**. Rotation order is determined
 * 				as ``YZX``.
 */
EulerRotation eulerRotation(const Vec3 &axis, float angle);


// TODO: (sonictk) Add functionality for converting between euler angle rotation orders

// TODO: (sonictk) Implement parity with Maya MQuaternion API and this


#endif /* SS_VECTOR_MATH_H */
