#include "ss_library.h"


inline SS_DLLHandle loadSharedLibrary(const char *filename)
{
	SS_DLLHandle libHandle = LoadLibrary((LPCTSTR)filename);
	if (!libHandle) {
		SS_OSPrintLastError();
		return NULL;
	}

	return libHandle;
}


inline int unloadSharedLibrary(SS_DLLHandle handle)
{
	if (!handle) {
		perror("The handle is not valid! Cannot unload!\n");
		return -1;
	}

	BOOL result = FreeLibrary(handle);
	if (result == 0) {
		SS_OSPrintLastError();
		return -2;
	}

	return 0;
}


inline SS_FuncPtr loadSymbolFromLibrary(SS_DLLHandle handle, const char *symbol)
{
	SS_FuncPtr symbolAddr = GetProcAddress(handle, (LPCSTR)symbol);
	if (!symbolAddr) {
		SS_OSPrintLastError();
		return NULL;
	}

	return symbolAddr;
}
