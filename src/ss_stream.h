/**
 * @file   ss_stream.h
 * @brief  Stream related functions.
 */
#ifndef SS_STREAM_H
#define SS_STREAM_H


#ifdef _WIN32
#include "ss_stream_win32.cpp"

#elif __linux__ || __APPLE__
#include "ss_stream_linux.cpp"

#else
#error "Unsupported OS platform for compilation."
#endif // Platform Layer


/**
 * This function prints the last error message that was set from an OS call.
 */
void SS_OSPrintLastError();



#endif /* SS_STREAM_H */
