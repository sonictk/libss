#include "ss_library.h"

#include <dlfcn.h>

inline SS_DLLHandle loadSharedLibrary(const char *filename, int flags)
{
	SS_DLLHandle libHandle = dlopen(filename, flags);
	if (!libHandle) {
		char *errMsg = dlerror();
		fprintf(stderr, "Failed to load library %s: %s!\n", filename, errMsg);
		return NULL;
	}

	return libHandle;
}

inline SS_DLLHandle loadSharedLibrary(const char *filename)
{
	SS_DLLHandle libHandle = loadSharedLibrary(filename, RTLD_LAZY);
	return libHandle;
}


inline int unloadSharedLibrary(SS_DLLHandle handle)
{
	if (!handle) {
		perror("The handle is not valid! Cannot unload!\n");
		return -1;
	}

	int result = dlclose(handle);
	if (result != 0) {
		char *errMsg = dlerror();
		fprintf(stderr, "Could not free library! %s\n", errMsg);
	}

	return 0;
}


inline SS_FuncPtr loadSymbolFromLibrary(SS_DLLHandle handle, const char *symbol)
{
	if (!handle) {
		perror("The given handle was not valid!\n");
		return NULL;
	}

	void *symbolAddr = dlsym(handle, symbol);
	if (symbolAddr == NULL) {
		// NOTE: (sonictk) Following recommendation of Linux manual; clear the
		// old error code, call ``dlsym()`` again, and then call ``dlerror()``
		// again since the symbol returned could actually be ``NULL``.
		dlerror();
		dlsym(handle, symbol);
		char *errMsg = dlerror();
		if (!errMsg) {
			return symbolAddr;
		}
		fprintf(stderr, "Unable to find symbol: %s! %s\n", symbol, errMsg);

		return NULL;
	}

	return symbolAddr;
}
