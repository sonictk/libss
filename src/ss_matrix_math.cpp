#include "ss_matrix_math.h"
#include "ss_common_math.h"


#ifdef __cplusplus

#include <cstdlib>
#include <cmath>
#include <cfloat>

#else

#include <stdlib.h>
#include <math.h>
#include <float.h>

#endif // __cplusplus


Mat33 rotationMatrix(const Vec3 &axis, float angle)
{
	// NOTE: (sonictk) Implementation from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToMatrix/index.htm

	float angleRad = angle * DEG_TO_RAD;

	float c = cosf(angleRad);
	float s = sinf(angleRad);
	float t = 1.0f - c;

	float x = axis.x;
	float y = axis.y;
	float z = axis.z;

	Mat33 result = {{{(t * x * x) + c, (t * x * y) - (z * s), (t * x * z) + (y * s)},
	                 {(t * x * y) + (z * s), (t * y * y) + c, (t * y * z) - (x * s)},
	                 {(t * x * z) - (y * s), (t * y * z) + (x * s), (t * z * z) + c}}};

	return result;
}

Mat33 rotationMatrix(const Quat &q)
{
	// NOTE: (sonictk) Implementation from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToMatrix/index.htm
	Mat33 result;

	result[0][0] = 1.0f - (2.0f * powf(q.y, 2.0f)) - (2.0f * powf(q.z, 2.0f));
	result[0][1] = (2.0f * q.x * q.y) - (2.0f * q.z * q.w);
	result[0][2] = (2.0f * q.x * q.z) + (2.0f * q.y * q.w);

	result[1][0] = (2.0f * q.x * q.y) + (2.0f * q.z * q.w);
	result[1][1] = 1.0f - (2.0f * powf(q.x, 2.0f)) - (2.0f * powf(q.z, 2.0f));
	result[1][2] = (2.0f * q.y * q.z) - (2.0f * q.x * q.w);

	result[2][0] = (2.0f * q.x * q.z) - (2.0f * q.y * q.w);
	result[2][1] = (2.0f * q.y * q.z) + (2.0f * q.x * q.w);
	result[2][2] = 1.0f - (2.0f * powf(q.x, 2.0f) - (2.0f * powf(q.y, 2.0f)));

	return result;
}

Mat33 rotationMatrix(const EulerRotation &rotation)
{
	// NOTE: (sonictk) Implementation from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToMatrix/index.htm

	float heading, attitude, bank;
	switch (rotation.order) {
	case RotationOrder::kXZY:
		heading = rotation.angles.x;
		attitude = rotation.angles.z;
		bank = rotation.angles.y;
		break;
	case RotationOrder::kYXZ:
		heading = rotation.angles.y;
		attitude = rotation.angles.x;
		bank = rotation.angles.z;
		break;
	case RotationOrder::kYZX:
		heading = rotation.angles.y;
		attitude = rotation.angles.z;
		bank = rotation.angles.x;
		break;
	case RotationOrder::kZXY:
		heading = rotation.angles.z;
		attitude = rotation.angles.x;
		bank = rotation.angles.y;
		break;
	case RotationOrder::kZYX:
		heading = rotation.angles.z;
		attitude = rotation.angles.y;
		bank = rotation.angles.x;
		break;
	case RotationOrder::kXYZ: // NOTE: (sonictk) XYZ is most likely, so we fallback to that
	default:
		heading = rotation.angles.x;
		attitude = rotation.angles.y;
		bank = rotation.angles.z;
		break;
	}

	heading *= DEG_TO_RAD;
	attitude *= DEG_TO_RAD;
	bank *= DEG_TO_RAD;

	float sa = sinf(attitude);
	float ca = cosf(attitude);
	float sb = sinf(bank);
	float cb = cosf(bank);
	float sh = sinf(heading);
	float ch = cosf(heading);

	Mat33 result;
	result[0][0] = ch * ca;
	result[0][1] = (-ch * sa * cb) + (sh * sb);
	result[0][2] = (ch * sa * sb) + (sh * cb);
	result[1][0] = sa;
	result[1][1] = ca * cb;
	result[1][2] = -ca * sb;
	result[2][0] = -sh * ca;
	result[2][1] = (sh * sa * cb) + (ch * sb);
	result[2][2] = (-sh * sa * sb) + (ch * cb);

	return result;
}


Mat44 rotationMatrixMat44(const Vec3 &axis, float angle)
{
	Mat33 rot33 = rotationMatrix(axis, angle);
	Mat44 result = {{{rot33[0][0], rot33[0][1], rot33[0][2], 0.0f},
	                 {rot33[1][0], rot33[1][1], rot33[1][2], 0.0f},
	                 {rot33[2][0], rot33[2][1], rot33[2][2], 0.0f},
	                 {0.0f, 0.0f, 0.0f, 1.0f}}};

	return result;
}

Mat44 rotationMatrixMat44(const Quat &q)
{
	Mat33 rot33 = rotationMatrix(q);
	Mat44 result = {{{rot33[0][0], rot33[0][1], rot33[0][2], 0.0f},
	                 {rot33[1][0], rot33[1][1], rot33[1][2], 0.0f},
	                 {rot33[2][0], rot33[2][1], rot33[2][2], 0.0f},
	                 {0.0f, 0.0f, 0.0f, 1.0f}}};

	return result;
}

Mat44 rotationMatrixMat44(const EulerRotation &rotation)
{
	Mat33 rot33 = rotationMatrix(rotation);
	Mat44 result = {{{rot33[0][0], rot33[0][1], rot33[0][2], 0.0f},
	                 {rot33[1][0], rot33[1][1], rot33[1][2], 0.0f},
	                 {rot33[2][0], rot33[2][1], rot33[2][2], 0.0f},
	                 {0.0f, 0.0f, 0.0f, 1.0f}}};

	return result;
}

Mat44 rotationMatrixMat44(const float rotation[3], const RotationOrder order)
{
	EulerRotation euler;
	euler.angles = vec3(rotation);
	euler.order = order;

	return rotationMatrixMat44(euler);
}


void rotateBy(Mat44 &mat, const Vec3 &axis, const float angle)
{
	// NOTE: (sonictk) Implementation from:
	// http://www.opengl-tutorial.org/assets/faq_quaternions/index.html#Q38
	// Basically it's rotation of matrix by a quaternion
	Mat44 rotMatrix;

	// TODO: (sonictk) If the direction of rotation isn't negated here, it seems to
	// work for left-handed coordinate systems (rather than right-handed, as is OGL standard)
	float angleRad = -angle * DEG_TO_RAD;
	float rcos = cosf(angleRad);
	float rsin = sinf(angleRad);

	float u = axis.x;
	float v = axis.y;
	float w = axis.z;

	float oneMinusCosine = 1.0f - rcos;

	// NOTE: (sonictk) This is the upper 3x3 part of the rot matrix
	rotMatrix[0][0] =       rcos +   (u*u*(oneMinusCosine));
	rotMatrix[1][0] = w *   rsin +   (v*u*(oneMinusCosine));
	rotMatrix[2][0] = -v *  rsin +   (w*u*(oneMinusCosine));
	rotMatrix[0][1] = -w *  rsin +   (u*v*(oneMinusCosine));
	rotMatrix[1][1] =       rcos +   (v*v*(oneMinusCosine));
	rotMatrix[2][1] = u *   rsin +   (w*v*(oneMinusCosine));
	rotMatrix[0][2] = v *   rsin +   (u*w*(oneMinusCosine));
	rotMatrix[1][2] = -u *  rsin +   (v*w*(oneMinusCosine));
	rotMatrix[2][2] =       rcos +   (w*w*(oneMinusCosine));

	// NOTE: (sonictk) Set the rest of the rot matrix to identity
	rotMatrix[0][3] =
		rotMatrix[1][3] =
		rotMatrix[2][3] =
		rotMatrix[3][0] =
		rotMatrix[3][1] =
		rotMatrix[3][2] = 0;

	rotMatrix[3][3] = 1.0f;

	mat *= rotMatrix;
}

void rotateBy(Mat44 &mat, const Quat &rotation)
{
	Mat44 rotMat = rotationMatrixMat44(rotation);
	mat *= rotMat;
}

void rotateBy(Mat44 &mat, const EulerRotation &rotation)
{
	// NOTE: (sonictk) Implementation from:
	// http://www.opengl-tutorial.org/assets/faq_quaternions/index.html#Q36
	Mat44 rotMat;

	float heading, attitude, bank;

	// NOTE: (sonictk) Basically, the rotation matrix is generated from ``X.Y.Z`` and
	// then simplified to get the optimized version seen below.
	switch (rotation.order) {
	case RotationOrder::kXZY:
		heading = rotation.angles.x;
		attitude = rotation.angles.z;
		bank = rotation.angles.y;
		break;
	case RotationOrder::kYXZ:
		heading = rotation.angles.y;
		attitude = rotation.angles.x;
		bank = rotation.angles.z;
		break;
	case RotationOrder::kYZX:
		heading = rotation.angles.y;
		attitude = rotation.angles.z;
		bank = rotation.angles.x;
		break;
	case RotationOrder::kZXY:
		heading = rotation.angles.z;
		attitude = rotation.angles.x;
		bank = rotation.angles.y;
		break;
	case RotationOrder::kZYX:
		heading = rotation.angles.z;
		attitude = rotation.angles.y;
		bank = rotation.angles.x;
		break;
	case RotationOrder::kXYZ: // NOTE: (sonictk) XYZ is most likely, so we fallback to that
	default:
		heading = rotation.angles.x;
		attitude = rotation.angles.y;
		bank = rotation.angles.z;
		break;
	}

	heading *= DEG_TO_RAD;
	attitude *= DEG_TO_RAD;
	bank *= DEG_TO_RAD;

	float a = cosf(heading);
	float b = sinf(heading);
	float c = cosf(attitude);
	float d = sinf(attitude);
	float e = cosf(bank);
	float f = sinf(bank);

	float ad = a * d;
	float bd = b * d;

	// NOTE: (sonictk) Set the rotation (upper 3x3) part of the matrix
	rotMat[0][0] = c * e;
	rotMat[0][1] = -c * f;
	rotMat[0][2] = d;
	rotMat[1][0] = (bd * e) + (a * f);
	rotMat[1][1] = (-bd * f) + (a * e);
	rotMat[1][2] = -b * c;
	rotMat[2][0] = (-ad * e) + (b * f);
	rotMat[2][1] = (ad * f) + (b * e);
	rotMat[2][2] = a * c;

	// NOTE: (sonictk) "identity out" the rest of the matrix
	rotMat[0][3] = rotMat[1][3] = rotMat[2][3] = rotMat[3][0] = rotMat[3][1] = rotMat[3][2] = 0;
	rotMat[3][3] = 1.0f;

	mat *= rotMat;
}

void rotateBy(Mat44 &mat, const float rotation[3], const RotationOrder order)
{
	EulerRotation euler;
	euler.angles = vec3(rotation);
	euler.order = order;

	rotateBy(mat, euler);
}


Mat44 getRotation(const Mat44 &mat)
{
	Mat44 result = mat44(mat);
	clearTranslation(result);
	neutralizeScale(result);

	return result;

}

void getRotation(const Mat44 &mat, float rotation[3], const RotationOrder order)
{
	// NOTE: (sonictk) Implementation reference: https://pdfs.semanticscholar.org/6681/37fa4b875d890f446e689eea1e334bcf6bf6.pdf

	Mat44 matCopy = mat44(mat);

	// NOTE: (sonictk) Remove scaling by normalizing the basis vectors.
	neutralizeScale(matCopy);
	Mat44 tmpMat = mat44();
	float cosy;

	// TODO: (sonictk) Figure out what to do for other rotation orders
	switch (order) {
	case RotationOrder::kXYZ:
		// NOTE: (sonictk) Extract the X rotation.
		rotation[0] = atan2f(matCopy[1][2], matCopy[2][2]);

		// NOTE: (sonictk) We remove the X rotation so that the remaining rotation
		// is only around 2 axes. This prevents gimbal lock.
		rotateBy(tmpMat, vec3(-rotation[0], 0, 0), RotationOrder::kXYZ);
		tmpMat *= matCopy;

		// NOTE: (sonictk) Now extract the other two angles.
		cosy = squareRoot(tmpMat[0][0] * tmpMat[0][0] + tmpMat[0][1] * tmpMat[0][1]);
		rotation[1] = atan2f(-tmpMat[0][2], cosy);
		rotation[2] = atan2f(-tmpMat[1][0], tmpMat[1][1]);

		break;

	case RotationOrder::kZYX:
	default:
		rotation[0] = -atan2f(matCopy[1][2], matCopy[2][2]);

		// NOTE: (sonictk) We remove the X rotation so that the remaining rotation
		// is only around 2 axes. This prevents gimbal lock.
		rotateBy(tmpMat, vec3(0, 0, -rotation[0]), RotationOrder::kZYX);
		tmpMat *= matCopy;

		// NOTE: (sonictk) Now extract the other two angles.
		cosy = squareRoot(tmpMat[2][2] * tmpMat[2][2] + tmpMat[2][1] * tmpMat[2][1]);
		rotation[1] = -atan2f(-tmpMat[2][0], cosy);
		rotation[2] = -atan2f(-tmpMat[1][2], tmpMat[1][1]);

		break;
	}
}

void getRotation(const Mat44 &mat, Quaternion &q)
{
	// NOTE: (sonictk) Reference implementation: https://d3cw3dd2w32x2b.cloudfront.net/wp-content/uploads/2015/01/matrix-to-quat.pdf
	float trace;
	if (mat[2][2] < 0) {
		if (mat[0][0] > mat[1][1]) {
			trace = 1.0f + mat[0][0] - mat[1][1] - mat[2][2];
			q = vec4(trace,
					 mat[0][1] + mat[1][0],
					 mat[2][0] + mat[0][2],
					 mat[1][2] - mat[2][1]);
		} else {
			trace = 1.0f - mat[0][0] + mat[1][1] - mat[2][2];
			q = vec4(mat[0][1] + mat[1][0],
					 trace,
					 mat[1][2] + mat[2][1],
					 mat[2][0] - mat[0][2]);
		}
	} else {
		if (mat[0][0] < -mat[1][1]) {
			trace = 1.0f - mat[0][0] - mat[1][1] + mat[2][2];
			q = vec4(mat[2][0] + mat[0][2],
					 mat[1][2] + mat[2][1],
					 trace,
					 mat[0][1] - mat[1][0]);
		} else {
			trace = 1.0f + mat[0][0] + mat[1][1] + mat[2][2];
			q = vec4(mat[1][2] - mat[2][1],
					 mat[2][0] - mat[0][2],
					 mat[0][1] - mat[1][0],
					 trace);
		}
	}

	q *= 0.5f / squareRoot(trace);
}

Quat getRotationFromTransformation(const Mat44 &mat)
{
	Mat44 rotMat = mat44(mat);

	clearTranslation(rotMat);
	neutralizeScale(rotMat);

	Quaternion q;
	getRotation(rotMat, q);

	return q;
}


bool setRotation(Mat44 &mat, const float rotation[3], const RotationOrder order)
{
	Vec3 shear;
	Vec3 scale;
	bool status = extractAndRemoveScaleAndShear(mat, scale, shear);
	if (!status) {
		return status;
	}

	Vec3 translation = getTranslation(mat);

	Mat44 rot = rotationMatrixMat44(rotation, order);

	Mat44 result = mat44();

	scaleBy(result, scale);
	rotateBy(result, rotation, order);
	translateBy(result, translation);

	mat = result;

	return true;
}


bool getScaleFromRow(const float &scale, const Vec3 &row)
{
	for (int c=0; c < 3; ++c) {
		if ((fabsf(scale) < 1) && (fabsf(row[c]) >= FLT_MAX * fabsf(scale))) {
			return false;
		}
	}

	return true;
}


bool extractAndRemoveScaleAndShear(Mat44 &mat, Vec3 &scale, Vec3 &shear)
{
	// NOTE: (sonictk) Reference implementation from ILM Imath library, which is
	// based off the method described by Spencer W. Thomas in Graphics Gems II:
	// "Decomposing a Matrix into Simple Transformations", p. 320.

	Vec3 rows[3];

	rows[0] = vec3(mat[0][0], mat[0][1], mat[0][2]);
	rows[1] = vec3(mat[1][0], mat[1][1], mat[1][2]);
	rows[2] = vec3(mat[2][0], mat[2][1], mat[2][2]);

	float maxVal = 0.0f;
	for (int r=0; r < 3; ++r) {
		for (int c=0; c < 3; ++c) {
			if (fabsf(rows[r][c]) > maxVal) {
				maxVal = fabsf(rows[r][c]);
			}
		}
	}

	// Normailze the 3x3 matrix here.
	// It was noticed that this can improve numerical stability significantly,
	// especially when many of the upper 3x3 matrix's coefficients are very
	// close to zero; we correct for this step at the end by multiplying the
	// scaling factors by maxVal at the end (shear and rotation are not
	// affected by the normalization).
	if (!areFloatsEqual(maxVal, 0.0f)) {
		for (int r=0; r < 3; ++r) {
			if (!getScaleFromRow(maxVal, rows[r])) {
				return false;
			} else {
				rows[r] /= maxVal;
			}
		}
	}

	// Compute X scale.
	scale.x = length(rows[0]);
	if (!getScaleFromRow(scale.x, rows[0])) {
		return false;
	}

	// Normalize the first row.
	rows[0] /= scale.x;

	// An XY shear factor will shear the X coord. as the Y coord. changes.
	// There are 6 combinations (XY, XZ, YZ, YX, ZX, ZY), although we only
	// extract the first 3 because we can effect the last 3 by shearing in
	// XY, XZ, YZ combined rotations and scales.
	//
	// shear matrix <	1,	YX,	 ZX,  0,
	//				   XY,	 1,	 ZY,  0,
	//				   XZ,	YZ,	  1,  0,
	//					0,	 0,	  0,  1 >

	// Compute XY shear factor and make 2nd row orthogonal to 1st.
	shear[0] = innerProduct(rows[0], rows[1]);
	rows[1] -= shear[0] * rows[0];

	// Now, compute Y scale.
	scale.y = length(rows[1]);
	if (!getScaleFromRow(scale.y, rows[1])) {
		return false;
	}

	// Normalize the 2nd row and correct the XY shear factor for Y scale.
	rows[1] /= scale.y;
	shear[0] /= scale.y;

	// Compute XZ and YZ shear factors; orthogonalize the 3rd row.
	shear[1] = innerProduct(rows[0], rows[2]);
	rows[2] -= shear[1] * rows[0];
	shear[2] = innerProduct(rows[1], rows[2]);

	// Next, get Z scale.
	scale.z = length(rows[2]);
	if (!getScaleFromRow(scale.z, rows[2])) {
		return false;
	}

	// Normalize 3rd row and correct XZ and YZ shear factors for Z scale.
	rows[2] /= scale.z;
	shear[1] /= scale.z;
	shear[2] /= scale.z;

	// At this point, the upper 3x3 matrix is orthonormal.
	// Check for a coord. system flip. If ther determinant is less than zero,
	// negate the matrix and scaling factors.
	if (innerProduct(rows[0], crossProduct(rows[1], rows[2])) < 0.0f) {
		for (int r=0; r < 3; ++r) {
			scale[r] *= -1;
			rows[r] *= -1;
		}
	}

	// Copy over the orthonormal rows into the returned matrix.
	// The upper 3x3 matrix is now a rotation matrix.
	for (int r=0; r < 3; ++r) {
		mat[r][0] = rows[r][0];
		mat[r][1] = rows[r][1];
		mat[r][2] = rows[r][2];
	}

	// Correct the scaling factors for the normalization step that we performed;
	// shear and rotation are not affected by the normalization.
	scale *= maxVal;

	return true;
}


float determinant(const Mat44 &mat)
{
	/* NOTE: (sonictk) After expansion, the formula for a determinant of a 4x4
	   matrix is:
	   ```
	   + a.[f.(k.p - l.o) - g.(j.p - l.n) + h.(j.o - k.n)]
	   - b.[e.(k.p - l.o) - g.(i.p - l.m) + h.(i.o - k.m)]
	   + c.[e.(j.p - l.n) - f.(i.p - l.m) + h.(i.n - j.m)]
	   - d.[e.(j.o - k.n) - f.(i.o - k.m) + g.(i.n - j.m)]
	   ```
	   where the 4x4 matrix is composed as such:
	       0 1 2 3
	   0  |a b c d|
	   1  |e f g h|
	   2  |i k j l|
	   3  |m n o p|
	*/
	return
		+ (mat(0,0) * ((mat(1,1) * ((mat(2,1) * mat(3,3)) - (mat(2,3) * mat(3,2))))
					   - (mat(1,2) * (mat(2,2) * mat(3,3) - mat(2,3) * mat(3,1)))
					   + (mat(1,3) * (mat(2,2) * mat(3,2) - mat(2,1) * mat(3,1)))))
		- (mat(0,1) * ((mat(1,0) * ((mat(2,1) * mat(3,3)) - (mat(2,3) * mat(3,2))))
					   - (mat(1,2) * (mat(2,0) * mat(3,3) - mat(2,3) * mat(3,0)))
					   + (mat(1,3) * (mat(2,0) * mat(3,2) - mat(2,1) * mat(3,0)))))
		+ (mat(0,2) * ((mat(1,0) * ((mat(2,2) * mat(3,3)) - (mat(2,3) * mat(3,1))))
					   - (mat(1,1) * (mat(2,0) * mat(3,3) - mat(2,3) * mat(3,0)))
					   + (mat(1,3) * (mat(2,0) * mat(3,1) - mat(2,2) * mat(3,0)))))
		- (mat(0,3) * ((mat(1,0) * ((mat(2,2) * mat(3,2)) - (mat(2,1) * mat(3,1))))
					   - (mat(1,1) * (mat(2,0) * mat(3,2) - mat(2,1) * mat(3,0)))
					   + (mat(1,2) * (mat(2,0) * mat(3,1) - mat(2,2) * mat(3,0)))));
}


// * Referenced from **Paul Bourke's** implementation available at:
// * ``http://paulbourke.net/miscellaneous/determinant/determinant.c``
float determinant(float **mat, unsigned int dimension)
{
	unsigned int i, j, j1, j2 ;				// general loop and matrix subscripts
	float det = 0;
	float **m = NULL ;				// pointer to pointers to implement 2d square array

	switch (dimension) {
	case 0:
		return 0;
		break;
	case 1:
		return mat[0][0];
		break;
	case 2:	// the recursion series
		det = mat[0][0] * mat[1][1] - mat[1][0] * mat[0][1];
		break;
	default:
		// for each column in sub-matrix
		for (j1 = 0 ; j1 < dimension ; j1++) {
			// get space for the pointer list
			m = (float **)malloc((dimension - 1) * sizeof(float *));
			for (i = 0 ; i < dimension-1 ; i++)
				m[i] = (float *)malloc((dimension - 1) * sizeof(float));
			//	  i[0][1][2][3]	 first malloc
			//  m -> +  +  +	 +	 space for 4 pointers
			//		|  |  |	 |			j  second malloc
			//		|  |  |	 +-> _ _ _ [0] pointers to
			//		|  |  +----> _ _ _ [1] and memory for
			//		|  +-------> _ a _ [2] 4 floats
			//		+----------> _ _ _ [3]
			//
			//					a[1][2]
			// build sub-matrix with minor elements excluded
			for (i=1; i < dimension; ++i) {
				j2 = 0;			   // start at first sum-matrix column position
									   // loop to copy source matrix less one column
				for (j=0; j < dimension; ++j) {
					if (j == j1) continue; // don't copy the minor column element

					m[i - 1][j2] = mat[i][j];	// copy source element into new sub-matrix
					// i-1 because new sub-matrix is one row
					// (and column) smaller with excluded minors
					j2++;	// move to next sub-matrix column position
				}
			}

			// sum x raised to y power recursively get determinant of
			// next sub-matrix which is now one row & column smaller
			det += powf(-1.0f, 1.0f + j1 + 1.0f) * mat[0][j1] * determinant(m, dimension - 1);

			for (i = 0 ; i < dimension-1 ; i++) {
				// free the storage allocated to to this minor's set of pointers
				free(m[i]);
			}
			// free the storage for the original pointer to pointer
			free(m);
		}
		break;
	}

	return det;
}

Mat44 cofactor(const Mat44 &mat)
{
	Mat44 result;

	/* NOTE: (sonictk) Results were determined after expansion of the formula
	   for the cofactor of a 4x4
	   matrix where the 4x4 matrix is composed as such:

	       0 1 2 3
	   0  |a b c d|
	   1  |e f g h|
	   2  |i k j l|
	   3  |m n o p|
	*/
	float a = mat(0, 0);
	float b = mat(0, 1);
	float c = mat(0, 2);
	float d = mat(0, 3);

	float e = mat(1, 0);
	float f = mat(1, 1);
	float g = mat(1, 2);
	float h = mat(1, 3);

	float i = mat(2, 0);
	float j = mat(2, 1);
	float k = mat(2, 2);
	float l = mat(2, 3);

	float m = mat(3, 0);
	float n = mat(3, 1);
	float o = mat(3, 2);
	float p = mat(3, 3);

	result[0][0] =
		+ (f * ((k * p) - (l * o)))
		- (g * ((j * p) - (l * n)))
		+ (h * ((j * o) - (k * n)));
	result[0][1] =
		- (e * ((k * p) - (l * o)))
		+ (g * ((i * p) - (l * m)))
		- (h * ((i * o) - (k * m)));
	result[0][2] =
		+ (e * ((j * p) - (l * n)))
		- (f * ((i * p) - (l * m)))
		+ (h * ((i * n) - (j * m)));
	result[0][3] =
		- (e * ((j * o) - (k * n)))
		+ (f * ((i * o) - (k * m)))
		- (g * ((i * n) - (j * m)));

	result[1][0] =
		- (b * ((k * p) - (l * o)))
		+ (c * ((j * p) - (l * n)))
		- (d * ((j * o) - (k * n)));
	result[1][1] =
		+ (a * ((k * p) - (l * o)))
		- (c * ((i * p) - (l * m)))
		+ (d * ((i * o) - (k * m)));
	result[1][2] =
		- (a * ((j * p) - (l * n)))
		+ (b * ((i * p) - (l * m)))
		- (d * ((i * n) - (j * m)));
	result[1][3] =
		- (a * ((j * o) - (k * n)))
		+ (b * ((i * o) - (k * m)))
		- (c * ((i * n) - (j * m)));

	result[2][0] =
		+ (b * ((g * p) - (h * o)))
		- (c * ((f * p) - (h * n)))
		+ (d * ((f * o) - (g * n)));
	result[2][1] =
		- (a * ((g * p) - (h * o)))
		+ (c * ((e * p) - (h * m)))
		- (d * ((e * o) - (g * m)));
	result[2][2] =
		+ (a * ((f * p) - (h * n)))
		- (b * ((e * p) - (h * m)))
		+ (d * ((e * n) - (f * m)));
	result[2][3] =
		- (a * ((f * o) - (g * n)))
		+ (b * ((e * o) - (g * m)))
		- (c * ((e * n) - (f * m)));

	result[3][0] =
		- (b * ((g * l) - (h * k)))
		+ (c * ((f * l) - (h * j)))
		- (d * ((f * k) - (g * j)));
	result[3][1] =
		+ (a * ((g * l) - (h * k)))
		- (c * ((e * l) - (h * i)))
		+ (d * ((e * k) - (g * i)));
	result[3][2] =
		- (a * ((f * l) - (h * j)))
		+ (b * ((e * l) - (h * i)))
		- (d * ((e * j) - (f * i)));
	result[3][3] =
		- (a * ((f * k) - (g * j)))
		+ (b * ((e * k) - (g * i)))
		- (c * ((e * j) - (f * i)));

	return result;
}


Mat44 adjugate(const Mat44 &mat)
{
	return transpose(cofactor(mat));
}


int inverse(const Mat44 &inMat, Mat44 &outMat)
{
	// NOTE: (sonictk) This code is from the MESA implementation of the GLU library.
	// It (probably) expands from ``A^-1 = 1 / determinant(A) * adjugate(A)``.
	Mat44 invMat;
	invMat[0][0] =
		+ inMat[1][1] * inMat[2][2] * inMat[3][3]
		- inMat[1][1] * inMat[2][3] * inMat[3][2]
		- inMat[2][1] * inMat[1][2] * inMat[3][3]
		+ inMat[2][1] * inMat[1][3] * inMat[3][2]
		+ inMat[3][1] * inMat[1][2] * inMat[2][3]
		- inMat[3][1] * inMat[1][3] * inMat[2][2];

	invMat[1][0] =
		- inMat[1][0] * inMat[2][2] * inMat[3][3]
		+ inMat[1][0] * inMat[2][3] * inMat[3][2]
		+ inMat[2][0] * inMat[1][2] * inMat[3][3]
		- inMat[2][0] * inMat[1][3] * inMat[3][2]
		- inMat[3][0] * inMat[1][2] * inMat[2][3]
		+ inMat[3][0] * inMat[1][3] * inMat[2][2];

	invMat[2][0] =
		+ inMat[1][0] * inMat[2][1] * inMat[3][3]
		- inMat[1][0] * inMat[2][3] * inMat[3][1]
		- inMat[2][0] * inMat[1][1] * inMat[3][3]
		+ inMat[2][0] * inMat[1][3] * inMat[3][1]
		+ inMat[3][0] * inMat[1][1] * inMat[2][3]
		- inMat[3][0] * inMat[1][3] * inMat[2][1];

	invMat[3][0] =
		- inMat[1][0] * inMat[2][1] * inMat[3][2]
		+ inMat[1][0] * inMat[2][2] * inMat[3][1]
		+ inMat[2][0] * inMat[1][1] * inMat[3][2]
		- inMat[2][0] * inMat[1][2] * inMat[3][1]
		- inMat[3][0] * inMat[1][1] * inMat[2][2]
		+ inMat[3][0] * inMat[1][2] * inMat[2][1];

	invMat[0][1] =
		- inMat[0][1] * inMat[2][2] * inMat[3][3]
		+ inMat[0][1] * inMat[2][3] * inMat[3][2]
		+ inMat[2][1] * inMat[0][2] * inMat[3][3]
		- inMat[2][1] * inMat[0][3] * inMat[3][2]
		- inMat[3][1] * inMat[0][2] * inMat[2][3]
		+ inMat[3][1] * inMat[0][3] * inMat[2][2];

	invMat[1][1] =
		+ inMat[0][0] * inMat[2][2] * inMat[3][3]
		- inMat[0][0] * inMat[2][3] * inMat[3][2]
		- inMat[2][0] * inMat[0][2] * inMat[3][3]
		+ inMat[2][0] * inMat[0][3] * inMat[3][2]
		+ inMat[3][0] * inMat[0][2] * inMat[2][3]
		- inMat[3][0] * inMat[0][3] * inMat[2][2];

	invMat[2][1] =
		- inMat[0][0] * inMat[2][1] * inMat[3][3]
		+ inMat[0][0] * inMat[2][3] * inMat[3][1]
		+ inMat[2][0] * inMat[0][1] * inMat[3][3]
		- inMat[2][0] * inMat[0][3] * inMat[3][1]
		- inMat[3][0] * inMat[0][1] * inMat[2][3]
		+ inMat[3][0] * inMat[0][3] * inMat[2][1];

	invMat[3][1] =
		+ inMat[0][0] * inMat[2][1] * inMat[3][2]
		- inMat[0][0] * inMat[2][2] * inMat[3][1]
		- inMat[2][0] * inMat[0][1] * inMat[3][2]
		+ inMat[2][0] * inMat[0][2] * inMat[3][1]
		+ inMat[3][0] * inMat[0][1] * inMat[2][2]
		- inMat[3][0] * inMat[0][2] * inMat[2][1];

	invMat[0][2] =
		+ inMat[0][1] * inMat[1][2] * inMat[3][3]
		- inMat[0][1] * inMat[1][3] * inMat[3][2]
		- inMat[1][1] * inMat[0][2] * inMat[3][3]
		+ inMat[1][1] * inMat[0][3] * inMat[3][2]
		+ inMat[3][1] * inMat[0][2] * inMat[1][3]
		- inMat[3][1] * inMat[0][3] * inMat[1][2];

	invMat[1][2] =
		- inMat[0][0] * inMat[1][2] * inMat[3][3]
		+ inMat[0][0] * inMat[1][3] * inMat[3][2]
		+ inMat[1][0] * inMat[0][2] * inMat[3][3]
		- inMat[1][0] * inMat[0][3] * inMat[3][2]
		- inMat[3][0] * inMat[0][2] * inMat[1][3]
		+ inMat[3][0] * inMat[0][3] * inMat[1][2];

	invMat[2][2] =
		+ inMat[0][0] * inMat[1][1] * inMat[3][3]
		- inMat[0][0] * inMat[1][3] * inMat[3][1]
		- inMat[1][0] * inMat[0][1] * inMat[3][3]
		+ inMat[1][0] * inMat[0][3] * inMat[3][1]
		+ inMat[3][0] * inMat[0][1] * inMat[1][3]
		- inMat[3][0] * inMat[0][3] * inMat[1][1];

	invMat[3][2] =
		- inMat[0][0] * inMat[1][1] * inMat[3][2]
		+ inMat[0][0] * inMat[1][2] * inMat[3][1]
		+ inMat[1][0] * inMat[0][1] * inMat[3][2]
		- inMat[1][0] * inMat[0][2] * inMat[3][1]
		- inMat[3][0] * inMat[0][1] * inMat[1][2]
		+ inMat[3][0] * inMat[0][2] * inMat[1][1];

	invMat[0][3] =
		- inMat[0][1] * inMat[1][2] * inMat[2][3]
		+ inMat[0][1] * inMat[1][3] * inMat[2][2]
		+ inMat[1][1] * inMat[0][2] * inMat[2][3]
		- inMat[1][1] * inMat[0][3] * inMat[2][2]
		- inMat[2][1] * inMat[0][2] * inMat[1][3]
		+ inMat[2][1] * inMat[0][3] * inMat[1][2];

	invMat[1][3] =
		+ inMat[0][0] * inMat[1][2] * inMat[2][3]
		- inMat[0][0] * inMat[1][3] * inMat[2][2]
		- inMat[1][0] * inMat[0][2] * inMat[2][3]
		+ inMat[1][0] * inMat[0][3] * inMat[2][2]
		+ inMat[2][0] * inMat[0][2] * inMat[1][3]
		- inMat[2][0] * inMat[0][3] * inMat[1][2];

	invMat[2][3] =
		- inMat[0][0] * inMat[1][1] * inMat[2][3]
		+ inMat[0][0] * inMat[1][3] * inMat[2][1]
		+ inMat[1][0] * inMat[0][1] * inMat[2][3]
		- inMat[1][0] * inMat[0][3] * inMat[2][1]
		- inMat[2][0] * inMat[0][1] * inMat[1][3]
		+ inMat[2][0] * inMat[0][3] * inMat[1][1];

	invMat[3][3] =
		+ inMat[0][0] * inMat[1][1] * inMat[2][2]
		- inMat[0][0] * inMat[1][2] * inMat[2][1]
		- inMat[1][0] * inMat[0][1] * inMat[2][2]
		+ inMat[1][0] * inMat[0][2] * inMat[2][1]
		+ inMat[2][0] * inMat[0][1] * inMat[1][2]
		- inMat[2][0] * inMat[0][2] * inMat[1][1];

	float det =
		+ inMat[0][0] * outMat[0][0]
		+ inMat[0][1] * outMat[1][0]
		+ inMat[0][2]* outMat[2][0]
		+ inMat[0][3] * outMat[3][0];

	// NOTE: (sonictk) If the determinant is 0, there is no inverse matrix
	if (areFloatsEqual(det, 0.0f)) {
		return -1;
	}

	float invDet = 1.0f / det;
	for (int r=0; r <= 3; ++r) {
		for (int c=0; c <= 3; ++c) {
			outMat[r][c] = invMat[r][c] * invDet;
		}
	}

	return 0;
}


Mat44 projection(float fov, float aspectRatio, float nearDist, float farDist)
{
	Mat44 result = mat44();

	// NOTE: (sonictk) Avoid degenerate cases which result in zero-division
	if (fov <= 0.0f || areFloatsEqual(aspectRatio, 0.0f)) {
		return result;
	}

	/* Referenced from: http://www.songho.ca/opengl/gl_projectionmatrix.html
	   General form of the Projection Matrix

	   uh = cot(fov / 2) == 1 / tan(fov / 2)
	   uw / uh = 1 / aspectRatio

	   uw        0       0       0
	   0        uh       0       0
	   0         0    -f/(f-n)  -1
	   0         0    -fn/(f-n)  0

	   Remember that in OpenGL however, this matrix is transposed.
	*/
	float frustumDepth = farDist - nearDist;
	result[1][1] = 1.0f / tanf(0.5f * fov * DEG_TO_RAD);
	result[0][0] = result[1][1] / aspectRatio;
	result[2][2] = -farDist / frustumDepth;
	result[2][3] = -1.0f;
	result[3][2] = -farDist * nearDist / frustumDepth;
	result[3][3] = 0.0f;

	return result;
}


Mat44 view(Vec3 pos, Vec3 target, Vec3 up)
{
	// NOTE: (sonictk) Implementation referenced from https://www.3dgep.com/understanding-the-view-matrix/#The_View_Matrix
	Vec3 zAxis = normalize(pos - target);                 // "forward" vector
	Vec3 xAxis = normalize(crossProduct(up, zAxis));      // "right" vector
	Vec3 yAxis = crossProduct(zAxis, xAxis);              // "up" vector

	Mat44 viewMat = {
		{{xAxis.x, yAxis.x, zAxis.x, -innerProduct(xAxis, pos)},
		 {xAxis.y, yAxis.y, zAxis.y, -innerProduct(yAxis, pos)},
		 {xAxis.z, yAxis.z, zAxis.z, -innerProduct(zAxis, pos)},
		 {0,       0,       0,       1}}
	};

	return viewMat;
}


Mat44 view(Vec3 pos, float pitch, float yaw)
{
	// NOTE: (sonictk) Implementation referenced from https://www.3dgep.com/understanding-the-view-matrix/#The_View_Matrix
	float cosPitch = cosf(pitch * DEG_TO_RAD);
	float sinPitch = sinf(pitch * DEG_TO_RAD);

	float cosYaw = cosf(yaw * DEG_TO_RAD);
	float sinYaw = sinf(yaw * DEG_TO_RAD);

	Vec3 xAxis = vec3(cosYaw, 0, -sinYaw);
	Vec3 yAxis = vec3(sinYaw * sinPitch, cosPitch, cosYaw * sinPitch);
	Vec3 zAxis = vec3(sinYaw * cosPitch, -sinPitch, cosPitch * cosYaw);

	Mat44 viewMat = {
		{{xAxis.x, yAxis.x, zAxis.x, -innerProduct(xAxis, pos)},
		 {xAxis.y, yAxis.y, zAxis.y, -innerProduct(yAxis, pos)},
		 {xAxis.z, yAxis.z, zAxis.z, -innerProduct(zAxis, pos)},
		 {0,       0,       0,       1}}
	};

	return viewMat;
}
