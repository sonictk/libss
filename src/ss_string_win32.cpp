#include "ss_platform.h"
#include "ss_string.h"


#ifdef __cplusplus
#include <cassert>
#else
#include <assert.h>
#endif // __cplusplus


#include <Strsafe.h>
#include <Shlwapi.h>


int strNCmp(const char *lhs, const char *rhs, const int count)
{
	int result = StrCmpN((PCTSTR)lhs, (PCTSTR)rhs, count);

	return result;
}


size_t findStrLenC(const char *str)
{
	size_t result;
	HRESULT status = StringCchLengthA((LPCTSTR)str, STRSAFE_MAX_CCH, &result);

	assert(SUCCEEDED(status));

	if (FAILED(status)) {
		return 0;
	}

	return result;
}


char *findSubStr(char *str, const char *tgt)
{
	PTSTR resultW = StrStrA((PTSTR)str, (PCTSTR)tgt);
	char *result = (char *)resultW;

	return result;
}

const char *findSubStr(const char *str, const char *tgt)
{
	PCTSTR resultW = StrStrA((PCTSTR)str, (PCTSTR)tgt);
	const char *result = (const char *)resultW;

	return result;
}


char *findRChar(char *str, int ch)
{
	PTSTR resultW = StrRChrA((PTSTR)str, NULL, (CHAR)ch);
	char *result = (char *)resultW;

	return result;
}

const char *findRChar(const char *str, int ch)
{
	PCTSTR resultW = StrRChrA((PCTSTR)str, NULL, (CHAR)ch);
	const char *result = (const char *)resultW;

	return result;
}


char *copyStringN(char *dest, const char *src, const size_t count)
{
	HRESULT status = StringCchCopyA((LPTSTR)dest, count + 1, (LPCTSTR)src);
	if (FAILED(status)) {
		return NULL;
	}

	// Ensure null-termination.
	dest[count] = '\0';

	return dest;
}


int CStrToInt(const char *str)
{
	int result = StrToInt((PCTSTR)str);

	return result;
}
