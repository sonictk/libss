/**
 * @file   ss_compression.h
 * @brief  Library for various compression algorithms on data storage.
 */
#ifndef SS_COMPRESSION_H
#define SS_COMPRESSION_H


/**
 * Unpacks the memory previously packed using RLE compression from ``src`` and stores the
 * resulting uncompressed data in ``dest``.
 *
 * @param dest 		The destination buffer to store the uncompressed data.
 *
 * @param src 			The source buffer containing the compressed RLE data.
 * 					This _must_ have enough data to unpack that fulfills the length specified by ``lenDest``.
 *
 * @param lenDest 		The maximum number of bytes to write in the destination buffer.
 */
void unpackBitsRLE(int8_t *dest, const int8_t *const src, int lenDest);


#endif /* SS_COMPRESSION_H */
