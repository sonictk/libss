/**
 * @file   ss_logging.h
 * @brief  Utilities for logging and debug output.
 */
#ifndef SS_LOGGING_H
#define SS_LOGGING_H


#ifdef __cplusplus

#include <cstdio>

#else

#include <stdio.h>

#endif // __cplusplus


#include "ss_ansi.h"


#ifdef _WIN32

#define SS_PERROR(fmt, ...) \
	do { fprintf(stderr, SS_ANSI_COLOR_RED); fprintf(stderr, fmt, __VA_ARGS__); fprintf(stderr, SS_ANSI_COLOR_RESET); fprintf(stderr, "\n"); } while (0)

#define SS_PWARNING(fmt, ...) \
	do { fprintf(stdout, SS_ANSI_COLOR_YELLOW); fprintf(stdout, fmt, __VA_ARGS__); fprintf(stdout, SS_ANSI_COLOR_RESET); fprintf(stdout, "\n"); } while (0)

#define SS_PINFO(fmt, ...) \
	do { fprintf(stdout, fmt, __VA_ARGS__); fprintf(stdout, "\n"); } while (0)


#elif __linux__ || __APPLE__

#define SS_PERROR(fmt, ...) \
	do { fprintf(stderr, SS_ANSI_COLOR_RED); fprintf(stderr, fmt, ##__VA_ARGS__); fprintf(stderr, SS_ANSI_COLOR_RESET); fprintf(stderr, "\n"); } while (0)

#define SS_PWARNING(fmt, ...) \
	do { fprintf(stdout, SS_ANSI_COLOR_YELLOW); fprintf(stdout, fmt, ##__VA_ARGS__); fprintf(stdout, SS_ANSI_COLOR_RESET); fprintf(stdout, "\n"); } while (0)

#define SS_PINFO(fmt, ...) \
	do { fprintf(stdout, fmt, ##__VA_ARGS__); fprintf(stdout, "\n"); } while (0)

#else

#error "Unsupported OS platform for compilation."


#endif // OS layer


#endif /* SS_LOGGING_H */
