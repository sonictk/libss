#include "ss_stream.h"

#include <Windows.h>
#include <Strsafe.h>

void SS_OSPrintLastError()
{
	char errMsg[256];
	DWORD errCode = GetLastError();
	if (errCode == 0) {
		return;
	}
#ifdef UNICODE
	FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,
				   NULL,
				   errCode,
				   MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
				   (LPTSTR)errMsg,
				   sizeof(errMsg),
				   NULL);
#else
	FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,
				   NULL,
				   errCode,
				   MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
				   (LPTSTR)errMsg,
				   sizeof(errMsg),
				   NULL);
#endif // UNICODE

	perror(errMsg);
}
