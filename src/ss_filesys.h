/**
 * @file		ss_filesys.h
 * @brief  	This header contains filesystem-related utility functions that
 * 			are intended to work across different operating platforms.
 */
#ifndef SS_FILESYS_H
#define SS_FILESYS_H

// NOTE: (sonictk) We do this so that for Linux, fseeko() will use 64 bit sizes for the ``off_t`` size.
#if __linux__ || __APPLE__
#define _FILE_OFFSET_BITS 64
#endif


// NOTE: (sonictk) We use the Windows implementation since the Linux one suffers
// from the [**Year 2038** issue](https://en.wikipedia.org/wiki/Year_2038_problem)
/// This contains a 64-bit value representing the number of 100-nanosecond
/// intervals since January 1, 1601 (UTC).
typedef uint64_t FileTime;


#define FILE_ACCESS_GENERIC_READ 1
#define FILE_ACCESS_GENERIC_WRITE 2

#define FILE_ACCESS_SHARE_READ 4
#define FILE_ACCESS_SHARE_WRITE 8
#define FILE_ACCESS_SHARE_DELETE 16
#define FILE_ACCESS_SHARE_BLOCK 32


enum SS_FileCreationDisposition
{
	SS_FileCreationDisposition_CreateAlways,
	SS_FileCreationDisposition_CreateNew,
	SS_FileCreationDisposition_OpenAlways,
	SS_FileCreationDisposition_OpenExisting,
	SS_FileCreationDisposition_TruncateExisting,

	SS_FileCreationDisposition_Count
};


/// Enums for where a file pointer should be positioned within a file.
enum FilePos
{
	FilePos_Begin,
	FilePos_Current,
	FilePos_End
};


/// Various types of seek positions within a file.
enum SeekOffsetPos
{
	SeekOffsetPos_Before,
	SeekOffsetPos_After,
};


/**
 * This converts the Windows ticks given to Unix seconds.
 *
 * @param win32Ticks 	The number of Windows ticks.
 *
 * @return 			The equivalent number of Unix seconds.
 */
inline uint win32TicksToUnixSeconds(dlong win32Ticks)
{
	return (uint)((win32Ticks / WINDOWS_TICK) - SEC_TO_UNIX_EPOCH);
}

/**
 * This converts the Unix seconds given to  Windows ticks.
 *
 * @param seconds 	The number of Unix seconds.
 *
 * @return 		The equivalent number of Windows ticks.
 */
inline dlong unixSecondsToWin32Ticks(uint seconds)
{
	return (dlong)((seconds + SEC_TO_UNIX_EPOCH) * WINDOWS_TICK);
}

/// @overload
inline dlong unixSecondsToWin32Ticks(timet seconds)
{
	return unixSecondsToWin32Ticks((uint)seconds);
}


#ifdef _WIN32

#ifdef LIBSS_STATIC_BUILD
#include "ss_filesys_win32.h"
#else
#include "ss_filesys_win32.cpp"
#endif // LIBSS_STATIC_BUILD

#elif __linux__ || __APPLE__

#ifdef LIBSS_STATIC_BUILD
#include "ss_filesys_linux.h"
#else
#include "ss_filesys_linux.cpp"
#endif // LIBSS_STATIC_BUILD

#else

#error "Unsupported OS platform for compilation."

#endif // Platform layer


// TODO: (sonictk) Maybe add C++ checks for overloads so that this can be a
// C-compatible header as well

/**
 * Gets the directory of the given path.
 *
 * @param path 		The original path to get the directory of.
 * @param dirPath 		The buffer to write the directory to.
 *
 * @return				Returns ``-1`` if the path could not be retrieved; returns
 * 					the length of the directory path on success and sets ``dirPath``
 * 					to the directory path.
 */
int getDirPath(const char *path, char *dirPath);


/**
 * This function retrieves the full file path to the currently running executable.
 * On Windows, this requires linking against ``Kernel32.lib`` and ``Shlwapi.lib``.
 *
 * @param filename		The buffer to store the full file path in. Needs to be
 * 					long enough to store the full path in, else it will be
 * 					truncated.
 * @param len			The number of characters that will be copied to the buffer.
 *
 * @return				The number of characters copied to the buffer.
 * 					On error, returns ``-1``.
 */
int getAppPath(char *filename, unsigned int len);


/**
 * This function gets the last modified time of the given ``filename``.
 *
 * @param filename 	The file to find the last modification time for.
 *
 * @return 			The last modified time. On failure or if the file does
 * 					not exist, returns ``0``.
 */
FileTime getLastWriteTime(const char *filename);


/**
 * Checks if the file specified at the given ``filePath`` exists on disk.
 *
 * @param filePath 	The file to check.
 *
 * @return 			``true`` if the file exists, ``false`` otherwise or if an
 * 					error occurred while attempting to validate the file handle.
 */
bool doesFileExist(char *filePath);


/**
 * This function will rename the ``oldPath`` to the ``newPath``.
 * **This will overwrite the file specified in the new location!**
 *
 * @param oldPath 		The path to the old file to rename.
 * @param newPath 		The path that the new file will have.
 *
 * @return			``0`` on success, a negative value on failure.
 */
inline int renameFile(const char *oldPath, const char *newPath);


/**
 * Checks if the given path points to a valid file.
 *
 * @param path 	The full path to the file.
 *
 * @return 		``true`` if the file is valid, ``false`` otherwise.
 */
bool isFile(const char *path);


/**
 * Checks if the given path points to a valid directory.
 *
 * @param path 	The full path to the directory.
 *
 * @return 		``true`` if the path is a valid directory, ``false`` otherwise.
 */
bool isDirectory(const char *path);


/**
 * Creates a file handle to the given file path.
 *
 * @param path 	The full path to the file.
 *
 * @param access	The desired access to the file/device. Should be ``FILE_ACCESS_GENERIC_READ``,
 * 				``FILE_ACCESS_GENERIC_WRITE``, or both.
 *
 * @param shareMode The requested sharing mode of the file/device. This determines whether other
 * 				processes can continue to interact with the file/device while one process has an
 * 				open handle to it. Should be FILE_ACCESS_SHARE_READ, FILE_ACCESS_SHARE_WRITE,
 * 				FILE_ACCESS_SHARE_DELETE or FILE_ACCESS_SHARE_BLOCK. This flag is platform-specific
 * 				depending on which OSes support this feature.
 *
 * @param creationDisposition 	An action to take on a file or device that exists or does not exist.
 *
 * @return 		The OS file handle to the data. If an error occurred, returns a null
 * 				pointer and sets the global application error code, which can be retrieved
 * 				with ``getAppLastError``.
 */
SS_OSFileHandle createFile(const char *path, const int access, const int shareMode, const SS_FileCreationDisposition creationDisposition);


/**
 * Opens a file handle to the given file path. The existing file should already exist on disk.
 *
 * @param path 	The full path to the file.
 *
 * @param access	The desired access to the file/device. Should be ``FILE_ACCESS_GENERIC_READ``,
 * 				``FILE_ACCESS_GENERIC_WRITE``, or both.
 *
 * @param shareMode The requested sharing mode of the file/device. This determines whether other
 * 				processes can continue to interact with the file/device while one process has an
 * 				open handle to it. Should be FILE_ACCESS_SHARE_READ, FILE_ACCESS_SHARE_WRITE,
 * 				FILE_ACCESS_SHARE_DELETE or FILE_ACCESS_SHARE_BLOCK.
 *
 * @return 		The OS file handle to the data. If an error occurred, returns a null
 * 				pointer and sets the global application error code, which can be retrieved
 * 				with ``getAppLastError``.
 */
SS_OSFileHandle openFile(const char *path, const int access, const int shareMode);


/**
 * Symmetric version of ``openFile``. Closes an existing file handle.
 *
 * @param hFile 	The file handle to close.
 *
 * @return 		``0`` on success. If an error occurred, returns a negative value
 * 				and sets the global application error code, which can be retrieved
 * 				with ``getAppLastError``.
 */
int closeFile(SS_OSFileHandle &hFile);


/**
 * Checks if the given file ``path`` specified is a relative file path (i.e. one
 * that depends on having a base directory to determine the full path from).
 *
 * @param path		The file path to check.
 *
 * @return			``true`` if the path is a relative path, ``false`` otherwise.
 */
bool isRelativePath(const char *path);


/**
 * Formats an absolute file path from a relative ``path``.
 *
 * If ``dest`` is set to ``NULL`` and ``len`` is also set to ``0``, this function
 * will return the size of the buffer required to store the result for the full
 * path, excluding the null terminator. If an error occurred, returns ``0``.
 *
 * @param dest		The buffer to store the result.
 *
 * @param path		The relative path to format a fully-qualified file path for.
 *
 * @param len		The size of the destination buffer to store the result, in bytes.
 * 				This should be large enough to also account for the null terminator.
 *
 * @return			The length of the string copied to the destination buffer.
 */
int absolutePath(char *dest, const char *path, const int len);


/**
 * Sets the file seek postion for an open file handle.
 *
 * @param hFile		The file handle to set the seek position for.
 * @param distance		The amount to change the seek head by, in bytes.
 * @param movement		The type of movement to perform on the seek head.
 *
 * @return				The position of the new file pointer. If an error occurred,
 * 					returns a negative value and sets the global application error
 * 					code, which can be retrieved with ``getAppLastError``.
 */
int setFilePtr(SS_OSFileHandle &hFile, int distance, FilePos movement);

/// @overload
int64_t setFilePtr64(SS_OSFileHandle &hFile, int64_t distance, FilePos movement);


/**
 * Retrieves the current position of the file pointer within an open file handle.
 *
 * @param hFile		The file handle to retrieve the current seek position for.
 *
 * @return				The position of the file pointer, in bytes, from the start of the file.
 * 					If an error occurred, returns a negative value and sets the
 * 					global application error code, which can be retrieved with ``getAppLastError``.
 */
int getFilePtrPos(SS_OSFileHandle &hFile);


/**
 * Reads ``size`` bytes of the given file handle into the ``buffer`` provided.
 * Be aware that this _will_ move the current file pointer by the amount of bytes read.
 *
 * @param hFile 		The file handle to read.
 *
 * @param buffer 		The destination buffer to read the bytes to. This buffer
 * 					_must_ be at least of size ``size``.
 *
 * @param size 		The number of bytes to read into the buffer.
 *
 * @return				The number of bytes actually read from the file handle.
 * 					If an error occured, returns a negative value and sets the
 * 					global application error code, which can be retrieved with ``getAppLastError``.
 */
int readFile(SS_OSFileHandle &hFile, void *buffer, int size);


/**
 * Reads ``size`` bytes of the given file handle into the ``buffer`` provided.
 * Be aware that this _will_ move the current file pointer by the amount of bytes read.
 * This function should be used for reading sizes that are larger than that of a 32-bit value.
 *
 * @param hFile 		The file handle to read.
 *
 * @param buffer 		The destination buffer to read the bytes to. This buffer
 * 					_must_ be at least of size ``size``.
 *
 * @param size 		The number of bytes to read into the buffer. This _must_ be a minimum of 8 bytes.
 *
 * @return				The number of bytes actually read from the file handle.
 * 					If an error occured, returns a negative value and sets the
 * 					global application error code, which can be retrieved with ``getAppLastError``.
 */
int64_t readFile64(SS_OSFileHandle &hFile, void *buffer, int64_t size);


/**
 * Writes data to the specified file or input/output (I/O) device.
 *
 * @param hFile 			The file to write data to.
 * @param buffer			The buffer containing the information to write to the file.
 * @param bytesToWrite		The number of bytes to write.
 * @param bytesWritten		The storage for the number of bytes actually written.
 *
 * @return					``true`` if the operation succeeded, ``false`` otherwise.
 */
bool writeFile(SS_OSFileHandle &hFile, const void *buffer, int bytesToWrite, int &bytesWritten);


/**
 * Removes the path portion of a fully qualified path and file.
 *
 * @param path A pointer to a null-terminated string of length
 * 				PSDPARSER_MAX_PATH that contains the path and file
 * 				name. When this function returns successfully, the string
 * 				contains only the file name, with the path removed.
 */
void pathStripPath(char *path);


SS_OSFileHandle createTempFile();


char *getTempDirPath();


#endif /* SS_FILESYS_H */
