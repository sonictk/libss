/**
 * @file   	ss_test.h
 * @brief  	Utilities for testing code.
 */
#ifndef SS_TEST_H
#define SS_TEST_H

#ifdef __cplusplus

#include <cstdarg>

#else

#include <stdarg.h>

#endif // __cplusplus

#include "ss_ansi.h"
#include "ss_timer.h"


/// Storage for the number of tests that have been run so far.
extern int SS_TESTS_RUN;

/// Storage for the number of tests that have failed. This is used for tests that
/// are executed via ``SS_SASSERT`` and its variants.
extern int SS_TESTS_FAILED;


/// Function pointer for valid tests to be run in this library.
typedef int (SSTestFunction)();


/**
 * Macro for printing a message to ``stdout``.
 *
 * @return 	Number of characters in the error message.
 */
#define SS_LOGM(message) printf(SS_ANSI_COLOR_CYAN "\n# %s %s():%d %s\n" SS_ANSI_COLOR_RESET, __FILE__, __func__, __LINE__, message)


/**
 * Macro for printing an error message to ``stdout``.
 *
 * @return 	Number of characters in the error message.
 */
#define SS_FAIL(expr) printf(SS_ANSI_COLOR_RED "\n######################\n Test failure in %s():%d in %s!\nThe following assertion failed: %s\n######################\n" SS_ANSI_COLOR_RESET, __func__, __LINE__, __FILE__, #expr)


/**
 * Macro for printing an error message to ``stdout``.
 *
 * @param expr	The assertion being made.
 *
 * @param message 	A custom message to print as part of the failure.
 *
 * @return 		Number of characters in the error message.
 */
#define SS_FAILM(expr, message) printf(SS_ANSI_COLOR_RED "\n######################\n Test failure in %s():%d in %s!\nThe following assertion failed: %s\nReason: %s\n######################\n" SS_ANSI_COLOR_RESET, __func__, __LINE__, __FILE__, #expr, message)


/**
 * Macro for printing a successful test result to ``stdout``.
 *
 * @return		Number of characters in the message.
 */
#define SS_SUCCEED() printf(SS_ANSI_COLOR_GREEN "# Succeeded: %s():%d\n" SS_ANSI_COLOR_RESET, __func__, __LINE__)


/**
 * Macro for printing a successful test result to ``stdout``.
 *
 * @param message	A custom message to print as part of the message.
 *
 * @return			Number of characters in the message.
 */
#define SS_SUCCEEDM(message) printf(SS_ANSI_COLOR_GREEN "# Succeeded: %s (%s():%d)\n" SS_ANSI_COLOR_RESET, message, __func__, __LINE__)


/**
 * Macro for printing a assertion pass result to ``stdout``.
 *
 * @return		Number of characters in the message.
 */
#define SS_PASSED(expr) printf(SS_ANSI_COLOR_CYAN "# Passed: %s (assertion made at %s():%d)\n" SS_ANSI_COLOR_RESET, #expr, __func__, __LINE__)


/**
 * Basic unit test that runs the given ``test`` function. Prints a success message
 * to ``stdout`` if the test succeeded. The test must return ``0`` on success, and
 * a negative value on failure. If the test fails, program execution is halted.
 *
 * @param test		The expression (rvalue) to check.
 *
 * @return			``-1`` if the test failed and prints a message to ``stdout``.
 */
#define SS_ASSERT(test) do { if (!(test)) { SS_FAIL(test); ++SS_TESTS_FAILED; return -1; } else { SS_PASSED(test); } } while (0)


/**
 * Basic unit test that runs the given ``test`` function. Prints a success message
 * to ``stdout`` if the test succeeded and the ``message`` specified if the test failed.
 * The test must return ``0`` on success, and a negative value on failure. If the
 * test fails, program execution is halted.
 *
 * @param test		The expression (rvalue) to check.
 * @param message 	The message to print if the test fails.
 *
 * @return			``-1`` if the test failed and prints a message to ``stdout``.
 */
#define SS_ASSERTM(test, message) do { if (!(test)) { SS_FAILM(test, message); ++SS_TESTS_FAILED; return -1; } else { SS_PASSED(test); } } while (0)


/**
 * Basic unit test that runs the given ``test`` function. Prints a success message
 * to ``stdout`` if the test succeeded. The test must return ``0`` on success, and
 * a negative value on failure. Unlike ``SS_ASSERT`` and ``SS_ASSERTM``, failure
 * only displays an error message; program execution is allowed to continue (Soft
 * assert).
 *
 * @param test		The expression (rvalue) to check.
 *
 * @return			``-1`` if the test failed and prints a message to ``stdout``.
 */
#define SS_SASSERT(test) do { if (!(test)) { SS_FAIL(test); ++SS_TESTS_FAILED; return 0; } else { SS_PASSED(test); } } while (0)


/**
 * Basic unit test that runs the given ``test`` function. Prints a success
 * message to ``stdout`` if the test succeeded and the ``message`` specified if
 * the test failed.  The test must return ``0`` on success, and a negative value
 * on failure. Unlike ``SS_ASSERT`` and ``SS_ASSERTM``, failure only displays an
 * error message; program execution is allowed to continue (Soft assert).
 *
 * @param test		The expression (rvalue) to check.
 * @param message 	The message to print if the test fails.
 *
 * @return			``-1`` if the test failed and prints a message to ``stdout``.
 */
#define SS_SASSERTM(test, message) do { if (!(test)) { SS_FAILM(test, message); ++SS_TESTS_FAILED; return 0; } else { SS_PASSED(test); } } while (0)


/**
 * Verifies that a test runs successfully and increments the global test counter.
 * To use this, ``SS_TESTS_RUN`` must have been defined.
 * The test must return ``0`` on success, and a negative value on failure.
 *
 * @param test 	The test to verify the result for.
 *
 * @return			Returns the result from a given test.
 */
#define SS_VERIFY(test) do { int r=test(); ++SS_TESTS_RUN; if(r) { return r; } else { SS_SUCCEED(); } } while (0)


/**
 * Verifies that a test runs successfully and increments the global test counter.
 * To use this, ``SS_TESTS_RUN`` must have been defined.
 * The test must return ``0`` on success, and a negative value on failure.
 *
 * @param test 	The test to verify the result for.
 *
 * @return			Returns the result from a given test.
 */
#define SS_VERIFYM(test, message) do { int r=test(); ++SS_TESTS_RUN; if(r) { return r; } else { SS_SUCCEEDM(message); } } while (0)

// TODO: (sonictk) Think about a low-overhead way for categorizing tests.

// TODO: (sonictk) Think about printing timing information in each test.


#endif /* SS_TEST_H */
