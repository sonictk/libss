#include "ss_platform.h"
#include "ss_filesys.h"
#include "ss_string.h"
#include "ss_stream.h"
#include "ss_memory.h"
#include "ss_errno.h"

#include <Shlwapi.h>
#include <strsafe.h>


bool doesFileExist(char *filePath)
{
	HANDLE fileHandle = CreateFile((LPCTSTR)filePath,
								   GENERIC_READ,
								   FILE_SHARE_READ,
								   NULL,
								   OPEN_EXISTING,
								   FILE_ATTRIBUTE_NORMAL,
								   NULL);

	if (fileHandle == INVALID_HANDLE_VALUE) {
		return false;
	} else {
		BOOL status = CloseHandle(fileHandle);
		if (status == 0) {
			return false;
		}

		return true;
	}
}


int getAppPath(char *filename, unsigned int len)
{
	DWORD pathLen = GetModuleFileName(NULL, (LPTSTR)filename, (DWORD)len);
	if (pathLen == 0) {
		perror("Failed to get executable path!\n");
		return -1;
	}
	return pathLen;
}


int getDirPath(const char *path, char *dirPath)
{
	StringCchCopy((LPTSTR)dirPath, MAX_PATH, (LPCTSTR)path);
	BOOL result = PathRemoveFileSpec((LPTSTR)dirPath);
	if (result == 0) {
		perror("Path was unmodified!\n");
		return -1;
	}
	size_t strLen;

#ifdef UNICODE
	WCHAR dirPathW[MAX_PATH];
	int numChars = MultiByteToWideChar(CP_UTF8, MB_PRECOMPOSED, dirPath, -1, dirPathW, MAX_PATH);
	StringCchLengthW(dirPathW, STRSAFE_MAX_CCH, &strLen);
#else
	StringCchLength(dirPath, STRSAFE_MAX_CCH, &strLen);
#endif // UNICODE

	return (int)strLen;
}


inline bool win32IsTimeValid(FILETIME time)
{
	bool result = (time.dwLowDateTime != 0) || (time.dwHighDateTime != 0);
	return result;
}


FileTime getLastWriteTime(const char *filename)
{
	FileTime result = 0;

	FILETIME lastWriteTime;
	WIN32_FILE_ATTRIBUTE_DATA data;
	if (GetFileAttributesEx((LPCTSTR)filename, GetFileExInfoStandard, &data)) {
		lastWriteTime = data.ftLastWriteTime;
	} else {
		SS_OSPrintLastError();
		return result;
	}

	result = (FileTime)lastWriteTime.dwHighDateTime << sizeof(DWORD)|lastWriteTime.dwLowDateTime;

	return result;
}


inline int convertPathSeparatorsToOSNative(char *filename)
{
	sizet len = findStrLenC(filename);
	char *tmp = (char *)ss_malloc((len + 1) * sizeof(char));
	int replaced = replaceChar(filename,
							   tmp,
							   UNIX_PATH_SEPARATOR,
							   SS_OS_PATH_SEPARATOR,
							   (unsigned int)len + 1);
	if (replaced <= 0) {
		return replaced;
	}

	copyStringN(filename, tmp, len + 1);
	ss_free(tmp);

	return replaced;
}


inline int win32RenameFile(const char *oldPath, const char *newPath, unsigned int flags)
{
	BOOL result = MoveFileEx((LPCTSTR)oldPath, (LPCTSTR)newPath, (DWORD)flags);
	if (result == 0) {
		SS_OSPrintLastError();
		return -1;
	}

	return 0;
}

inline int renameFile(const char *oldPath, const char *newPath)
{
	return win32RenameFile(oldPath, newPath, MOVEFILE_REPLACE_EXISTING|MOVEFILE_WRITE_THROUGH);
}


bool isFile(const char *path)
{
	DWORD fileAttrs = GetFileAttributes((LPCTSTR)path);
	if (fileAttrs == INVALID_FILE_ATTRIBUTES) {
		return false;
	} else {
		return true;
	}
}


bool isDirectory(const char *path)
{
	DWORD fileAttrs = GetFileAttributes((LPCTSTR)path);
	if (fileAttrs == INVALID_FILE_ATTRIBUTES) {
		return false;
	} else {
		if (fileAttrs & FILE_ATTRIBUTE_DIRECTORY) {
			return true;
		}
		return false;
	}
}


SS_OSFileHandle createFile(const char *path, const int access, const int shareMode, const SS_FileCreationDisposition creationDisposition)
{
	DWORD desiredAccess = 0;
	if (access & FILE_ACCESS_GENERIC_READ) {
		desiredAccess |= GENERIC_READ;
	}
	if (access & FILE_ACCESS_GENERIC_WRITE) {
		desiredAccess |= GENERIC_WRITE;
	}

	DWORD accessShareMode = 0;
	if (shareMode & FILE_ACCESS_SHARE_READ) {
		accessShareMode |= FILE_SHARE_READ;
	}
	if (shareMode & FILE_SHARE_WRITE) {
		accessShareMode |= FILE_SHARE_WRITE;
	}
	if (shareMode & FILE_SHARE_DELETE) {
		accessShareMode |= FILE_SHARE_DELETE;
	}
	if (shareMode & FILE_ACCESS_SHARE_BLOCK) {
		accessShareMode = 0;
	}

	DWORD creationDispositionW;
	switch(creationDisposition) {
	case SS_FileCreationDisposition_CreateAlways:
		creationDispositionW = CREATE_ALWAYS;
		break;
	case SS_FileCreationDisposition_CreateNew:
		creationDispositionW = CREATE_NEW;
		break;
	case SS_FileCreationDisposition_OpenAlways:
		creationDispositionW = OPEN_ALWAYS;
		break;
	case SS_FileCreationDisposition_OpenExisting:
		creationDispositionW = OPEN_EXISTING;
		break;
	case SS_FileCreationDisposition_TruncateExisting:
		creationDispositionW = TRUNCATE_EXISTING;
		break;
	default:
		SSSetLastError(SS_Status_InvalidParameter);

		return NULL;
	}

	HANDLE hFile = CreateFileA(path,
							   desiredAccess,
							   accessShareMode,
							   NULL,
							   creationDispositionW,
							   FILE_ATTRIBUTE_NORMAL,
							   NULL);

	if (hFile == INVALID_HANDLE_VALUE) {
		DWORD win32LastError = GetLastError();

		switch(win32LastError) {
		case ERROR_ACCESS_DENIED:
			SSSetLastError(SS_Status_AccessDenied);
			break;
		default:
			SSSetLastError(SS_Status_InvalidFilePath);
			break;
		}

		return NULL;
	}

	return hFile;
}


SS_OSFileHandle openFile(const char *path, const int access, const int shareMode)
{
	DWORD desiredAccess = 0;
	if (access & FILE_ACCESS_GENERIC_READ) {
		desiredAccess |= GENERIC_READ;
	}
	if (access & FILE_ACCESS_GENERIC_WRITE) {
		desiredAccess |= GENERIC_WRITE;
	}

	DWORD accessShareMode = 0;
	if (shareMode & FILE_ACCESS_SHARE_READ) {
		accessShareMode |= FILE_SHARE_READ;
	}
	if (shareMode & FILE_SHARE_WRITE) {
		accessShareMode |= FILE_SHARE_WRITE;
	}
	if (shareMode & FILE_SHARE_DELETE) {
		accessShareMode |= FILE_SHARE_DELETE;
	}
	if (shareMode & FILE_ACCESS_SHARE_BLOCK) {
		accessShareMode = 0;
	}

	HANDLE hFile = CreateFileA(path,
							   desiredAccess,
							   accessShareMode,
							   NULL,
							   OPEN_EXISTING,
							   FILE_ATTRIBUTE_NORMAL,
							   NULL);

	if (hFile == INVALID_HANDLE_VALUE) {
		DWORD win32LastError = GetLastError();

		switch(win32LastError) {
		case ERROR_ACCESS_DENIED:
			SSSetLastError(SS_Status_AccessDenied);
			break;
		default:
			SSSetLastError(SS_Status_InvalidFilePath);
			break;
		}

		return NULL;
	}

	return hFile;
}


int closeFile(SS_OSFileHandle &hFile) {
	BOOL result = CloseHandle(hFile);
	if (result == 0) {

		SSSetLastError(SS_Status_UnableToCloseHandle);

		return -1;
	}

	return 0;
}


bool isRelativePath(const char *path)
{
	BOOL isRelative = PathIsRelativeA((LPCSTR)path);
	bool result = (bool)isRelative;

	return result;
}


int absolutePath(char *dest, const char *path, const int len)
{
	DWORD lenDest = GetFullPathNameA((LPCSTR)path, (DWORD)len, (LPSTR)dest, NULL);
	int result = (int)lenDest;

	return result;
}


int setFilePtr(SS_OSFileHandle &hFile, int distance, FilePos movement)
{
	DWORD moveMethod;
	switch (movement) {
	case FilePos_Begin:
		moveMethod = FILE_BEGIN;
		break;
	case FilePos_Current:
		moveMethod = FILE_CURRENT;
		break;
	case FilePos_End:
		moveMethod = FILE_END;
		break;
	default:
		SSSetLastError(SS_Status_InvalidParameter);

		return -1;
	}

	DWORD filePtr = SetFilePointer(hFile, distance, NULL, moveMethod);
	if (filePtr == INVALID_SET_FILE_POINTER) {
		SSSetLastError(SS_Status_CannotSetFilePtr);

		return -2;
	}

	int result = (int)filePtr;

	return result;
}

int64_t setFilePtr64(SS_OSFileHandle &hFile, int64_t distance, FilePos movement)
{
	DWORD moveMethod;
	switch (movement) {
	case FilePos_Begin:
		moveMethod = FILE_BEGIN;
		break;
	case FilePos_Current:
		moveMethod = FILE_CURRENT;
		break;
	case FilePos_End:
		moveMethod = FILE_END;
		break;
	default:
		SSSetLastError(SS_Status_InvalidParameter);

		return -1;
	}

	LARGE_INTEGER distanceW;
	distanceW.QuadPart = (LONGLONG)distance;

	LARGE_INTEGER resultW;
	BOOL status = SetFilePointerEx(hFile, distanceW, &resultW, moveMethod);
	if (status == 0) {
		SSSetLastError(SS_Status_CannotSetFilePtr);

		return -2;
	}

	int64_t result = (int64_t)resultW.QuadPart;

	return result;
}


int getFilePtrPos(SS_OSFileHandle &hFile)
{
	DWORD filePtr = SetFilePointer(hFile, 0, NULL, FILE_CURRENT);
	if (filePtr == INVALID_SET_FILE_POINTER) {
		SSSetLastError(SS_Status_CannotSetFilePtr);

		return -1;
	}

	int result = (int)filePtr;

	return result;
}


int readFile(SS_OSFileHandle &hFile, void *buffer, int size)
{
	DWORD bytesRead;
	BOOL status = ReadFile((HANDLE)hFile, (LPVOID)buffer, (DWORD)size, &bytesRead, NULL);
	if (status) {
		if (bytesRead == 0) {
			SSSetLastError(SS_Status_EndOfFile);

			return -2;
		}

		int result = int(bytesRead);

		return result;

	} else {
		DWORD win32LastError = GetLastError();
		switch(win32LastError) {
		case ERROR_ACCESS_DENIED:
			SSSetLastError(SS_Status_AccessDenied);
			break;
		default:
			SSSetLastError(SS_Status_FileReadFailure);
			break;
		}
	}

	return -1;
}


int64_t readFile64(SS_OSFileHandle &hFile, void *buffer, int64_t size)
{
	assert(size != 0);

	// NOTE: (sonictk) Easiest way to ensure read succeeds; divide reads into chunked reads
	// instead of one huge access.
	DWORD chunkSize = sizeof(int64_t);
	int64_t numChunks = size / (int64_t)chunkSize;
	int64_t finalChunkSize = size % (int64_t)chunkSize;

	DWORD chunkBytesRead = 0;
	int64_t totalBytesRead = 0;
	BOOL status;
	for (int64_t i=0; i < numChunks; ++i) {
		status = ReadFile((HANDLE)hFile, (LPVOID)((uint8_t *)buffer + totalBytesRead), (DWORD)chunkSize, &chunkBytesRead, NULL);
		if (status) {
			if (chunkBytesRead == 0) {
				SSSetLastError(SS_Status_EndOfFile);
				return -2;
			} else {
				SSSetLastError(SS_Status_FileReadFailure);
				return -3;
			}
		}
		assert(chunkBytesRead == chunkSize);

		totalBytesRead += chunkBytesRead;
	}

	status = ReadFile((HANDLE)hFile, (LPVOID)((uint8_t *)buffer + totalBytesRead), (DWORD)finalChunkSize, &chunkBytesRead, NULL);
	if (status) {
		if (chunkBytesRead == 0) {
			SSSetLastError(SS_Status_EndOfFile);
			return -2;
		} else {
			SSSetLastError(SS_Status_FileReadFailure);
			return -3;
		}
	}
	assert(chunkBytesRead == chunkSize);

	totalBytesRead += chunkBytesRead;

	return totalBytesRead;
}


bool writeFile(SS_OSFileHandle &hFile, const void *buffer, int bytesToWrite, int &bytesWritten)
{
	BOOL writeResult = WriteFile(hFile, (LPCVOID)buffer, (DWORD)bytesToWrite, (LPDWORD)&bytesWritten, NULL);
	bool result = (bool)writeResult;

	return result;
}


void pathStripPath(char *path)
{
	assert(path != NULL);

	PathStripPath((LPTSTR)path);

	return;
}


SS_OSFileHandle createTempFile()
{
	char tempPath[SS_MAX_PATH];
#ifdef _DEBUG
	DWORD lenPath = GetTempPath(SS_MAX_PATH, tempPath);
	assert(lenPath != 0);
#else
	GetTempPath(SS_MAX_PATH, tempPath);
#endif // _DEBUG

	char tempFileName[SS_MAX_PATH];
#ifdef _DEBUG
	UINT unid = GetTempFileName((LPCTSTR)tempPath, "tmp", 0, tempFileName);
	assert(unid != 0);
#else
	GetTempFileName((LPCTSTR)tempPath, "tmp", 0, tempFileName);
#endif // _DEBUG

	SS_OSFileHandle result = createFile(tempFileName,
										FILE_ACCESS_GENERIC_READ|FILE_ACCESS_GENERIC_WRITE,
										FILE_ACCESS_SHARE_READ,
										SS_FileCreationDisposition_CreateAlways);

	assert(result != NULL);

	return result;
}


char *getTempDirPath()
{
	char *tmpPath = (char *)ss_malloc((SS_MAX_PATH + 1) * sizeof(char));
	DWORD lenPath = GetTempPathA(SS_MAX_PATH + 1, tmpPath);
	if (lenPath == 0) { return NULL; }
	return tmpPath;
}
