#ifndef SS_IMAGE_WRITE_H
#define SS_IMAGE_WRITE_H


int ss_write_ppm(const char *filename,
				 const uint8_t const *buffer,
				 const int width,
				 const int height,
				 const int max,
				 const int channels);


#endif /* SS_IMAGE_WRITE_H */
