#include "ss_uuid.h"
#include "ss_string.h"


#include <Windows.h>


#define SS_UUID_NULL GUID_NULL

typedef GUID SSUuid;


int createUuid(SSUuid &uuid)
{
	HRESULT result = CoCreateGuid(&uuid);

	if (result != S_OK) {
		return -1;
	}

	return 0;
}


int getUuidAsString(const SSUuid &uuid, char name[SS_UUID_STRING_LENGTH])
{
	RPC_CSTR uuidStr;
	RPC_STATUS status = UuidToString(&uuid, &uuidStr);
	if (status != S_OK) {
		return -1;
	}

	copyStringN(name, (const char *)uuidStr, SS_UUID_STRING_LENGTH + 1);

	status = RpcStringFree(&uuidStr);
	if (status != S_OK) {
		return -2;
	}

	return 0;
}


int getUuid(const char name[SS_UUID_STRING_LENGTH], SSUuid &uuid)
{
	RPC_STATUS status = UuidFromString((unsigned char *)(LPCTSTR)name, &uuid);

	switch (status) {
	case RPC_S_OK:
		return 0;
	case RPC_S_INVALID_STRING_UUID:
		return -1;
	default:
		return -2;
	}
}


bool areUuidsEqual(SSUuid uuid1, SSUuid uuid2)
{
	// NOTE: (sonictk) We pass by value here since there does not exist a viable
	// conversion from const UUID * to UUID *
	RPC_STATUS status;
	int result = UuidEqual(&uuid1, &uuid2, &status);

	if (status != RPC_S_OK) {
		return false;
	}

	return (bool)result;
}
