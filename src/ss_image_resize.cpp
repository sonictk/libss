#ifndef SS_IMAGE_RESIZE_H
#define SS_IMAGE_RESIZE_H


#ifdef __cplusplus
#include <cmath>
#include <cassert>
#else
#include <math.h>
#endif // __cplusplus


void resizeImgDataNearestNeighbour(uint8_t *dest,
								   const uint8_t *src,
								   const int origWidth,
								   const int origHeight,
								   const int newWidth,
								   const int newHeight,
								   const int numChannels,
								   const int bytesPerChannel)
{
	assert(dest != NULL);
	assert(src != NULL);
	assert(origWidth > 0);
	assert(origHeight > 0);
	assert(newWidth > 0);
	assert(newHeight > 0);
	assert(numChannels > 0);

	// NOTE: (sonictk) Basically, for the new image buffer, for the current pixel being filled,
	// find the nearest neighbouring pixel and use that pixel's colour.
	float xRatio = (float)origWidth / (float)newWidth;
	float yRatio = (float)origHeight / (float)newHeight;

	const int newStrideInBytes = newWidth * numChannels * bytesPerChannel;
	const int oldStrideInBytes = origWidth * numChannels * bytesPerChannel;

	float srcCol = 0;
	float srcRow = 0;
	for (int row=0; row < newHeight; ++row) {
		for (int col=0; col < newWidth; ++col) {
			srcCol = floorf((float)col * xRatio);
			srcRow = floorf((float)row * yRatio);
			for (int chn=0; chn < numChannels; ++chn) {
				for (int bpc=0; bpc < bytesPerChannel; ++bpc) {
					int destIdx = (row * newStrideInBytes) + (col * numChannels * bytesPerChannel) + (chn * bytesPerChannel) + bpc;
					int srcIdx = ((int)srcRow * oldStrideInBytes) + ((int)srcCol * numChannels * bytesPerChannel) + (chn * bytesPerChannel) + bpc;

					assert(destIdx < (newHeight * newStrideInBytes));
					assert(srcIdx < (origHeight * oldStrideInBytes));

					dest[destIdx] = src[srcIdx];
				}
			}
		}
	}

	return;
}


void resizeImgDataBilinear(uint8_t *dest,
						   const uint8_t *src,
						   const int origWidth,
						   const int origHeight,
						   const int newWidth,
						   const int newHeight,
						   const int numChannels,
						   const int bytesPerChannel)
{
	assert(dest != NULL);
	assert(src != NULL);
	assert(origWidth > 0);
	assert(origHeight > 0);
	assert(newWidth > 0);
	assert(newHeight > 0);
	assert(numChannels > 0);
	assert(bytesPerChannel > 0);

	// NOTE: (sonictk) Basically, for each output pixel:
	// Find the 2x2 matrix of pixels from the source to interpolate between.
	// We shall refer to the matrix of pixels as ABCD, where A is the top-left pixel,
	// B is the top-right pixel, C is the bottom-left pixel and D is the bottom-right
	// pixel of the 2x2 matrix.
	// Lerp AB and CD separately, then lerp both results based on where the dest. pixel is.
	float xRatio = (float)origWidth / (float)newWidth;
	float yRatio = (float)origHeight / (float)newHeight;

	int newStrideInBytes = newWidth * numChannels * bytesPerChannel;
	int oldStrideInBytes = origWidth * numChannels * bytesPerChannel;
	int lenSrc = oldStrideInBytes * origHeight;

	int ax = 0;
	int ay = 0;
	int bx = 0;
	int by = 0;
	int cx = 0;
	int cy = 0;
	int dx = 0;
	int dy = 0;
	float ctrX = 0.0f;
	float ctrY = 0.0f;
	float srcCtrX = 0.0f;
	float srcCtrY = 0.0f;
	for (int r=0; r < newHeight; ++r) {
		for (int c=0; c < newWidth; ++c) {
			// NOTE: (sonictk) We floor to just choose the nearest pixel from the given factor.
			// With luck (except for pixel art) this should be "good enough".
			// Here we form the cross matrix of pixels to lerp between. We start by finding their
			// respective x-y coords.
			ctrX = (float)c / (float)newWidth;
			ctrY = (float)r / (float)newHeight;

			srcCtrX = ctrX * origWidth;
			srcCtrY = ctrY * origHeight;

			ax = (int)(floorf(srcCtrX));
			ay = (int)(floorf(srcCtrY));

			bx = clamp(ax + 1, 0, origWidth - 1);
			by = ay;

			cx = ax;
			cy = clamp(ay + 1, 0, origHeight - 1);

			dx = bx;
			dy = cy;

			for (int chn=0; chn < numChannels; ++chn) {
				for (int bpc=0; bpc < bytesPerChannel; ++bpc) {
					int destIdx = (r * newStrideInBytes) + (c * numChannels * bytesPerChannel) + (chn * bytesPerChannel) + bpc;
					int aSrcIdx = (ay * oldStrideInBytes) + (ax * numChannels * bytesPerChannel) + (chn * bytesPerChannel) + bpc;
					int bSrcIdx = (by * oldStrideInBytes) + (bx * numChannels * bytesPerChannel) + (chn * bytesPerChannel) + bpc;
					int cSrcIdx = (cy * oldStrideInBytes) + (cx * numChannels * bytesPerChannel) + (chn * bytesPerChannel) + bpc;
					int dSrcIdx = (dy * oldStrideInBytes) + (dx * numChannels * bytesPerChannel) + (chn * bytesPerChannel) + bpc;

					assert(aSrcIdx < lenSrc);
					assert(bSrcIdx < lenSrc);
					assert(cSrcIdx < lenSrc);
					assert(dSrcIdx < lenSrc);

					float lerpAB = lerp(src[aSrcIdx], src[bSrcIdx], ctrX);
					float lerpCD = lerp(src[cSrcIdx], src[dSrcIdx], ctrX);

					uint8_t lerpVal = (uint8_t)floorf(lerp(lerpAB, lerpCD, ctrY));

					dest[destIdx] = lerpVal;
				}
			}
		}
	}

	return;
}


#endif /* SS_IMAGE_RESIZE_H */
