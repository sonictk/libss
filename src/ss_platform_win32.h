/**
 * @file   ss_platform_win32.h
 * @brief  Windows platform abstraction layer.
 */
#ifndef SS_PLATFORM_WIN32_H
#define SS_PLATFORM_WIN32_H

#include <Windows.h>

/// TLS storage specifier.
#define SS_ThreadLocalStorage __declspec(thread)

#define SS_DLLExport __declspec(dllexport)
#define SS_DLLImport __declspec(dllimport)
#define SS_GCCExport

#define SS_MAX_PATH MAX_PATH
#define SS_OS_PATH_SEPARATOR '\\'
#define SS_OS_ALT_PATH_SEPARATOR '/'

/// A handle to a file.
typedef HANDLE SS_OSFileHandle;

/// A handle to a window.
typedef HWND SS_OSWindowHandle;

/// A handle to an instance.
typedef HINSTANCE SS_OSInstHandle;

/// A handle to a DLL.
typedef HMODULE SS_DLLHandle;

/// A function pointer to a symbol in a DLL.
typedef FARPROC SS_FuncPtr;


#endif /* SS_PLATFORM_WIN32_H */
