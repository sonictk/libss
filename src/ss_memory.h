/**
 * @file   ss_memory.h
 * @brief  Memory management functions.
 */
#ifndef SS_MEMORY_H
#define SS_MEMORY_H

#ifdef _WIN32
#include "ss_memory_win32.cpp"

#elif __linux__ || __APPLE__
#include "ss_memory_linux.cpp"

#else
#error "Unsupported OS platform for compilation."

#endif // OS platform layer


/**
 * Allocates ``sz`` bytes' worth of memory from the global process heap.
 * The allocated memory is not moveable.
 *
 * @param sz	The number of bytes to allocate.
 *
 * @return		A pointer to the memory block allocated. If an error occurred,
 * 			returns a null pointer.
 */
void *ss_malloc(size_t sz);


/**
 * Reallocates the given area of memory. It must be previously allocated by ss_malloc(),
 * or ss_realloc() and not yet freed with a call to ss_free or ss_realloc.
 * Otherwise, the results are undefined.
 *
 * @param p		A pointer to the start of the memory block to re-allocate.
 * @param sz		The new size of the memory block to allocate.
 *
 * @return			A pointer to the new memory block allocated. If an error occurred,
 * 				returns a null pointer.
 */
void *ss_realloc(void *p, size_t sz);


/**
 * Frees a memory block that was previously allocated with ``ss_malloc``.
 *
 * @param p	A pointer to the memory block to free.
 */
void ss_free(void *p);


/**
 * Copies count characters from the object pointed to by src to the object pointed to by dest.
 * Both objects are interpreted as arrays of ``unsigned char``.
 * The behavior is undefined if access occurs beyond the end of the dest array.
 * If the objects overlap, the behavior is undefined.
 * The behavior is undefined if either dest or src is a null pointer.
 *
 * @param dest 		pointer to the object to copy to.
 * @param src 			pointer to the object to copy from.
 * @param count 		number of bytes to copy.
 *
 * @return				Returns a copy of dest.
 */
void *ss_memcpy(void *dest, const void *src, size_t count);


#endif /* SS_MEMORY_H */
