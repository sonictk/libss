#include "ss_string.h"

#include <string.h>
#include <stdlib.h>


int strNCmp(const char *lhs, const char *rhs, const int count)
{
	int result = strncmp(lhs, rhs, (size_t)count);

	return result;
}


size_t findStrLenC(const char *str)
{
	size_t result = strlen(str);

	return result;
}


char *findSubStr(char *str, const char *tgt)
{
	char *result = strstr(str, tgt);

	return result;
}


const char *findSubStr(const char *str, const char *tgt)
{
	const char *result = strstr(str, tgt);

	return result;
}


char *findRChar(char *str, int ch)
{
	char *result = strrchr(str, ch);

	return result;
}

const char *findRChar(const char *str, int ch)
{
	const char *result = strrchr(str, ch);

	return result;
}


char *copyStringN(char *dest, const char *src, const size_t count)
{
	char *result = strncpy(dest, src, (size_t)count);
	return result;
}


int CStrToInt(const char *str)
{
	int result = atoi(str);

	return result;
}
