#include "ss_errno.h"


/// The global error code.
static SS_ThreadLocalStorage SS_Status globalSSStatus = SS_Status_Success;


SS_Status SSGetLastError()
{
	return globalSSStatus;
}


void SSSetLastError(SS_Status status)
{
	globalSSStatus = status;

	return;
}


const char *SSGetErrMsg(SS_Status status)
{
	switch (status) {
	case SS_Status_Success:
		return GLOBAL_SS_ERR_MSG_SUCCESS;

	case SS_Status_InvalidNumOfArgs:
		return GLOBAL_SS_ERR_MSG_INVALID_NUM_OF_ARGS;

	case SS_Status_InvalidFilePath:
		return GLOBAL_SS_ERR_MSG_INVALID_FILE_PATH;

	case SS_Status_InvalidFileData:
		return GLOBAL_SS_ERR_MSG_INVALID_FILE_DATA;

	case SS_Status_EnvironmentVariableNotFound:
		return GLOBAL_SS_ERR_MSG_ENVIRONMENT_VARIABLE_NOT_FOUND;

	case SS_Status_CannotSetFilePtr:
		return GLOBAL_SS_ERR_MSG_CANNOT_SET_FILE_PTR;

	case SS_Status_EndOfFile:
		return GLOBAL_SS_ERR_MSG_EOF;

	case SS_Status_FileReadFailure:
		return GLOBAL_SS_ERR_MSG_FILE_READ_FAILURE;

	case SS_Status_FileWriteFailure:
		return GLOBAL_SS_ERR_MSG_FILE_WRITE_FAILURE;

	case SS_Status_InvalidParameter:
		return GLOBAL_SS_ERR_MSG_INVALID_PARAMETER;

	case SS_Status_UnableToCloseHandle:
		return GLOBAL_SS_ERR_MSG_UNABLE_TO_CLOSE_HANDLE;

	case SS_Status_AccessDenied:
		return GLOBAL_SS_ERR_MSG_ACCESS_DENIED;

	case SS_Status_FileSharingViolation:
		return GLOBAL_SS_ERR_MSG_FILE_SHARING_VIOLATION;

	case SS_Status_AlreadyExists:
		return GLOBAL_SS_ERR_MSG_ALREADY_EXISTS;

	case SS_Status_OutOfMemory:
		return GLOBAL_SS_ERR_MSG_OUT_OF_MEMORY;

	case SS_Status_Segfault:
		return GLOBAL_SS_ERR_MSG_ACCESS_VIOLATION;

	case SS_Status_UnknownFailure:
	default:
		return GLOBAL_SS_ERR_MSG_UNKNOWN_FAILURE;
	}
}
