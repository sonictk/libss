#include "ss_serialization.h"


uint8 *readText(const char *filePath, SS_ReadResult &status)
{
	// NOTE: (sonictk) Read text file as binary anyway since it's just bytes
	FILE *handle = fopen(filePath, "rb");
	if (!handle) {
		fprintf(stderr, "Unable to open file: %s!\n", filePath);
		status = SS_ReadResult_FileOpenFailure;
		return NULL;
	}
	fseek(handle, 0, SEEK_END);
	size_t fileSize = ftell(handle);
	fseek(handle, 0, SEEK_SET);
	size_t bufferLen = fileSize + 1;
	uint8 *content = (uint8 *)malloc(sizeof(uint8) * bufferLen);
	if (!content) {
		fclose(handle);
		fprintf(stderr, "Not enough memory to read file: %s!\n", filePath);
		status = SS_ReadResult_OutOfMemory;

		return NULL;
	}
	size_t numRead = fread(content, fileSize * sizeof(uint8), 1, handle);
	if (numRead == 0) {
		fclose(handle);
		fprintf(stderr, "Could not read file: %s!\n", filePath);
		status = SS_ReadResult_ReadFailure;

		return NULL;
	}
	// NOTE: (sonictk) Since we read in as binary, manually null-terminate
	content[fileSize] = '\0';
	fclose(handle);

	status = SS_ReadResult_Success;
	return content;
}

uint8 *readText(const char *filePath)
{
	SS_ReadResult status;
	return readText(filePath, status);
}
