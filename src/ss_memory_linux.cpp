#include "ss_memory.h"

#ifdef __cplusplus
#include <cstdlib>
using std::malloc;
using std::realloc;
using std::calloc;
using std::free;

#else
#include <stdlib.h>
#endif // __cplusplus


void *ss_malloc(size_t sz)
{
	return malloc(sz);
}


void *ss_realloc(void *p, size_t sz)
{
	return realloc(p, sz);
}


void ss_free(void *p)
{
	return free(p);
}


void *ss_memcpy(void *dest, const void *src, size_t count)
{
	return memcpy(dest, src, count);
}
