#include "ss_common_math.h"


uint8_t remapValue(const uint16_t &val)
{
	uint32_t valTypeRange = (uint32_t)pow(2, sizeof(uint16_t) * SS_PLATFORM_NUM_OF_BITS_IN_ONE_BYTE) - 1;
	uint32_t destTypeRange = (uint32_t)pow(2, sizeof(uint8_t) * SS_PLATFORM_NUM_OF_BITS_IN_ONE_BYTE) - 1;
	float factor = (float)destTypeRange / (float)valTypeRange;
	uint8_t result = (uint8_t)(factor * (float)val);

	return result;
}


uint8_t remapValue(const uint32_t &val)
{
	uint64_t valTypeRange = (uint64_t)pow(2, sizeof(uint32_t) * SS_PLATFORM_NUM_OF_BITS_IN_ONE_BYTE) - 1;
	uint64_t destTypeRange = (uint64_t)pow(2, sizeof(uint8_t) * SS_PLATFORM_NUM_OF_BITS_IN_ONE_BYTE) - 1;
	double factor = (double)destTypeRange / (double)valTypeRange;
	uint8_t result = (uint8_t)(factor * (double)val);

	return result;
}
