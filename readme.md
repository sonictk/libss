# Super Simple Libraries

These are basic libraries that I use in my applications for both math/platform
purposes. The goals are:

* To have a simple and clean implementation of basic data structures used in
  everyday computer graphics/general programming;

* To have a low-level platform abstraction layer that is reliable,
  understandable and minimally invasive;

* To have a strong focus on performance while maintaining legibility and
  reducing complexity of concerns as much as possible.

* To maintain the philosophy of "C with some nice C++ features" as far as
  possible, and to avoid "feature bloat". This doesn't mean we're against things
  like references, namespaces or basic functionality.

* Stability and backwards compatbility.

These goals are highly subject to change as I see fit for my purposes and as I
continue my learning journey. Rely on this for production code at your own risk.


# Coding Standards

## Naming conventions

- ``CamelCase`` and ``lowerCamelCase`` for all class definitions and function defintions.

- ``CAPITAL_SNAKE_CASE`` for all global constants and enums.


## Compatibility and portability

- C99 and C++11 standards only. No code requiring use of any other language features
  is encouraged. No C++ extensions are allowed where one platform would not have
  access to features over the other. The same applies to C as well.

- Compiler/platform-specific code should be used where it makes sense, but no more
  than that. Assembly is allowed for only Intel x64 instructions where it makes sense.

- All code *must* run on Windows 8/10, OSX High Sierra, and Linux CentOS 7/Ubuntu 17.10.
  Near-native OS implementations are highly encouraged over abstraction layers.

- Prefer importing C headers over C++ ones where possible for the standard library.
  (i.e. ``math.h`` over ``cmath``.) The only exceptions are where necessary functionality
  exists in the C++ version of the header, but not the C ones.

- Code tailored to compiler-specific extensions is highly discouraged. This
  means avoiding **variable-length arrays (VLAs)** (GCC), ``alloca`` (MSVC) etc.

## Namespaces

- C++ namespaces are **strongly discouraged** except in circumstances where they
  *must* be used to avoid collisions with other libraries. Use naming
  conventions to handle collisons.

- **Using-directives** are forbidden. **Using-declarations** are only allowed in
  implementation files; the only exception where they are allowed in headers is
  for standard types, and only in ``ss_platform.h``. Nowhere else should they be
  present.

## Macros and code-generation/metaprogramming

- Macros are allowed, but use common sense. Prefer a function if possible, as
  the intent is clearer and debugging is easier. The compiler can also perform
  more optimization work and introspection at compile-time more easily.

- Use of templates is discouraged and where used, they must be ``__cplusplus``
  guarded. They should also be specialized as much as possible for the use cases
  that are actually required. Yes, this library is already not C-compatible for a
  lot of reasons, but there is no reason to make it more difficult.


# Credits

Copyright Siew Yi Liang. All rights reserved.

License information is included in the ``LICENSE.txt`` file in the root of this
source distribution.
