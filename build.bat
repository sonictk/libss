@echo off
REM     Build script for the Super Simple libraries.
REM     Usage: build.bat [debug|release|clean] [2015|2017|2019]
REM                      build configuration      MSVC compiler version

setlocal

set ProjectName=libss

echo Build script started executing at %time% ...
REM Process command line arguments. Default is to build in release configuration.
set BuildType=%1
if "%BuildType%"=="" (set BuildType=release)

set MSVCVersion=%2
if "%MSVCVersion%"=="" (set MSVCVersion=2017)

echo Building %ProjectName% in %BuildType% configuration using MSVC %MSVCVersion%...

REM    Set up the Visual Studio environment variables for calling the MSVC compiler;
REM    we do this after the call to pushd so that the top directory on the stack
REM    is saved correctly; the check for DevEnvDir is to make sure the vcvarsall.bat
REM    is only called once per-session (since repeated invocations will screw up
REM    the environment)

if defined DevEnvDir goto build_setup

if %MSVCVersion%==2015 (
   call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64
   goto build_setup

)

if %MSVCVersion%==2017 (
   call "%vs2017installdir%\VC\Auxiliary\Build\vcvarsall.bat" x64
   goto build_setup
)

if %MSVCVersion%==2019 (
   call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
   goto build_setup
)

echo Invalid MSVC compiler version specified.
goto error

:build_setup

REM     Make a build directory to store artifacts; remember, %~dp0 is just a special
REM     FOR variable reference in Windows that specifies the current directory the
REM     batch script is being run in.
set BuildDir=%~dp0msbuild


if "%BuildType%"=="clean" (
    REM This allows execution of expressions at execution time instead of parse time, for user input
    setlocal EnableDelayedExpansion
    echo Cleaning build from directory: %BuildDir%. Files will be deleted^^!
    echo Continue ^(Y/N^)^?
    set /p ConfirmCleanBuild=
    if "!ConfirmCleanBuild!"=="Y" (
        echo Removing files in %BuildDir%...
        del /s /q %BuildDir%\*.*
    )
    goto end
)

echo Building in directory: %BuildDir% ...

if not exist %BuildDir% mkdir %BuildDir%

if %errorlevel% neq 0 goto error

pushd %BuildDir%

REM Setup static library compilation
set SourcesDir=%~dp0src
set StaticLibEntryPoint=%SourcesDir%\libss_static_main.cpp
set StaticLibCommonCompilerFlags=/c /nologo /WX /W4 /I "%SourcesDir%" /arch:AVX2 /Zc:__cplusplus /wd4201
set StaticLibDebugCompilerFlags=%StaticLibCommonCompilerFlags% /Od /Zi /D_DEBUG
set StaticLibReleaseCompilerFlags=%StaticLibCommonCompilerFlags% /Ox /DNDEBUG


REM Setup test(s) compilation
set TestsIncludePath=%SourcesDir%

set TestPlatformName=ss_test_platform
set TestPlatformEntryPoint=%~dp0tests\%TestPlatformName%.cpp
set OutTestPlatformExe=%BuildDir%\%TestPlatformName%.exe

set TestPlatformLinkerFlags=/subsystem:console /incremental:no /machine:x64 /nologo /defaultlib:Kernel32.lib /defaultlib:Shlwapi.lib /defaultlib:Ole32.lib /defaultlib:Rpcrt4.lib
set TestPlatformDebugLinkerFlags=%TestPlatformLinkerFlags% /opt:noref /debug /pdb:"%BuildDir%\%TestPlatformName%.pdb"
set TestPlatformReleaseLinkerFlags=%TestPlatformLinkerFlags% /opt:ref

set TestPlatformCompilerFlags=/nologo /WX /W4 /I "%TestsIncludePath%" /arch:AVX2 /Zc:__cplusplus /wd4201
set TestPlatformCompilerFlagsDebug=%TestPlatformCompilerFlags% /Od /Zi /D_DEBUG
set TestPlatformCompilerFlagsRelease=%TestPlatformCompilerFlags% /Ox /DNDEBUG

if "%BuildType%"=="debug" (
    set BuildCommand=cl %TestPlatformCompilerFlagsDebug% "%TestPlatformEntryPoint%" /Fe:"%OutTestPlatformExe%" /link %TestPlatformDebugLinkerFlags%
    set BuildStaticLibCommand=cl %StaticLibDebugCompilerFlags% %StaticLibEntryPoint% /Fo:"%BuildDir%\libss_static.obj"
) else (
    set BuildCommand=cl %TestPlatformCompilerFlagsRelease% "%TestPlatformEntryPoint%" /Fe:"%OutTestPlatformExe%" /link %TestPlatformReleaseLinkerFlags%
    set BuildStaticLibCommand=cl %StaticLibReleaseCompilerFlags% %StaticLibEntryPoint% /Fo:"%BuildDir%\libss_static.obj"
)

echo.
echo Compiling static library (command follows below)...
echo %BuildStaticLibCommand%

echo.
echo Output from compilation:
%BuildStaticLibCommand%

if %errorlevel% neq 0 goto error

echo.
echo Creating static library (command follows below)...
set StaticLibBuildCmd=lib /verbose /WX /machine:x64 /subsystem:windows /nologo /out:"%BuildDir%\libss_static.lib" "%BuildDir%\libss_static.obj"
echo Output from LIB:
echo %StaticLibBuildCmd%

%StaticLibBuildCmd%

if %errorlevel% neq 0 goto error


echo.
echo Compiling tests (command follows below)...
echo %BuildCommand%

echo.
echo Output from compilation:
%BuildCommand%

if %errorlevel% neq 0 goto error

:copy_sources_to_build
echo.
echo Copying source(s)...
set SourcesBuildDir=%BuildDir%\include
if not exist %SourcesBuildDir% mkdir %SourcesBuildDir%
set CopySourceFilesCmd=copy /Y /V %~dp0src\*.cpp %SourcesBuildDir%
set CopyHeaderFilesCmd=copy /Y /V %~dp0src\*.h %SourcesBuildDir%
%CopySourceFilesCmd%

if %errorlevel% neq 0 goto error

%CopyHeaderFilesCmd%

if %errorlevel% neq 0 goto error

if %errorlevel% == 0 goto success

:error
echo.
echo ***************************************
echo *      !!! An error occurred!!!       *
echo ***************************************
goto end


:success
echo.
echo ***************************************
echo *    Build completed successfully.    *
echo ***************************************

echo ***************************************
echo *    Running tests ...                *
echo ***************************************

start /B cmd.exe /c "%OutTestPlatformExe%"

goto end

:end

echo.
echo Build script finished execution at %time%.
popd

endlocal

exit /b %errorlevel%
